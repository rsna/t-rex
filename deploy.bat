SET imageName=trex
SET containerName=trex

docker build -t %imageName%  .

echo Delete old container...
docker rm -f %containerName%

echo Delete old Image
docker image prune -f

echo Run new container...
docker run -d -p 80:8080 --name %containerName% %imageName%