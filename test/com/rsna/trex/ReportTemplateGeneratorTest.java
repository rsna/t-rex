/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex;

import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Assert;
import org.junit.Test;

import com.rsna.trex.template.DataFieldCompletionAction;
import com.rsna.trex.template.DataFieldType;
import com.rsna.trex.template.Metadata;
import com.rsna.trex.template.Template;
import com.rsna.trex.template.field.BaseFieldTest;

/**
 * JUnit tests for {@link ReportTemplateGenerator}.
 */
public class ReportTemplateGeneratorTest extends BaseFieldTest {
    
    private static final String ID = "T1002";
    private static final String VERSION = "1.0";
    private static final String TITLE = "Template Title";
    private static final String DESCRIPTION = "Template Description";
    private static final String LANGUAGE = "en";
    private static final String TYPE = "IMAGE_REPORT_TEMPLATE";
    private static final String PUBLISHER = "Publisher";
    private static final String RIGHTS = "May be used freely, subject to license agreement.";
    private static final String LICENSE = "http://www.radreport.org/license.pdf";
    private static final String DATE = "20140710";
    private static final String CREATOR = "Scott Jensen";
    private static final String CONTRIBUTOR = "Frank N. Stein";
    private static final String STATUS = "DRAFT";
    
    private static final Map<Metadata, String> metadataToValues;
    
    static {
        metadataToValues = new HashMap<Metadata, String>();
        metadataToValues.put(Metadata.CONTRIBUTOR, CONTRIBUTOR);
        metadataToValues.put(Metadata.CREATOR, CREATOR);
        metadataToValues.put(Metadata.LAST_MODIFIED_TIMESTAMP, DATE);
        metadataToValues.put(Metadata.ID, ID);
        metadataToValues.put(Metadata.LANGUAGE, LANGUAGE);
        metadataToValues.put(Metadata.LICENSE, LICENSE);
        metadataToValues.put(Metadata.PUBLISHER, PUBLISHER);
        metadataToValues.put(Metadata.RIGHTS, RIGHTS);
        metadataToValues.put(Metadata.TITLE, TITLE);
        metadataToValues.put(Metadata.DESCRIPTION, DESCRIPTION);
        metadataToValues.put(Metadata.TYPE, TYPE);
        metadataToValues.put(Metadata.STATUS, STATUS);
    }
    
    @Test(expected=NullPointerException.class)
    public void testConstructorNullJSON() {
        new ReportTemplateGenerator((JSONObject)null);
    }
    
    @Test(expected=NullPointerException.class)
    public void testConstructorNullTemplate() {
        new ReportTemplateGenerator((Template)null);
    }
    
    @Test(expected=NullPointerException.class)
    public void testConstructorEmpty() {
        new ReportTemplateGenerator(new JSONObject());
    }
    
    @Test
    public void validateMRRT() throws Exception {
        JSONObject json = buildReportTemplateJSON();
        ReportTemplateGenerator generator = new ReportTemplateGenerator(json);
        String html = generator.generateHTMLReportTemplate();
        System.out.println(html);
        
        // add testing of the generated HTML
        File temp = File.createTempFile("test", ".html");
        temp.deleteOnExit();
        
        try {
            validateMRRT(html);
        }
        catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
    
    private void validateMRRT(String html) throws Exception {
        final File temp = File.createTempFile("test", ".html");
        temp.deleteOnExit();
        
        try (FileWriter w = new FileWriter(temp)) {
            w.write(html);
            w.flush();
        }
        catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Unable to write the HTML to a file - " + e.getMessage() + "\n" + html);
        }
        
        final String charset = "UTF-8";
        final String url = "http://www.radreport.org/validate/validate_file.php";
        
        final MultipartUtility multipart = new MultipartUtility(url, charset);
        multipart.addFilePart("file", temp);
        
        final List<String> response = multipart.finish();
        final StringBuilder sb = new StringBuilder();
        for (String line : response) {
            sb.append(line);
        }
        
        Document document = Jsoup.parse(sb.toString());
        Elements elements = document.getElementsByClass("alert-message");
        if (!elements.isEmpty()) {
            elements = document.getElementsByTag("pre");
            if (!elements.isEmpty()) {
                String error = Jsoup.parse(elements.first().html()).text();
                StringBuilder msg = new StringBuilder();
                msg.append("Not validated as MRRT - ");
                for (int i = 0; i < error.length(); i++) {
                    if (error.codePointAt(i) < 161) {
                        msg.append(error.substring(i, i + 1));
                    }
                }
                String message = msg.toString();
                message = message.replaceAll("Error:", "\nError:");
                System.out.println(message);
                Assert.fail(message);
            }
        }
    }
    
    private JSONObject buildReportTemplateJSON() throws JSONException {
        StringBuilder sb = new StringBuilder();
        sb.append(
            "{" +
                "\"metadata\": {" +
                    "\"id\": \"" + ID + "\", " +
                    "\"reportTemplateCreatorVersion\": \"" + VERSION + "\", " +
                    "\"title\": \"" + TITLE + "\", " +
                    "\"description\": \"" + DESCRIPTION + "\", " +
                    "\"language\": \"" + LANGUAGE + "\", " +
                    "\"type\": \"" + TYPE + "\", " +
                    "\"publisher\": \"" + PUBLISHER + "\", " +
                    "\"rights\": \"" + RIGHTS + "\", " +
                    "\"license\": \"" + LICENSE + "\", " +
                    "\"date\": \"" + DATE + "\", " +
                    "\"creator\": \"" + CREATOR + "\", " +
                    "\"contributor\": \"" + CONTRIBUTOR + "\", " +
                    "\"status\": \"" + STATUS + "\" " +
                "}, " +
                "\"fields\": {");
        
        int id = 0;
        
        // Add the default sections
        sb.append(
            "\"SECTION_HEADER_ID\": {" +
                "\"id\": \"SECTION_HEADER_ID\", " +
                "\"text\": {" +
                    "\"value\": \"\", " +
                    "\"codeMeaning\": \"\", " +
                    "\"codeValue\": \"\", " +
                    "\"codeScheme\": \"\" " + 
                "}, " +
                "\"type\": \"SECTION_HEADER\", " +
                "\"level\": \"1\", " +
            "}");
        
        // Add multiple variations of each field type
        for (DataFieldType type : DataFieldType.values()) {
            if (type != DataFieldType.SECTION_HEADER) {
                for (DataFieldCompletionAction action : DataFieldCompletionAction.values()) {
                    switch (type) {
                        case DATE: {
                            // TODO add test for default value
                            sb.append(
                                ", \"" + id + "\": {" +
                                    "\"id\": \"FIELD_" + id + "\", " +
                                    "\"text\": {" +
                                        "\"value\": \"Date Field " + System.currentTimeMillis() + "\", " +
                                        "\"codeMeaning\": \"\", " +
                                        "\"codeValue\": \"\", " +
                                        "\"codeScheme\": \"\" " + 
                                    "}, " +
                                    "\"type\": \"" + type.toString() + "\", " +
//                                    "\"defaultValue\": \"" + defaultValue + "\", " +
                                    "\"dataFieldCompletionAction\": \"" + action.toString() + "\", " +
                                    "\"title\": \"TITLE_" + id + "\" " +
                                "}");
                            id++;
                            break;
                        }
                        case NUMBER: {
                            sb.append(
                                ", \"" + id + "\": {" +
                                    "\"id\": \"FIELD_" + id + "\", " +
                                    "\"text\": {" +
                                        "\"value\": \"Number Field " + System.currentTimeMillis() + "\", " +
                                        "\"codeMeaning\": \"\", " +
                                        "\"codeValue\": \"\", " +
                                        "\"codeScheme\": \"\" " + 
                                    "}, " +
                                    "\"type\": \"" + type.toString() + "\", " +
                                    "\"dataFieldCompletionAction\": \"" + action.toString() + "\", " +
                                    "\"title\": \"TITLE_" + id + "\" " +
                                "}");
                            id++;
                            // With default value
                            sb.append(
                                ", \"" + id + "\": {" +
                                    "\"id\": \"FIELD_" + id + "\", " +
                                    "\"text\": {" +
                                        "\"value\": \"Number Field " + System.currentTimeMillis() + "\", " +
                                        "\"codeMeaning\": \"\", " +
                                        "\"codeValue\": \"\", " +
                                        "\"codeScheme\": \"\" " + 
                                    "}, " +
                                    "\"type\": \"" + type.toString() + "\", " +
                                    "\"defaultValue\": \"123\", " +
                                    "\"dataFieldCompletionAction\": \"" + action.toString() + "\", " +
                                    "\"title\": \"TITLE_" + id + "\" " +
                                "}");
                            id++;
                            // With min/max
                            sb.append(
                                ", \"" + id + "\": {" +
                                    "\"id\": \"FIELD_" + id + "\", " +
                                    "\"text\": {" +
                                        "\"value\": \"Number Field " + System.currentTimeMillis() + "\", " +
                                        "\"codeMeaning\": \"\", " +
                                        "\"codeValue\": \"\", " +
                                        "\"codeScheme\": \"\" " + 
                                    "}, " +
                                    "\"type\": \"" + type.toString() + "\", " +
                                    "\"defaultValue\": \"123\", " +
                                    "\"dataFieldCompletionAction\": \"" + action.toString() + "\", " +
                                    "\"title\": \"TITLE_" + id + "\", " +
                                    "\"minimum\": \"1\", " +
                                    "\"maximum\": \"1000\" " +
                                "}");
                            id++;
                            // With units and step
                            sb.append(
                                ", \"" + id + "\": {" +
                                    "\"id\": \"FIELD_" + id + "\", " +
                                    "\"text\": {" +
                                        "\"value\": \"Number Field " + System.currentTimeMillis() + "\", " +
                                        "\"codeMeaning\": \"\", " +
                                        "\"codeValue\": \"\", " +
                                        "\"codeScheme\": \"\" " + 
                                    "}, " +
                                    "\"type\": \"" + type.toString() + "\", " +
                                    "\"defaultValue\": \"123\", " +
                                    "\"dataFieldCompletionAction\": \"" + action.toString() + "\", " +
                                    "\"title\": \"TITLE_" + id + "\", " +
                                    "\"minimum\": \"1\", " +
                                    "\"maximum\": \"1000\", " +
                                    "\"units\": \"cm\", " +
                                    "\"step\": \"1.1\" " +
                                "}");
                            id++;
                            break;
                        }
                        case SELECTION_LIST: {
                            // basic settings
                            sb.append(
                                ", \"" + id + "\": {" +
                                    "\"id\": \"FIELD_" + id + "\", " +
                                    "\"text\": {" +
                                        "\"value\": \"List Field " + System.currentTimeMillis() + "\", " +
                                        "\"codeMeaning\": \"\", " +
                                        "\"codeValue\": \"\", " +
                                        "\"codeScheme\": \"\" " + 
                                    "}, " +
                                    "\"type\": \"" + type.toString() + "\", " +
                                    "\"defaultValue\": \"\", " +
                                    "\"multipleSelections\": \"false\", " +
                                    "\"dataFieldCompletionAction\": \"" + action.toString() + "\", " +
                                    "\"title\": \"TITLE_" + id + "\", " +
                                    "\"options\": [{" +
                                        "\"value\": \"OPTION A\", " +
                                        "\"codeMeaning\": \"\", " +
                                        "\"codeValue\": \"\", " +
                                        "\"codeScheme\": \"\" " + 
                                    "}, { " +
                                        "\"value\": \"OPTION B\", " +
                                        "\"codeMeaning\": \"\", " +
                                        "\"codeValue\": \"\", " +
                                        "\"codeScheme\": \"\" " + 
                                    "}] " +
                                "}");
                            id++;
                            // Multiple selections
                            sb.append(
                                ", \"" + id + "\": {" +
                                    "\"id\": \"FIELD_" + id + "\", " +
                                    "\"text\": {" +
                                        "\"value\": \"List Field " + System.currentTimeMillis() + "\", " +
                                        "\"codeMeaning\": \"\", " +
                                        "\"codeValue\": \"\", " +
                                        "\"codeScheme\": \"\" " + 
                                    "}, " +
                                    "\"type\": \"" + type.toString() + "\", " +
                                    "\"defaultValue\": \"\", " +
                                    "\"multipleSelections\": \"true\", " +
                                    "\"dataFieldCompletionAction\": \"" + action.toString() + "\", " +
                                    "\"title\": \"TITLE_" + id + "\", " +
                                    "\"options\": [{" +
                                        "\"value\": \"OPTION A\", " +
                                        "\"codeMeaning\": \"\", " +
                                        "\"codeValue\": \"\", " +
                                        "\"codeScheme\": \"\" " + 
                                    "}, { " +
                                        "\"value\": \"OPTION B\", " +
                                        "\"codeMeaning\": \"\", " +
                                        "\"codeValue\": \"\", " +
                                        "\"codeScheme\": \"\" " + 
                                    "}] " +
                                "}");
                            id++;
                            // default value
                            sb.append(
                                ", \"" + id + "\": {" +
                                    "\"id\": \"FIELD_" + id + "\", " +
                                    "\"text\": {" +
                                        "\"value\": \"List Field " + System.currentTimeMillis() + "\", " +
                                        "\"codeMeaning\": \"\", " +
                                        "\"codeValue\": \"\", " +
                                        "\"codeScheme\": \"\" " + 
                                    "}, " +
                                    "\"type\": \"" + type.toString() + "\", " +
                                    "\"defaultValue\": \"OPTION B\", " +
                                    "\"multipleSelections\": \"false\", " +
                                    "\"dataFieldCompletionAction\": \"" + action.toString() + "\", " +
                                    "\"title\": \"TITLE_" + id + "\", " +
                                    "\"options\": [{" +
                                        "\"value\": \"OPTION A\", " +
                                        "\"codeMeaning\": \"\", " +
                                        "\"codeValue\": \"\", " +
                                        "\"codeScheme\": \"\" " + 
                                    "}, { " +
                                        "\"value\": \"OPTION B\", " +
                                        "\"codeMeaning\": \"\", " +
                                        "\"codeValue\": \"\", " +
                                        "\"codeScheme\": \"\" " + 
                                    "}] " +
                                "}");
                            id++;
                            // options with radlex codes
                            sb.append(
                                ", \"" + id + "\": {" +
                                    "\"id\": \"FIELD_" + id + "\", " +
                                    "\"text\": {" +
                                        "\"value\": \"List Field " + System.currentTimeMillis() + "\", " +
                                        "\"codeMeaning\": \"\", " +
                                        "\"codeValue\": \"\", " +
                                        "\"codeScheme\": \"\" " + 
                                    "}, " +
                                    "\"type\": \"" + type.toString() + "\", " +
                                    "\"defaultValue\": \"OPTION B\", " +
                                    "\"multipleSelections\": \"false\", " +
                                    "\"dataFieldCompletionAction\": \"" + action.toString() + "\", " +
                                    "\"title\": \"TITLE_" + id + "\", " +
                                    "\"options\": [{" +
                                        "\"value\": \"OPTION A\", " +
                                        "\"codeMeaning\": \"MEANING A\", " +
                                        "\"codeValue\": \"VALUE A\", " +
                                        "\"codeScheme\": \"SCHEME A\" " + 
                                    "}, { " +
                                        "\"value\": \"OPTION B\", " +
                                        "\"codeMeaning\": \"MEANING B\", " +
                                        "\"codeValue\": \"VALUE B\", " +
                                        "\"codeScheme\": \"SCHEME B\" " + 
                                    "}, { " +
                                        "\"value\": \"OPTION C\", " +
                                        "\"codeMeaning\": \"MEANING C\", " +
                                        "\"codeValue\": \"VALUE C\", " +
                                        "\"codeScheme\": \"SCHEME C\" " + 
                                    "}] " +
                                "}");
                            id++;
                            break;
                        }
                        case TEXT: {
                            String[] defaultValues = {"", "DEFAULT_" + id};
                            for (String defaultValue : defaultValues) {
                                // Add one instance with multiple lines set to true and one set to false
                                sb.append(
                                    ", \"" + id + "\": {" +
                                        "\"id\": \"FIELD_" + id + "\", " +
                                        "\"text\": {" +
                                            "\"value\": \"Text Field " + System.currentTimeMillis() + "\", " +
                                            "\"codeMeaning\": \"\", " +
                                            "\"codeValue\": \"\", " +
                                            "\"codeScheme\": \"\" " + 
                                        "}, " +
                                        "\"type\": \"" + type.toString() + "\", " +
                                        "\"defaultValue\": \"" + defaultValue + "\", " +
                                        "\"dataFieldCompletionAction\": \"" + action.toString() + "\", " +
                                        "\"title\": \"TITLE_" + id + "\", " +
                                        "\"multipleLines\": false" +
                                    "}");
                                id++;
                                sb.append(
                                    ", \"" + id + "\": {" +
                                        "\"id\": \"FIELD_" + id + "\", " +
                                        "\"text\": {" +
                                            "\"value\": \"Date Field " + System.currentTimeMillis() + "\", " +
                                            "\"codeMeaning\": \"\", " +
                                            "\"codeValue\": \"\", " +
                                            "\"codeScheme\": \"\" " + 
                                        "}, " +
                                        "\"type\": \"" + type.toString() + "\", " +
                                        "\"defaultValue\": \"" + defaultValue + "\", " +
                                        "\"dataFieldCompletionAction\": \"" + action.toString() + "\", " +
                                        "\"title\": \"TITLE_" + id + "\", " +
                                        "\"multipleLines\": false" +
                                    "}");
                                id++;
                                // with coded value
                                sb.append(
                                    ", \"" + id + "\": {" +
                                        "\"id\": \"FIELD_" + id + "\", " +
                                        "\"text\": {" +
                                            "\"value\": \"Date Field " + System.currentTimeMillis() + "\", " +
                                            "\"codeMeaning\": \"CODE_MEANING\", " +
                                            "\"codeValue\": \"CODE_VALUE\", " +
                                            "\"codeScheme\": \"CODE_SCHEME\" " + 
                                        "}, " +
                                        "\"type\": \"" + type.toString() + "\", " +
                                        "\"defaultValue\": \"" + defaultValue + "\", " +
                                        "\"dataFieldCompletionAction\": \"" + action.toString() + "\", " +
                                        "\"title\": \"TITLE_" + id + "\", " +
                                        "\"multipleLines\": false" +
                                    "}");
                                id++;
                            }
                            break;
                        }
                        case TIME: {
                            // TODO add test for default value
                            sb.append(
                                ", \"" + id + "\": {" +
                                    "\"id\": \"FIELD_" + id + "\", " +
                                    "\"text\": {" +
                                        "\"value\": \"TIme Field " + System.currentTimeMillis() + "\", " +
                                        "\"codeMeaning\": \"\", " +
                                        "\"codeValue\": \"\", " +
                                        "\"codeScheme\": \"\" " + 
                                    "}, " +
                                    "\"type\": \"" + type.toString() + "\", " +
//                                    "\"defaultValue\": \"" + defaultValue + "\", " +
                                    "\"dataFieldCompletionAction\": \"" + action.toString() + "\", " +
                                    "\"title\": \"TITLE_" + id + "\" " +
                                "}");
                            id++;
                            break;
                        }
                    }
                }
            }
        }
        
        sb.append(
                "}, " + 
                "\"layout\": [{" +
                    "\"id\": \"SECTION_HEADER_ID\", " +
                    "\"children\": [");
        
        for (int fieldId = 0; fieldId < id; fieldId++) {
            if (fieldId > 0) {
                sb.append(", ");
            }
            sb.append("{ \"id\": \"FIELD_" + fieldId + "\" }");
        }
        
        sb.append(
                    "]" +
                "}]" +
            "}");
        
        System.out.println(sb.toString());
        
        JSONObject json = new JSONObject(sb.toString());
        System.out.println(json.toString(4));
        return json;
    }
}
