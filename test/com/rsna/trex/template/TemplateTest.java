/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.template;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import com.rsna.trex.template.field.BaseFieldTest;

/**
 * JUnit tests for {@link Template}.
 */
public class TemplateTest extends BaseFieldTest {
    
    private static final String ID = "T1002";
    private static final String VERSION = "1.0";
    private static final String TITLE = "Template Title";
    private static final String LANGUAGE = "en";
    private static final String TYPE = "IMAGE_REPORT_TEMPLATE";
    private static final String PUBLISHER = "Publisher";
    private static final String RIGHTS = "May be used freely, subject to license agreement.";
    private static final String LICENSE = "http://www.radreport.org/license.pdf";
    private static final String DATE = "20140710";
    private static final String CREATOR = "Scott Jensen";
    private static final String CONTRIBUTOR = "Frank N. Stein";
    
    private static final Map<Metadata, String> metadataToValues;
    
    static {
        metadataToValues = new HashMap<Metadata, String>();
        metadataToValues.put(Metadata.CONTRIBUTOR, CONTRIBUTOR);
        metadataToValues.put(Metadata.CREATOR, CREATOR);
        metadataToValues.put(Metadata.LAST_MODIFIED_TIMESTAMP, DATE);
        metadataToValues.put(Metadata.ID, ID);
        metadataToValues.put(Metadata.LANGUAGE, LANGUAGE);
        metadataToValues.put(Metadata.LICENSE, LICENSE);
        metadataToValues.put(Metadata.PUBLISHER, PUBLISHER);
        metadataToValues.put(Metadata.RIGHTS, RIGHTS);
        metadataToValues.put(Metadata.TITLE, TITLE);
        metadataToValues.put(Metadata.TYPE, TYPE);
    }
    
    @Test(expected=NullPointerException.class)
    public void testConstructorNull() {
        new Template((String)null);
    }
    
    @Test(expected=NullPointerException.class)
    public void testConstructorNull2() {
        new Template((JSONObject)null);
    }
    
    @Test(expected=NullPointerException.class)
    public void testConstructorEmpty() {
        new Template(new JSONObject());
    }
    
    @Test(expected=NullPointerException.class)
    public void testConstructorMissingMetadata() throws JSONException {
        JSONObject json = buildReportTemplateJSON();
        json.remove("metadata");
        new Template(json);
    }
    
    @Test
    public void test() throws JSONException {
        JSONObject json = buildReportTemplateJSON();
        new Template(json);
    }
    
    private JSONObject buildReportTemplateJSON() throws JSONException {
        JSONObject metadata = new JSONObject();
        metadata.put("id", ID);
        metadata.put("reportTemplateCreatorVersion", VERSION);
        metadata.put("title", TITLE);
        metadata.put("language", LANGUAGE);
        metadata.put("type", TYPE);
        metadata.put("publisher", PUBLISHER);
        metadata.put("rights", RIGHTS);
        metadata.put("license", LICENSE);
        metadata.put("date", DATE);
        metadata.put("creator", CREATOR);
        metadata.put("contributor", CONTRIBUTOR);
        
        JSONObject fields = new JSONObject();
        
        JSONObject field = new JSONObject();
        field.put("id", "FIELD_ID");
        field.put("text", buildCodedValueJSON("FIELD_TEXT", "", "", ""));
        field.put("type", DataFieldType.NUMBER.toString());
        field.put("defaultValue", "FIELD_DEFAULT_VALUE");
        field.put("dataFieldCompletionAction", DataFieldCompletionAction.ALERT.toString());
        field.put("title", "FIELD_TITLE");
        field.put("min", "MIN");
        field.put("max", "MAX");
        field.put("unit", "UNIT");
        field.put("step", "STEP");
        fields.put(field.optString("id"), field);
        
        JSONObject section = new JSONObject();
        section.put("id", "SECTION_ID");
        section.put("text", buildCodedValueJSON("SECTION_NAME", "", "", ""));
        section.put("type", DataFieldType.SECTION_HEADER.toString());
        section.put("level", 1);
        fields.put(section.optString("id"), section);
        
        JSONArray nodes = new JSONArray();
        JSONObject node1 = new JSONObject();
        node1.put("id", section.optString("id"));
        nodes.put(node1);
        
        JSONArray children = new JSONArray();
        JSONObject node2 = new JSONObject();
        node2.put("id", field.optString("id"));
        children.put(node2);
        node1.put("children", children);
        
        JSONObject json = new JSONObject();
        json.put("metadata", metadata);
        json.put("fields", fields);
        json.put("layout", nodes);
        
        System.out.println(json.toString(4));
        return json;
    }
}
