/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.template;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.rsna.trex.template.DataFieldType;
import com.rsna.trex.template.field.BaseFieldTest;
import com.rsna.trex.template.field.DateField;
import com.rsna.trex.template.field.Field;
import com.rsna.trex.template.field.NumberField;
import com.rsna.trex.template.field.SelectionListField;
import com.rsna.trex.template.field.TextField;
import com.rsna.trex.template.field.TimeField;

/**
 * JUnit tests for {@link DataFieldType}.
 */
public class DataFieldTypeTest extends BaseFieldTest {
    
    /**
     * Test that {@link DataFieldType#buildField(JSONObject)} creates that correct type of Field.
     * 
     * @throws JSONException
     */
    @Test
    public void testBuildField() throws JSONException {
        JSONObject fieldJSON = new JSONObject();
        Field field = null;
        
        // No type attribute should return null.
        field = DataFieldType.buildField(fieldJSON);
        Assert.assertNull("Field type should be null!", field);
        
        // Empty type attribute should return null.
        fieldJSON.put("type", "");
        field = DataFieldType.buildField(fieldJSON);
        Assert.assertNull("Field type should be null!", field);
        
        // Invalid type attribute should return null.
        fieldJSON.put("type", "INVALID");
        field = DataFieldType.buildField(fieldJSON);
        Assert.assertNull("Field type should be null!", field);
        
        fieldJSON.put("type", "TEXT");
        field = DataFieldType.buildField(fieldJSON);
        Assert.assertTrue("Incorrect field type build!", (field instanceof TextField));
        
        fieldJSON.put("type", "NUMBER");
        field = DataFieldType.buildField(fieldJSON);
        Assert.assertTrue("Incorrect field type build!", (field instanceof NumberField));
        
        fieldJSON.put("type", "SELECTION_LIST");
        field = DataFieldType.buildField(fieldJSON);
        Assert.assertTrue("Incorrect field type build!", (field instanceof SelectionListField));
        
        fieldJSON.put("type", "DATE");
        field = DataFieldType.buildField(fieldJSON);
        Assert.assertTrue("Incorrect field type build!", (field instanceof DateField));
        
        fieldJSON.put("type", "TIME");
        field = DataFieldType.buildField(fieldJSON);
        Assert.assertTrue("Incorrect field type build!", (field instanceof TimeField));
        
        // TODO selection list
//        fieldJSON.put("type", "SELECTION_LIST");
//        field = DataFieldType.buildField(fieldJSON);
//        Assert.assertTrue("Incorrect field type build!", (field instanceof SelectionListField));
    }
}
