/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.template;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.rsna.trex.template.Metadata;

/**
 * JUnit tests for {@link Metadata}.
 */
public class MetadataTest {
    private static final String ID = "T1002";
    private static final String VERSION = "1.0";
    private static final String TITLE = "Template Title";
    private static final String DESCRIPTION = "Template Description";
    private static final String EMAIL = "Author's Email";
    private static final String LANGUAGE = "en";
    private static final String TYPE = "IMAGE_REPORT_TEMPLATE";
    private static final String PUBLISHER = "Publisher";
    private static final String RIGHTS = "May be used freely, subject to license agreement.";
    private static final String LICENSE = "http://www.radreport.org/license.pdf";
    private static final String DATE = "20140710";
    private static final String CREATOR = "Scott Jensen";
    private static final String CONTRIBUTOR = "Frank N. Stein";
    private static final String STATUS = "ACTIVE";
    
    private static final Map<Metadata, String> metadataToValues;
    
    static {
        metadataToValues = new HashMap<Metadata, String>();
        metadataToValues.put(Metadata.CONTRIBUTOR, CONTRIBUTOR);
        metadataToValues.put(Metadata.CREATOR, CREATOR);
        metadataToValues.put(Metadata.ID, ID);
        metadataToValues.put(Metadata.LANGUAGE, LANGUAGE);
        metadataToValues.put(Metadata.LICENSE, LICENSE);
        metadataToValues.put(Metadata.PUBLISHER, PUBLISHER);
        metadataToValues.put(Metadata.RIGHTS, RIGHTS);
        metadataToValues.put(Metadata.TITLE, TITLE);
        metadataToValues.put(Metadata.DESCRIPTION, DESCRIPTION);
        metadataToValues.put(Metadata.EMAIL, EMAIL);
        metadataToValues.put(Metadata.TYPE, TYPE);
        metadataToValues.put(Metadata.STATUS, STATUS);
        metadataToValues.put(Metadata.CREATED_TIMESTAMP, DATE);
        metadataToValues.put(Metadata.SUBMITTED_TIMESTAMP, DATE);

        String date = (new SimpleDateFormat("yyyy-MM-dd")).format(new Date(Long.valueOf(DATE)));
        metadataToValues.put(Metadata.LAST_MODIFIED_TIMESTAMP, date);
    }
    
    @Test
    public void testSetValue() throws Exception {
        JSONObject json = buildReportTemplateJSON(1);
        Metadata.STATUS.setValue(json, "TEST");
        Assert.assertEquals(1, Metadata.STATUS.extractValues(json).size());
        Assert.assertEquals("TEST", Metadata.STATUS.extractValues(json).get(0));
    }
    
    @Test
    public void testExtractValues() throws JSONException {
        JSONObject json = buildReportTemplateJSON(1);
        for (Metadata metadata : Metadata.values()) {
            List<String> values = metadata.extractValues(json);
            Assert.assertTrue("Should only be 1 value!", values.size() == 1);
            Assert.assertEquals(metadata.toString() + " is not correct!", metadataToValues.get(metadata), values.get(0));
        }
        
        // test multiple contributors
        int count = 4;
        json = buildReportTemplateJSON(count);
        List<String> values = Metadata.CONTRIBUTOR.extractValues(json);
        Assert.assertTrue("Should only be " + count + " values!", values.size() == count);
        for (int i = 0; i < count; i++) {
            Assert.assertEquals(CONTRIBUTOR + i, values.get(i));
        }
    }
    
    private JSONObject buildReportTemplateJSON(int numOfContributors) throws JSONException {
        JSONObject metadata = new JSONObject();
        metadata.put("id", ID);
        metadata.put("reportTemplateCreatorVersion", VERSION);
        metadata.put("title", TITLE);
        metadata.put("description", DESCRIPTION);
        metadata.put("email", EMAIL);
        metadata.put("language", LANGUAGE);
        metadata.put("type", TYPE);
        metadata.put("publisher", PUBLISHER);
        metadata.put("rights", RIGHTS);
        metadata.put("license", LICENSE);
        metadata.put("date", DATE);
        metadata.put("creator", CREATOR);
        metadata.put("status", STATUS);
        metadata.put("createdTimestamp", DATE);
        metadata.put("submittedTimestamp", DATE);
        metadata.put("lastModifiedTimestamp", DATE);
        
        JSONArray contribs = new JSONArray();
        for (int i = 0; i < numOfContributors; i++) {
            contribs.put(CONTRIBUTOR + (numOfContributors > 1 ? i : ""));
        }
        metadata.put("contributors", contribs);
        
        JSONObject json = new JSONObject();
        json.put("metadata", metadata);
        return json;
    }
}