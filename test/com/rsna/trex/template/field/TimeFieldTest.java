/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.template.field;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.junit.Assert;
import org.junit.Test;

import com.rsna.trex.template.DataFieldCompletionAction;
import com.rsna.trex.template.DataFieldType;
import com.rsna.trex.template.field.TimeField;

/**
 * JUnit tests for {@link TimeField}.
 */
public class TimeFieldTest extends BaseFieldTest {
    
    @Test
    public void testBuildInput() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("id", "FIELD_ID");
        json.put("text", buildCodedValueJSON("FIELD_TEXT", "", "", ""));
        json.put("type", DataFieldType.TIME.toString());
        json.put("dataFieldCompletionAction", DataFieldCompletionAction.ALERT.toString());
        json.put("title", "FIELD_TITLE");
        json.put("defaultValue", "111");
        
        TimeField field = new TimeField(json);
        Element element = field.buildFieldElement();
        
        Assert.assertEquals("Invalid tag type!", "input", element.nodeName());
        Assert.assertTrue("Missing type attribute!", element.attributes().hasKey("type"));
        Assert.assertEquals("Invalid input type!", "time", element.attributes().get("type"));
        
        Assert.assertTrue("Missing value attribute!", element.attributes().hasKey("value"));
        Assert.assertEquals("Invalid value type!", "111", element.attributes().get("value"));
    }
}
