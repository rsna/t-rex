/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.template.field;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Element;
import org.junit.Assert;
import org.junit.Test;

import com.rsna.trex.template.DataFieldCompletionAction;
import com.rsna.trex.template.DataFieldType;
import com.rsna.trex.template.field.NumberField;

/**
 * JUnit tests for {@link NumberField}.
 */
public class NumberFieldTest extends BaseFieldTest {
    
    @Test
    public void testBuildInput() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("id", "FIELD_ID");
        json.put("text", buildCodedValueJSON("FIELD_TEXT", "", "", ""));
        json.put("type", DataFieldType.NUMBER.toString());
        json.put("defaultValue", "FIELD_DEFAULT_VALUE");
        json.put("dataFieldCompletionAction", DataFieldCompletionAction.ALERT.toString());
        json.put("title", "FIELD_TITLE");
        json.put("defaultValue", "2.2");
        json.put("min", "MIN");
        json.put("max", "MAX");
        json.put("units", "UNIT");
        json.put("step", "1.1");

        NumberField field = new NumberField(json);
        Element element = field.buildFieldElement();
        
        Assert.assertEquals("Invalid tag type!", "span", element.nodeName());
        Assert.assertEquals("Too many child elements!", 1, element.children().size());
        
        element = element.child(0);
        Assert.assertEquals("Invalid tag type!", "input", element.nodeName());
        Assert.assertTrue("Missing type attribute!", element.attributes().hasKey("type"));
        Assert.assertEquals("Invalid input type!", "number", element.attributes().get("type"));
        
        Attributes attributes = element.attributes();
        Assert.assertTrue("Missing 'min' key!", attributes.hasKey("min"));
        Assert.assertEquals("Invalid attribute value!", "MIN", attributes.get("min"));
        Assert.assertTrue("Missing 'max' key!", attributes.hasKey("max"));
        Assert.assertEquals("Invalid attribute value!", "MAX", attributes.get("max"));
        Assert.assertTrue("Missing 'data-field-units' key!", attributes.hasKey("data-field-units"));
        Assert.assertEquals("Invalid attribute value!", "UNIT", attributes.get("data-field-units"));
        Assert.assertTrue("Missing 'step' key!", attributes.hasKey("step"));
        Assert.assertEquals("Invalid attribute value!", "1.1", attributes.get("step"));  
        Assert.assertTrue("Missing value attribute!", element.attributes().hasKey("value"));
        Assert.assertEquals("Invalid value type!", "2.2", element.attributes().get("value")); 
    }
}
