/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.template.field;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Element;
import org.junit.Assert;
import org.junit.Test;

import com.rsna.trex.template.DataFieldCompletionAction;
import com.rsna.trex.template.DataFieldType;
import com.rsna.trex.template.field.AbstractInputField;
import com.rsna.trex.template.field.Field;

/**
 * JUnit tests for {@link Field}.
 */
public class AbstractInputFieldTest extends BaseFieldTest {
    
    @Test(expected = IllegalArgumentException.class)  
    public void testConstructor() { 
        new TestField(null, null);
    }
    
    @Test
    public void testBuildElement() throws JSONException {
        Map<String, String> fieldAttributes = new HashMap<String, String>();
        Field field = new TestField(buildJSON(), fieldAttributes);
        
        // There should be a <p> tag with a <label> and <input>.
        Element element = field.buildElement();
        Assert.assertEquals("Incorrect <p> tag name!", "p", element.nodeName());
        Assert.assertTrue("Incorrect # of children!", element.children().size() == 2);
        
        Element label = element.child(0);
        Assert.assertEquals("Incorrect <label> tag name!", "label", label.nodeName());
        Assert.assertEquals("Incorrect for attribute value!", "FIELD_ID", label.attributes().get("for"));
        
        // Test label created successfully.
        element = element.child(1);
        Assert.assertEquals("Incorrect <input> tag name!", "input", element.nodeName());
        Attributes attributes = element.attributes();
        Assert.assertEquals("Incorrect number of attributes!", 6 + fieldAttributes.size(), attributes.size());
        Assert.assertEquals("FIELD_ID", attributes.get("id"));
        Assert.assertEquals(DataFieldType.NUMBER.toString(), attributes.get("type"));
        Assert.assertEquals("", attributes.get("name"));
        Assert.assertEquals(DataFieldType.NUMBER.toString(), attributes.get("data-field-type"));
        Assert.assertEquals(DataFieldCompletionAction.ALERT.toString(), attributes.get("data-field-completion-action"));
        Assert.assertEquals("FIELD_DEFAULT_VALUE", attributes.get("value"));
        
        for (Map.Entry<String, String> entry : fieldAttributes.entrySet()) {
            Assert.assertEquals(entry.getValue(), attributes.get(entry.getKey()));
        }
    }
    
    private JSONObject buildJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("id", "FIELD_ID");
        json.put("text", buildCodedValueJSON("FIELD_TEXT", "", "", ""));
        json.put("type", DataFieldType.NUMBER.toString());
        json.put("defaultValue", "FIELD_DEFAULT_VALUE");
        json.put("dataFieldCompletionAction", DataFieldCompletionAction.ALERT.toString());
        json.put("title", "FIELD_TITLE");
        return json;
    }
    
    private static class TestField extends AbstractInputField {

        public TestField(JSONObject fieldJSON, Map<String, String> attributes) {
            super(fieldJSON);
        }

        @Override
        protected String getInputType() {
            return DataFieldType.NUMBER.toString();
        }
    }
}
