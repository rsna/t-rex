/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.template.field;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.junit.Assert;
import org.junit.Test;

import com.rsna.trex.template.DataFieldCompletionAction;
import com.rsna.trex.template.DataFieldType;
import com.rsna.trex.template.field.SelectionListField;

/**
 * JUnit tests for {@link SelectionListField}.
 */
public class SelectionListFieldTest {
    
    @Test
    public void testBuildElement() throws JSONException {
        JSONArray options = new JSONArray();
        for (int x = 0; x < 4; x++) {
            JSONObject json = new JSONObject();
            json.put("value", "OPTION_" + x);
            json.put("codeMeaning", "");
            json.put("codeValue", "");
            json.put("codeScheme", "");
            options.put(json);
        }
        
        JSONObject json = new JSONObject();
        json.put("id", "FIELD_ID");
        json.put("text", "FIELD_TEXT");
        json.put("type", DataFieldType.SELECTION_LIST.toString());
        json.put("defaultValue", "FIELD_DEFAULT_VALUE");
        json.put("dataFieldCompletionAction", DataFieldCompletionAction.ALERT.toString());
        json.put("defaultValue", options.getJSONObject(1).get("value"));
        json.put("options", options);
        
        // test multiple lines == true
        json.put("multipleSelections", true);
        json.put("allowFreeText", false);
        validateInput(new SelectionListField(json), true, options, 1, false);

        // test multiple lines == false
        json.put("multipleSelections", false);
        validateInput(new SelectionListField(json), false, options, 1, false);
        
        json.put("allowFreeText", true);
        validateInput(new SelectionListField(json), false, options, 1, true);
    }
    
    private void validateInput(SelectionListField field, boolean multipleSelections, JSONArray options, int defaultIndex, boolean allowFreeText) throws JSONException {
        Element select = field.buildFieldElement();
        
        if (allowFreeText) {
            Assert.assertEquals("Invalid tag type!", "input", select.nodeName());
            Element e = new Element(Tag.valueOf("span"), "");
        }
        else {
            Assert.assertEquals("Invalid tag type!", "select", select.nodeName());

            Assert.assertEquals("Incorrect # of children!", options.length(), select.children().size());
            for (int index = 0; index < options.length(); index++) {
                Element option = select.child(index);
                Assert.assertEquals("Incorrect <option> tag name!", "option", option.nodeName());
                Assert.assertEquals("Invalid value attribute!", options.getJSONObject(index).get("value"), option.attributes().get("value"));
                Assert.assertEquals("Text is not correct!", options.getJSONObject(index).get("value"), option.text());
                if (index == defaultIndex) {
                    Assert.assertTrue("Invalid selected attribute!", option.attributes().hasKey("selected"));
                }
            }
        }
        
        Assert.assertEquals("Invalid id attribute!", "FIELD_ID", select.attributes().get("id"));
        Assert.assertEquals("Invalid data-field-type attribute!", DataFieldType.SELECTION_LIST.toString(), select.attributes().get("data-field-type"));
        if (defaultIndex >= 0) {
            Assert.assertEquals("Invalid value attribute!", options.getJSONObject(defaultIndex).get("value"), select.attributes().get("value"));
        }
        
        if (multipleSelections) {
            Assert.assertEquals("Invalid multiple attribute!", "multiple", select.attributes().get("multiple"));
        }
        else {
            Assert.assertFalse("Invalid multiple attribute!", select.attributes().hasKey("multiple"));
        }
    }
}
