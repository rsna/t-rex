/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.template.field;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.junit.Assert;
import org.junit.Test;

import com.rsna.trex.template.DataFieldType;
import com.rsna.trex.template.field.SectionHeaderField;

/**
 * JUnit tests for {@link SectionHeaderField}.
 */
public class SectionHeaderFieldTest extends BaseFieldTest {
    
    @Test
    public void testBuildInput() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("id", "FIELD_ID");
        json.put("text", buildCodedValueJSON("FIELD_TEXT", "", "", ""));
        json.put("type", DataFieldType.SECTION_HEADER.toString());
        json.put("defaultValue", "FIELD_DEFAULT_VALUE");
        json.put("title", "FIELD_TITLE");
        json.put("level", 2);
        SectionHeaderField field = new SectionHeaderField(json);
        Element element = field.buildElement();
        System.out.println(element.html());
        
        Assert.assertEquals("Invalid tag type!", "section", element.nodeName());
        Assert.assertTrue("Missing id attribute!", element.attributes().hasKey("id"));
        Assert.assertEquals("Invalid id attribute!", "FIELD_ID", element.attributes().get("id"));
        Assert.assertTrue("Missing class attribute!", element.attributes().hasKey("class"));
        Assert.assertEquals("Invalid class attribute!", "level2", element.attributes().get("class"));
        Assert.assertTrue("Missing data-section-name attribute!", element.attributes().hasKey("data-section-name"));
        Assert.assertEquals("Invalid data-section-name attribute!", "FIELD_TEXT", element.attributes().get("data-section-name"));

        Assert.assertTrue("Incorrect # of children!", element.children().size() == 1);
        Element header = element.child(0);
        Assert.assertEquals("Incorrect <header> tag name!", "header", header.nodeName());
        Assert.assertEquals("Incorrect attribute value!", "level2", header.attributes().get("class"));
        Assert.assertEquals("Text is not correct!", "FIELD_TEXT", header.text());
    }
}
