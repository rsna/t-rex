/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.template.field;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.junit.Assert;
import org.junit.Test;

import com.rsna.trex.template.DataFieldCompletionAction;
import com.rsna.trex.template.DataFieldType;
import com.rsna.trex.template.field.TextField;

/**
 * JUnit tests for {@link TextField}.
 */
public class TextFieldTest extends BaseFieldTest {
    
    @Test
    public void testSingleLine() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("id", "FIELD_ID");
        json.put("text", buildCodedValueJSON("FIELD_TEXT", "", "", ""));
        json.put("type", DataFieldType.TEXT.toString());
        json.put("defaultValue", "FIELD_DEFAULT_VALUE");
        json.put("dataFieldCompletionAction", DataFieldCompletionAction.ALERT.toString());
        json.put("tooltip", "FIELD_TITLE");
        json.put("defaultValue", "FIELD_DEFAULT_VALUE");
        json.put("multipleLines", false);
        validateInput(new TextField(json), false);
    }
    
    @Test
    public void testMultipleLine() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("id", "FIELD_ID");
        json.put("text", buildCodedValueJSON("FIELD_TEXT", "", "", ""));
        json.put("type", DataFieldType.TEXT.toString());
        json.put("defaultValue", "FIELD_DEFAULT_VALUE");
        json.put("dataFieldCompletionAction", DataFieldCompletionAction.ALERT.toString());
        json.put("tooltip", "FIELD_TITLE");
        json.put("defaultValue", "FIELD_DEFAULT_VALUE");
        json.put("multipleLines", true);
        validateInput(new TextField(json), true);
    }
    
    private void validateInput(TextField field, boolean multipleLines) {
        Element element = field.buildFieldElement();
        if (multipleLines) {
            Assert.assertEquals("Invalid input type!", "textarea", element.nodeName());
            Assert.assertEquals("Invalid value type!", "FIELD_DEFAULT_VALUE", element.text());
        }
        else {
            Assert.assertEquals("Invalid input type!", "input", element.nodeName());
            Assert.assertEquals("Invalid text!", "text", element.attributes().get("type"));
        }
        Assert.assertEquals("Invalid id!", "FIELD_ID", element.attributes().get("id"));
        Assert.assertEquals("Invalid data-field-type!", "TEXT", element.attributes().get("data-field-type"));
        Assert.assertEquals("Invalid data-field-completion-action!", "ALERT", element.attributes().get("data-field-completion-action"));
    }
}
