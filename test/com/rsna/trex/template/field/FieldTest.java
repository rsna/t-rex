/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.template.field;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.junit.Assert;
import org.junit.Test;

import com.rsna.trex.template.DataFieldType;
import com.rsna.trex.template.field.Field;

/**
 * JUnit tests for {@link Field}.
 */
public class FieldTest extends BaseFieldTest {
    
    @Test(expected = IllegalArgumentException.class)  
    public void testConstructorNull() { 
        new Field(null) {
            @Override
            public Element buildElement() {
                return null;
            }
        };
    }
    
    @Test
    public void testGetMethods() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("id", "SOME_ID"); 
        json.put("name", ""); 
        json.put("tooltip", "TITLE"); 
        json.put("type", DataFieldType.TEXT.toString());
        Field field = new Field(json) {
            @Override
            public Element buildElement() {
                return null;
            }
        };
        Assert.assertEquals("ID is not correct!", "SOME_ID", field.getId());
        Assert.assertEquals("Name is not correct!", "", field.getName());
        Assert.assertEquals("Title is not correct!", "TITLE", field.getTooltip());
        Assert.assertEquals("Type is not correct!", DataFieldType.TEXT, field.getDataFieldType());
    }
}
