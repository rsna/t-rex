/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import org.json.JSONException;

import com.rsna.trex.template.Template;

/**
 * Manager to manage the templates.
 * <p>
 * This version of T-Rex will just be storing the template information to a local directory.  This will also
 * store some template information using a Java {@link Properties} file so that information about all of the
 * submitted templates can be easily and quickly retrieved.
 */
public class TemplateManager {
    
    /** The application's home directory. */
    private static final File APPLICATION_HOME_DIRECTORY; 
    
    /** Directory to save the submitted templates. */
    private static final File TEMPLATE_REPOSITORY_DIRECTORY;
    
    /** Directory to save the MRRT documents for the submitted templates. */
    private static final File MRRT_REPOSITORY_DIRECTORY;
    
    /** The {@link File} used for the Java {@link Properties}. */
    private static final File PROPERTIES_FILE;
    
    /** Extension to use for the template files. */
    private static final String TEMPLATE_EXTENSION = ".template";
    
    /** Extension to use for the MRRT files. */
    private static final String MRRT_EXTENSION = ".html";
    
    static {
        // TODO need to make this work on ubuntu/linux
        APPLICATION_HOME_DIRECTORY = createDirectoryFromRoot("trex", "templates");
        if (!APPLICATION_HOME_DIRECTORY.exists()) {
            APPLICATION_HOME_DIRECTORY.mkdirs();
        }
        
        TEMPLATE_REPOSITORY_DIRECTORY = new File(APPLICATION_HOME_DIRECTORY, "/repo");
        if (!TEMPLATE_REPOSITORY_DIRECTORY.exists()) {
            TEMPLATE_REPOSITORY_DIRECTORY.mkdirs();
        }
        
        MRRT_REPOSITORY_DIRECTORY = new File(APPLICATION_HOME_DIRECTORY, "/mrrt");
        if (!MRRT_REPOSITORY_DIRECTORY.exists()) {
            MRRT_REPOSITORY_DIRECTORY.mkdirs();
        }
        
        PROPERTIES_FILE = new File(APPLICATION_HOME_DIRECTORY, "trex.properties");
        if (!PROPERTIES_FILE.exists()) {
            try {
                PROPERTIES_FILE.createNewFile();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    /** Java {@link Properties} to store template information. */
    private final Properties properties;
    
    /**
     * Constructor. 
     */
    public TemplateManager() {
        this.properties = new Properties();
        try (FileInputStream in = new FileInputStream(PROPERTIES_FILE)) {
            this.properties.load(in);
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Store a template.  
     * 
     * @param template
     * @throws IOException
     */
    public void storeTemplate(final Template template) throws IOException {
        saveTemplate(template, false);
    }
    
    /**
     * Save a template.  This will write the template's JSON and MRRT document to the file system.
     * 
     * @param template
     * @param archived
     * @throws IOException
     */
    public void saveTemplate(final Template template, final boolean archived) throws IOException {
        // TODO implement some roll back logic.
        storeJSON(template);
        storeMRRT(template);
    }
    
    /**
     * Store the JSON representation of the template.
     * 
     * @param template
     * @throws IOException
     */
    private static void storeJSON(final Template template) throws IOException {
        File file = new File(TemplateManager.TEMPLATE_REPOSITORY_DIRECTORY, 
                             TemplateManager.buildJSONFileName(template));
        try {
            TemplateManager.writeFile(file, template.getJSON().toString(4));
        }
        catch (JSONException e) {
        }
    }
    
    /**
     * Write the MRRT document for the {@link Template} to the file system.
     * 
     * @param template
     * @throws IOException
     */
    private static void storeMRRT(final Template template) throws IOException {
        File file = new File(TemplateManager.MRRT_REPOSITORY_DIRECTORY, 
                             TemplateManager.buildMRRTFileName(template));
        TemplateManager.writeFile(file, template.getMRRT(false));
    }
    
    /**
     * Save the provided contents to the provided {@link File}.
     * 
     * @param file
     * @param contents
     * @throws IOException
     */
    private static void writeFile(File file, String contents) throws IOException {
        if (file.exists()) {
            file.delete();
        }

        try (FileWriter w = new FileWriter(file)) {
            w.write(contents);
            w.flush();
        }
    }
    
    /**
     * Build the name that should be used to store the JSON for the given {@link Template}.
     * 
     * @param template
     * @return
     */
    private static String buildJSONFileName(final Template template) {
        return template.getId() + TEMPLATE_EXTENSION;
    }
    
    /**
     * Build the name that should be used to store the MRRT for the given {@link Template}.
     * 
     * @param template
     * @return
     */
    private static String buildMRRTFileName(final Template template) {
        return template.getId() + MRRT_EXTENSION;
    }
    
    /**
     * Create a {@link File} using the provided path. This will create the path to the directory starting in the root
     * directory of the operating system. For example, the following {@code directoryPath}:
     * 
     * <pre>
     * "some", "path", "to", "a", "directory"
     * </pre>
     * 
     * will create the following directory structure on Windows:
     * 
     * <pre>
     * C:\some\path\to\a\directory\
     * </pre>
     * 
     * @param directoryPath
     *            Variable length array specifying the path to the directory starting from the operating system's root
     *            directory.
     * @return An instance of {@link File}.
     */
    private static File createDirectoryFromRoot(final String...directoryPath) {
        File file = File.listRoots()[0];
        for (String dir : directoryPath) {
            file = new File(file, dir);
        }
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }
}
