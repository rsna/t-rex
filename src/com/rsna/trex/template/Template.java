/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.template;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.rsna.trex.ReportTemplateGenerator;
import com.rsna.trex.template.field.Field;

/**
 * Parse the Report Template JSON into POJOs.
 */
public class Template {
    
	private static final Log LOG = LogFactory.getLog(Template.class);
	
    /** Report Template JSON format. */
    private final JSONObject templateJSON;
    
    /** Template's unique identifier. */
    private final String id;
    
    /** Version of the report template creator used to generate the JSON. */
    private final int creatorVersion;
    
    /** The report template's {@link Field} instances. */
    private final Map<String, Field> fields;
    
    public Template(final String id) {
        if (id == null) {
            throw new NullPointerException("The 'id' parameter is null!");
        }
        this.templateJSON = new JSONObject();
        this.id = id;
        this.creatorVersion = 0;
        this.fields = new HashMap<String, Field>();
    }
    
    /**
     * Constructor. Parses the report template JSON.
     * 
     * @param templateJSON
     *            Report template JSON.
     * @throws JSONException 
     */
    public Template(final JSONObject templateJSON) throws JSONException {
        if (templateJSON == null) {
            throw new NullPointerException("The 'sectionJSON' parameter is null!");
        }
        this.templateJSON = templateJSON;
        this.id = Metadata.ID.extractValues(this.templateJSON).get(0);
        this.creatorVersion = templateJSON.optJSONObject("metadata").optInt("reportTemplateCreatorVersion", 0);
        
        this.fields = new HashMap<String, Field>();
        for (Field field : Field.extractFields(templateJSON)) {
        	LOG.debug(field);
        	LOG.debug(field.getId());
            this.fields.put(field.getId(), field);
        }
    }

    /**
     * The template's unique identifier.
     * 
     * @return
     */
    public String getId() {
        return id;
    }
    
//    /**
//     * Set the template's status to "ACTIVE".
//     */
//    public void setStatusActive() {
//        Metadata.STATUS.setValue(this.templateJSON, "ACTIVE");
//    }
    
//    public void setMetadataValue(Metadata metadata, String value) {
//        metadata.setValue(this.templateJSON, value);
//    }
    
    /**
     * Get the JSON representation of the template.
     * 
     * @return
     */
    public JSONObject getJSON() {
        return this.templateJSON;
    }
    
    /**
     * Get the MRRT representation of this template.
     * 
     * @return
     */
    public String getMRRT(boolean preview) {
        if (preview) {
            return (new ReportTemplateGenerator(this)).generateHTMLReportTemplatePreview();
        }
        return (new ReportTemplateGenerator(this)).generateHTMLReportTemplate();
    }

    /**
     * The report template creator version.
     * 
     * @return
     */
    public int getCreatorVersion() {
        return creatorVersion;
    }

    /**
     * Get a report template's {@link Field} that has the provided ID.  This may return {@code null}. 
     * 
     * @return
     */
    public Field getField(String id) {
        return this.fields.get(id);
    }
    
    /**
     * Get a {@link Collection} of all of the Fields.
     * 
     * @return
     */
    public Collection<Field> getAllFields() {
        return this.fields.values();
    }
    
    /**
     * Get the {@link JSONArray} containing the node layout of the template.
     * 
     * @return
     */
    public JSONArray getLayout() {
        return this.templateJSON.optJSONArray("layout");
    }
}
