/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.template;

import org.json.JSONObject;

/**
 * Immutable class to parse the contents of a {@link JSONObject} containing code value information.
 */
public class CodedValue {
    
    /** The ID of the {@link Field} to which this {@link CodedValue} belongs. */
    private final String fieldId;
    
    /** The value of the {@link Field}. */
    private final String value;
    
    /** The code's meaning. */
    private final String codeMeaning;
    
    /** The code's value. */
    private final String codeValue;
    
    /** The code's scheme. */
    private final String codeScheme;
    
    /**
     * Instantiate an instance of {@link CodedValue}. 
     * 
     * @param fieldId The ID of the {@link Field} to which this {@link CodedValue} belongs.
     * @param json {@link JSONObject} containing the coded value data.
     */
    public CodedValue(final String fieldId, final JSONObject json) {
        if (json == null) {
            this.fieldId = null;
            this.value = "";
            this.codeMeaning = "";
            this.codeValue = "";
            this.codeScheme = "";
        }
        else {
            this.fieldId = fieldId;
            this.value = json.optString("value");
            this.codeMeaning = json.optString("codeMeaning");
            this.codeValue = json.optString("codeValue");
            this.codeScheme = json.optString("codeScheme");
        }
    }
    
    public CodedValue(final String fieldId, final String value, final String meaning, final String codeValue, final String scheme) {
        this.fieldId = fieldId;
        this.value = value;
        this.codeMeaning = meaning;
        this.codeValue = codeValue;
        this.codeScheme = scheme;
    }
    
    /**
     * Is the field coded?
     * 
     * @return
     */
    public boolean isCoded() {
        return (this.codeValue != null && !this.codeValue.trim().isEmpty() &&
                this.codeMeaning != null && !this.codeMeaning.trim().isEmpty() &&
                this.codeScheme != null && !this.codeScheme.trim().isEmpty());
    }
    
    /**
     * Get the ID of the {@link Field}.
     * 
     * @return
     */
    public String getFieldId() {
        return this.fieldId;
    }
    
    /**
     * Get the value of the {@link Field}.
     * 
     * @return
     */
    public String getValue() {
        return this.value;
    }

    /** 
     * Get the code's meaning.
     * 
     * @return
     */
    public String getCodeMeaning() {
        return this.codeMeaning;
    }

    /** 
     * Get the code's value.
     * 
     * @return
     */
    public String getCodeValue() {
        return this.codeValue;
    }

    /** 
     * Get the code's scheme.
     * 
     * @return
     */
    public String getCodeScheme() {
        return this.codeScheme;
    }
}
