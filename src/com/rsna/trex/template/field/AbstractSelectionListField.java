/*
 * Copyright (c) 2017, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.template.field;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;

import com.rsna.trex.template.CodedValue;

/**
 * Abstract base class for selection list fields (check boxes and radio buttons).
 */
abstract public class AbstractSelectionListField extends AbstractDataField {
	
	final JSONArray options;
    
    /**
     * Instantiate an instance of {@link AbstractSelectionListField}.
     * 
     * @param fieldJSON
     *            {@link JSONObject} containing the number field data.
     */
    public AbstractSelectionListField(final JSONObject fieldJSON) {
        super(fieldJSON);
        options = fieldJSON.optJSONArray("options");
    }
    
    /**
     * Get the type for the <input> tag, e.g. "checkbox" or "radio".
     * 
     * @return
     */
    abstract protected String getInputOptionType();

    @Override
    protected Element buildDataElement() {
    	final Element div = new Element(Tag.valueOf("span"), "");
    	if (this.options != null) {
            for (int index = 0; index < this.options.length(); index++) {
                div.appendChild( new Element(Tag.valueOf("br"), ""));
                
                final JSONObject json = this.options.optJSONObject(index);
                final String value = json.optString("value");
                final Element input = new Element(Tag.valueOf("input"), "");
                input.attr("id", getOptionId(index));
                input.attr("type", getInputOptionType());
                input.attr("name", super.getId());
                input.attr("value", value);
                if (json.optBoolean("selected", false)) {
                	input.attr("checked", "");
                }
                
                if (json.optBoolean("allowFreeText")) {
                	final Element freeTextSpan = new Element(Tag.valueOf("span"), "");
                	freeTextSpan.appendChild(input);
                	
                	final Element freeTextInput = new Element(Tag.valueOf("input"), "");
                    freeTextInput.attr("id", getOptionId(index) + "_FreeText");
                    freeTextInput.attr("type", "text");
                    freeTextInput.attr("value", value);
                    freeTextSpan.appendChild(freeTextInput);
                    
                    div.appendChild(freeTextSpan);
                }
                else {
                	input.appendText(value);
                    div.appendChild(input);
                }
            }
        }
        return div;
    }
    
    @Override
    public List<CodedValue> getCodedValues() {
        List<CodedValue> codedValues = super.getCodedValues();
        for (int index = 0; index < this.options.length(); index++) {
            CodedValue cv = new CodedValue(getOptionId(index), this.options.optJSONObject(index));
            codedValues.add(cv);
        }
        return codedValues;
    }
    
    private String getOptionId(final int index) {
        return this.id + "_" + index;
    }
}
