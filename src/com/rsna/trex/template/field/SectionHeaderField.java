/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.template.field;

import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;

/**
 * {@link Field} to represent section headers.  Section headers are compound fields containing a header label
 * and 0-n fields.
 */
public class SectionHeaderField extends Field {
	
	public static final String TYPE = "SECTION_HEADER";
    
    /** The section's level. */
    private final int level;
    
    /**
     * Build a section header field.
     * 
     * @param sectionJSON
     */
    public SectionHeaderField(JSONObject sectionJSON) {
        super(sectionJSON);
        this.level = sectionJSON.optInt("level");
    }

    @Override
    public Element buildElement() {
        Element sectionElement = new Element(Tag.valueOf("section"), "");
        sectionElement.attr("id", this.id);
        sectionElement.attr("class", "level" + this.level);
        sectionElement.attr("data-section-name", this.text.getValue());
        
        Element headerElement = new Element(Tag.valueOf("header"), "");
        headerElement.attr("class", "level" + this.level);
        headerElement.appendText(this.text.getValue());
        sectionElement.appendChild(headerElement);
        
        return sectionElement;
    }
}
