/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.template.field;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;

import com.rsna.trex.template.DataFieldType;

/**
 * Text fields may contain narrative text that is editable by the user. The main
 * function of a text field is to delimit a part of the report for rapid
 * navigation and possible modification. Text fields are currently in wide use
 * by radiology speech recognition systems. Text fields shall be expressed using
 * the HTML5 input element with type attribute = text (for single line text
 * fields) or with type attribute = textarea (for multi-line text fields).
 */
public class TextField extends AbstractInputField {
    
    /** Possible input types for text fields. */
    public enum TextFieldType { TEXT, TEXTAREA };
    
    /** The text field type for this {@link TextField}. */
    private final TextFieldType textFieldType;

    /**
     * Instantiate an instance of {@link TextField}.
     * 
     * @param fieldJSON {@link JSONObject} containing the text field data.
     */
    public TextField(final JSONObject fieldJSON) {
        super(updateDataFieldType(fieldJSON));
        
        boolean multipleLines = fieldJSON.optBoolean("multipleLines", false); 
        if (multipleLines) {
            this.textFieldType = TextFieldType.TEXTAREA;
        }
        else {
            this.textFieldType = TextFieldType.TEXT;
        }
    }
    
    private static JSONObject updateDataFieldType(final JSONObject fieldJSON) {
        boolean multipleLines = fieldJSON.optBoolean("multipleLines", false); 
        try {
	        if (multipleLines) {
	        	fieldJSON.put("type", DataFieldType.TEXTAREA.toString());
	        }
	        else {
	        	fieldJSON.put("type", DataFieldType.TEXT.toString());
	        }
        }
        catch (JSONException e) {
        	// very, very, very unlikely to happen so do nothing.
        }
    	return fieldJSON;
    }

    @Override
    protected Element buildDataElement() {
        if (this.textFieldType == TextFieldType.TEXT) {
            return super.buildDataElement();
        }
        Element element = new Element(Tag.valueOf("textarea"), "");
        addInputAttributes(element);
        return element;
    }
    
    @Override
    protected void setFieldValue(final Element element, final String value) {
        if (this.textFieldType == TextFieldType.TEXTAREA) {
        	element.appendText(value);
        }
        else {
        	super.setFieldValue(element, value);
        }
    }

    @Override
    protected String getInputType() {
        return this.textFieldType.name().toLowerCase();
    }

    @Override
    protected void addInputAttributes(Element input) {
        if (this.textFieldType == TextFieldType.TEXTAREA) {
            input.attr("rows", "3");
            input.attr("cols", "100");
        }
    }
}
