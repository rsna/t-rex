 /* Copyright (c) 2017, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.template.field;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;

import com.rsna.trex.servlet.ExportMRRTTemplate;
import com.rsna.trex.template.CodedValue;

/**
 * Radio fields.
 */
public class RadelementCdeField extends Field {

	private static final Log LOG = LogFactory.getLog(RadelementCdeField.class);
	
	private JSONObject set;
	private Field[] fields;
    /**
     * Instantiate an instance of {@link RadioField}.
     * 
     * @param fieldJSON
     *            {@link JSONObject} containing the number field data.
     * @throws JSONException 
     */
    public RadelementCdeField(final JSONObject set) throws JSONException {
    	super(set);
        this.set = set.getJSONObject("set");
        ConvertElementsToFields();
        LOG.debug(this.set);
        LOG.debug(this.fields);
    }
    
    public void ConvertElementsToFields() {
    	List<Field> fields = new ArrayList<Field>();
    	try {
			LOG.debug(this.set.getJSONArray("elements"));
			JSONArray elements = this.set.getJSONArray("elements");
			
			for (int i = 0 ; i < elements.length(); i++) {
				JSONObject element = elements.getJSONObject(i);
				
				// set the question text
				// don't know if this is the priorities we want for which data field is the 'question'
				// but question is null on a lot of elements
				JSONObject text = new JSONObject();
				String question = element.getString("question");
				
				if(question.isEmpty() || question == null) {
					question = element.getString("definition");
				}
				
				if(question.isEmpty() || question == null) {
					question = element.getString("name");
				}
				text.put("value", element.getString("id") + ": " +  question);
				text.put("codeMeaning", "");
				text.put("codeValue", "");
				text.put("codeScheme", "");

				// generate a different data field depending on what keys are in the element
				if (element.has("value_set")) {
					JSONObject valueSet = element.getJSONObject("value_set");
					JSONArray values = valueSet.getJSONArray("values");
					JSONArray options = new JSONArray();
					
					for (int index = 0; index < values.length(); index++) {
						JSONObject option = new JSONObject();
						option.put("value", values.getJSONObject(index).get("name"));
						option.put("codeMeaning", "");
						option.put("codeValue", "");
						option.put("codeScheme", "");
						options.put(option);
					}

					JSONObject data = new JSONObject();
					data.put("text", text);
					data.put("options", options);
					data.put("id", element.get("id"));
					data.put("type", "SELECTION_LIST");
					data.put("tooltip", element.get("question"));
					
					if(valueSet.getInt("max_cardinality") > 1) {
						data.put("multipleSelections", true); 
					}

					fields.add(new SelectionListField(data));
				} else if (element.has("integer_values")) {					
					JSONObject data = new JSONObject();
					data.put("text", text);
					data.put("id", element.get("id"));
					data.put("type", "NUMBER");
					data.put("tooltip", element.get("question"));
					data.put("min", element.getJSONObject("integer_values").get("min"));
					data.put("max", element.getJSONObject("integer_values").get("max"));
					data.put("step", element.getJSONObject("integer_values").get("step"));
					data.put("units", element.getJSONObject("integer_values").get("unit"));
					
					fields.add(new NumberField(data));
					
				} else if (element.has("float_values")) {
					JSONObject data = new JSONObject();
					data.put("text", text);
					data.put("id", element.get("id"));
					data.put("type", "NUMBER");
					data.put("tooltip", element.get("question"));
					data.put("min", element.getJSONObject("float_values").get("min"));
					data.put("max", element.getJSONObject("float_values").get("max"));
					data.put("step", element.getJSONObject("float_values").get("step"));
					data.put("unit", element.getJSONObject("float_values").get("unit"));
					
					fields.add(new NumberField(data));
				} else if (element.has("boolean_values")) {
					JSONArray options = new JSONArray();
					JSONObject option = new JSONObject();
					option.put("value", "");
					option.put("codeMeaning", "");
					option.put("codeValue", "");
					option.put("codeScheme", "");
					options.put(option);
					
					JSONObject data = new JSONObject();
					data.put("text", text);
					data.put("options", options);
					data.put("id", element.get("id"));
					data.put("type", "CHECKBOX");
					data.put("tooltip", element.get("question"));
					
					fields.add(new CheckboxField(data));
				} else if (element.has("string_values")) {
					JSONObject data = new JSONObject();
					data.put("text", text);
					data.put("id", element.get("id"));
					data.put("type", "TEXTAREA");
					data.put("tooltip", element.get("question"));
					
					data.put("multipleLines", true);
					
					fields.add(new TextField(data));
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	LOG.debug(fields);
    	this.fields = fields.toArray(new Field[] {});
    }

	@Override
	public Element buildElement() {
		Element setContainer = new Element(Tag.valueOf("div"), "");
		Element setHeader = new Element(Tag.valueOf("header"), "");
		setHeader.addClass("level2");
		try {
			String setName = this.set.getString("id") + "- " + this.set.getString("name");
			setHeader.appendText(setName);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Element elements = new Element(Tag.valueOf("div"), "");
		elements.attr("style", "padding-left: 50px");
		for (Field field : this.fields) {
			Element fieldElement = field.buildElement();
			elements.appendChild(fieldElement);
		}
		
		setContainer.appendChild(setHeader);
		setContainer.appendChild(elements);
		return setContainer;
	}
}
