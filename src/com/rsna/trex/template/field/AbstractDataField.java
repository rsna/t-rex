/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.template.field;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;

import com.rsna.trex.template.DataFieldCompletionAction;

/**
 * Abstract implementation of {@link Field} to provide some default behavior.
 */
public abstract class AbstractDataField extends Field {

    /**
     * Indicates an action that may be taken if the field is not populated by
     * the user of the Report Creator
     */
    protected final DataFieldCompletionAction dataFieldCompletionAction;
    
    /**
     * Constructor.  Extract the field's attributes from the provided {@link JSONObject}.
     * 
     * @param fieldJSON {@link JSONObject} containing the field's attributes.  This may not be null.
     */
    public AbstractDataField(final JSONObject fieldJSON) {
        super(fieldJSON);
        if (fieldJSON.has("dataFieldCompletionAction")) {
            this.dataFieldCompletionAction = DataFieldCompletionAction.valueOf(fieldJSON.optString("dataFieldCompletionAction"));
        }
        else {
            this.dataFieldCompletionAction = DataFieldCompletionAction.NONE;
        }
    }
    
    /**
     * Ask the subclass to create the appropriate type of {@link Element} for the {@link AbstractDataField}.
     * 
     * @return An instance of {@link Element}.
     */
    public Element buildElement() {
        Element fieldElement = new Element(Tag.valueOf("p"), "");
        fieldElement.attr("title", this.tooltip);
        fieldElement.appendChild(buildLabel());
        fieldElement.appendChild(buildFieldElement());
        for (Element e : buildAnyExtraElements()) {
            fieldElement.appendChild(e);
        }
        return fieldElement;
    }
    
    /**
     * Build {@link Element} to represent the {@link AbstractDataField}.
     * 
     * @return An instance of {@link Element}.
     */
    protected Element buildFieldElement() {
        Element element = buildDataElement();
        if (!element.hasAttr("id")) {
        	element.attr("id", this.id);
        }
        if (!element.hasAttr("name")) {
            element.attr("name", this.name);
        }
        element.attr("data-field-type", this.dataFieldType.toString());
        element.attr("data-field-completion-action", this.dataFieldCompletionAction.toString());
        
        String defaultValue = null;
        if (isDefaultValueCoded()) {
            JSONObject json = this.fieldJSON.optJSONObject("defaultValue");
            if (json != null) {
                defaultValue = json.optString("value");
            }
        }
        else {
            defaultValue = this.fieldJSON.optString("defaultValue");
        }
        
        if (defaultValue != null && !element.hasAttr("value")) {
            setFieldValue(element, defaultValue);
        }
        return element;
    }
    
    /**
     * Build any other {@link Element} instances that should be included for this Field.
     * 
     * @return {@link List} of {@link Element} instances, may be empty but not {@code null}.
     */
    protected List<Element> buildAnyExtraElements() {
    	return new ArrayList<>();
    }
    
    /**
     * Hook to allow subclasses to override to place the value somewhere other that as the element's value attribute.
     * 
     * @param element
     * @param value
     */
    protected void setFieldValue(final Element element, final String value) {
    	element.attr("value", value);
    }
    
    /**
     * Return if the field's default value attribute can be a coded value. Hook to allow subclasses to 
     * override. Default is to return {@code false}.
     * 
     * @return
     */
    protected boolean isDefaultValueCoded() {
        return false;
    }
    
    /**
     * Build the {@link Element} that should be used for this {@link Field}.
     * 
     * @return
     */
    abstract protected Element buildDataElement();
    
    /**
     * Build the HTML label for this {@link AbstractDataField}.
     * 
     * @return {@link Element} containing a HTML formatted label tag.
     */
    private Element buildLabel() {
        Element label = new Element(Tag.valueOf("label"), "");
        label.attr("for", this.id);
        label.appendText(this.text.getValue());
        return label;
    }
}
