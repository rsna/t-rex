/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.template.field;

/**
 * {@link Exception} to indicate that an invalid value was provided for an attribute for a {@link Field}.
 */
public class InvalidFieldAttributeException extends Exception {
    
    private static final long serialVersionUID = 1L;

    /** The {@link Field} that has an invalid attribute value. */
    private final Field field;
    
    /** The name of the attribute having the invalid value. */
    private final String attributeName;
    
    /** The invalid value. */
    private final String invalidAttributeValue;
    
    /**
     * Constructor.
     * 
     * @param field
     *            The {@link Field} that has an invalid attribute value.
     * @param name
     *            The name of the attribute having the invalid value.
     * @param invalidValue
     *            The invalid value.
     */
    public InvalidFieldAttributeException(final Field field, final String name, final String invalidValue) {
        super();
        this.field = field;
        this.attributeName = name;
        this.invalidAttributeValue = invalidValue;
    }
    
    @Override
    public String getMessage() {
        return "Invalid field attribute value: " + 
               "\n\t" + "FIELD == " + this.field.getId() + 
               "\n\t" + "ATTRIBUTE == " + this.attributeName + 
               "\n\t" + "INVALID VALUE == " + this.invalidAttributeValue;
    }

    /**
     * Get the {@link Field} that has an invalid attribute value.
     * 
     * @return A {@link Field} instance.
     */
    public Field getField() {
        return field;
    }

    /**
     * Get the name of the attribute having the invalid value.
     * 
     * @return
     */
    public String getAttributeName() {
        return attributeName;
    }

    /**
     * Get the invalid value.
     * 
     * @return
     */
    public String getInvalidAttributeValue() {
        return invalidAttributeValue;
    }
}
