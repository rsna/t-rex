/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.template.field;

import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;

/**
 * Abstract base class for all of the {@link Field} instances that provide an <input> field.
 */
abstract public class AbstractInputField extends AbstractDataField {
    
    /** 
     * Instantiate an instance of {@link AbstractInputField}.
     * 
     * @param fieldJSON {@link JSONObject} containing the field data.
     */
    public AbstractInputField(final JSONObject fieldJSON) {
        super(fieldJSON);
    }

    @Override
    protected Element buildDataElement() {
        Element element = new Element(Tag.valueOf("input"), "");
        element.attr("type", getInputType());
        addInputAttributes(element);
        return element;
    }
    
    /**
     * Get the type of <input> for this {@link AbstractInputField}.
     * 
     * @return
     */
    abstract protected String getInputType();
    
    /**
     * Hook to allow subclasses to add attributes to the input {@link Element}.  Default behavior it so add
     * nothing.
     * 
     * @param input {@link Element} representing the input.
     */
    protected void addInputAttributes(Element input) {
    }
}
