/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.template.field;

import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;

/**
 * Numeric fields contain a numeric value with associated optional range and
 * optional units. Numeric fields shall be expressed using the HTML5 input
 * element with type attribute = number.
 */
public class NumberField extends AbstractInputField {
    
    /** The minimum value this field will accept, checked by the Report Creator. */
    private final String min;
    
    /** The maximum value this field will accept, checked by the Report Creator. */
    private final String max;
    
    /** The unit of measure for the number, e.g. �HU� or "cm". */
    private final String units;
    
    /**
     * Use step=1 for integers, or specify a step value smaller than 1 for real
     * numbers. (e.g., 0.1, or 0.01, depending on desired precision).
     */
    private final Double step;

    /**
     * Instantiate an instance of {@link NumberField}.
     * 
     * @param fieldJSON
     *            {@link JSONObject} containing the number field data.
     */
    public NumberField(final JSONObject fieldJSON) {
        super(fieldJSON);
        this.min = fieldJSON.optString("min", null);
        this.max = fieldJSON.optString("max", null);
        this.units = fieldJSON.optString("units", null);
        this.step = fieldJSON.optDouble("step", 1D);
    }

    @Override
    public Element buildDataElement() {
        Element element = super.buildDataElement();
        
        if (this.min != null) {
            element.attr("min", this.min);
        }
        if (this.max != null) {
            element.attr("max", this.max);
        }
        if (this.step != null) {
            element.attr("step", this.step.toString());
        }
        if (this.units != null) {
            element.attr("data-field-units", this.units);
        }
        
        return element;
    }
    
    @Override
    protected Element buildFieldElement() {
        Element input = super.buildFieldElement();
        if (this.units == null) {
            return input;
        }
        Element span = new Element(Tag.valueOf("span"), "");
        span.appendChild(input);
        span.appendText(this.units);
        return span;
    }

    @Override
    protected String getInputType() {
        return "number";
    }
}
