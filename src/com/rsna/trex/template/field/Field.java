/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.template.field;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.nodes.Element;

import com.rsna.trex.template.CodedValue;
import com.rsna.trex.template.DataFieldType;
import com.rsna.trex.template.Template;

/**
 * Abstract base class for all of the fields.
 */
public abstract class Field {
    
	private static final Log LOG = LogFactory.getLog(Field.class);
	
    /** The field's JSON. */
    protected final JSONObject fieldJSON;

    /** Linking identifier for coded content that corresponds to this item. */
    protected final String id;
    
    /** The label text for the field. */
    protected final CodedValue text;

    /** A human readable alphanumeric identifier for this field. */
    protected final String name;

    /** The nature of the information intended to be captured by this field. */
    protected final DataFieldType dataFieldType;
    
    /** A hint that appears when the mouse hovers over the field. */
    protected final String tooltip;
    
    /**
     * Constructor.  Extract the field's attributes from the provided {@link JSONObject}.
     * 
     * @param fieldJSON {@link JSONObject} containing the field's attributes.  This may not be null.
     */
    public Field(final JSONObject fieldJSON) {
        if (fieldJSON == null) {
            throw new IllegalArgumentException("Parameter 'fieldJSON' is null!");
        }
        this.fieldJSON = fieldJSON;
        this.id = fieldJSON.optString("id");
        this.text = new CodedValue(this.id, fieldJSON.optJSONObject("text"));
        this.name = "";
        this.dataFieldType = DataFieldType.valueOf(fieldJSON.optString("type"));
        this.tooltip = fieldJSON.optString("tooltip");
    }
    
    /**
     * Get the ID of this {@link Field}. 
     * 
     * @return
     */
    public String getId() {
        return this.id;
    }
    
    /**
     * Get the field's text.
     * 
     * @return
     */
    public CodedValue getText() {
        return text;
    }

    /**
     * Get the field's name.
     * 
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Get the field's {@link DataFieldType}.
     * 
     * @return
     */
    public DataFieldType getDataFieldType() {
        return dataFieldType;
    }

    /**
     * Get the field's tool tip.
     * 
     * @return
     */
    public String getTooltip() {
        return tooltip;
    }
    
    /** 
     * Get the Field's coded values.
     * 
     * @return
     */
    public List<CodedValue> getCodedValues() {
        List<CodedValue> codedValues = new ArrayList<CodedValue>();
        codedValues.add(this.text);
        return codedValues;
    }

    /**
     * Ask the subclass to create the appropriate type of {@link Element} for the {@link Field}.
     * 
     * @return An instance of {@link Element}.
     */
    abstract public Element buildElement();
    
    /**
     * Utility method to extract the {@link Field} instances from the report template JSON.
     * 
     * @param json
     * @return
     * @throws JSONException 
     */
    public static Field[] extractFields(JSONObject json) throws JSONException {
        if (json == null || !json.has("fields")) {
            return new Field[0];
        }
        List<Field> fields = new ArrayList<Field>();
        JSONObject fieldsObj = json.optJSONObject("fields");
        //LOG.debug(fieldsObj);
        if (fieldsObj != null) {
            @SuppressWarnings("unchecked")
            Iterator<String> keys = fieldsObj.keys();
            while (keys.hasNext()) {
            	String key = keys.next();
//            	if (fieldsObj.optJSONObject(key).optString("type").equals("RADELEMENT_CDE")) {
//            		RadelementCdeField set = new RadelementCdeField(fieldsObj.optJSONObject(key));
//            		LOG.debug("ITS A CDE");
//            		fields.addAll(Arrays.asList(cdeFields));
//            	} else {
            		fields.add(DataFieldType.buildField(fieldsObj.optJSONObject(key)));
            	//}
                
            }
        }
        
        LOG.debug(fields);
        return fields.toArray(new Field[] {});
    }
}
