/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.template.field;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;

import com.rsna.trex.template.CodedValue;

/**
 * Selection list fields can take on a value selected by the user from a list of
 * items. Each item on the list may be associated with text that should be
 * displayed in the report if that item is selected. Selection lists may have a
 * default value, which is displayed in the field when the template is applied.
 * A user choice among the list items may be required or optional. A single
 * selection may be required, or multiple selected elements may be allowed. Only
 * the attributes and coded content associated with the item(s) selected by
 * the user are associated with the report instance that is generated.
 */
public class SelectionListField extends AbstractDataField {
    
    /** {@code Boolean} to indicate if the selection list allows multiple selections. */
    private final boolean allowMultipleSelections;
    
    /** {@link Boolean} to indicate if the selection list allows free text. */
    private final boolean allowFreeText;
    
    /**
     * {@link JSONArray} of the options for the selection list.  Each item in the array is a 
     * {@link JSONObject}.
     */
    private final JSONArray options;

    /**
     * Instantiate an instance of {@link SelectionListField}.
     * 
     * @param fieldJSON
     *            {@link JSONObject} containing the number field data.
     */
    public SelectionListField(final JSONObject fieldJSON) {
        super(fieldJSON);
        this.allowMultipleSelections = fieldJSON.optBoolean("multipleSelections", false); 
        this.allowFreeText = fieldJSON.optBoolean("allowFreeText", false); 
        this.options = fieldJSON.optJSONArray("options");
    }

    @Override
    protected Element buildDataElement() {
    	final Element element;
    	if (this.allowFreeText) {
	        element = new Element(Tag.valueOf("input"), "");
	        element.attr("list", getListId());
	        if (this.allowMultipleSelections) {
	            element.attr("multiple", "multiple");
	        }
    	}
    	else {
	        element = new Element(Tag.valueOf("select"), "");
	        addOptions(element);
            if (this.allowMultipleSelections) {
                element.attr("multiple", "multiple");
            }
    	}
        return element;
    }
    
    @Override
    protected List<Element> buildAnyExtraElements() {
    	if (!allowFreeText) {
    		return super.buildAnyExtraElements();
    	}
    	
    	final Element list = new Element(Tag.valueOf("datalist"), "");
        list.attr("id", getListId());
        addOptions(list);
        
        final List<Element> elements = new ArrayList<Element>();
        elements.add(list);
        return elements;
    }
    
    @Override
    public List<CodedValue> getCodedValues() {
        List<CodedValue> codedValues = super.getCodedValues();
        for (int index = 0; index < this.options.length(); index++) {
            CodedValue cv = new CodedValue(getOptionId(index), this.options.optJSONObject(index));
            codedValues.add(cv);
        }
        return codedValues;
    }
    
    /**
     * Add option elements to the provided {@link Element}.
     * 
     * @param element The {@link Element} to add the options.
     */
    private void addOptions(final Element element) {
    	if (this.options != null) {
            for (int index = 0; index < this.options.length(); index++) {
                final JSONObject json = this.options.optJSONObject(index);
                final String value = json.optString("value");
                final Element option = new Element(Tag.valueOf("option"), "");
                option.attr("id", getOptionId(index));
                option.attr("value", value);
                option.attr("name", this.getId());
                option.appendText(value);
                if (value.equals(this.fieldJSON.optString("defaultValue"))) {
                    option.attr("selected", "");
                }
                element.appendChild(option);
            }
        }
    }
    
    private String getOptionId(final int index) {
        return this.id + "_" + index;
    }
    
    private String getListId() {
        return this.id + "_LIST";
    }
}
