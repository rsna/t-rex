/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.template;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Enumeration to define the meta data for the report templates.  This information is used to populate the
 * meta tags in the head of the report template.
 */
public enum Metadata {
    ID("dcterms.identifier", false, "metadata", "id"),
    TITLE("dcterms.title", false, "metadata", "title"),
    DESCRIPTION("dcterms.description", false, "metadata", "description"),
    TYPE("dcterms.type", false, "metadata", "type"),
    LANGUAGE("dcterms.language", false, "metadata", "language"),
    PUBLISHER("dcterms.publisher", false, "metadata", "publisher"),
    RIGHTS("dcterms.rights", false, "metadata", "rights"),
    CDESETS("dcterms.cdesets", false, "metadata", "cdeSets"), 
    LICENSE("dcterms.license", false, "metadata", "license"),
    LAST_MODIFIED_TIMESTAMP("dcterms.date", false, "metadata", "lastModifiedTimestamp") {
        @Override
        public List<String> extractValues(JSONObject templateJSON) {
            List<String> values = super.extractValues(templateJSON);
            if (values.size() >= 1) {
                try {
                    long millis = Long.valueOf(values.get(0));
                    String date = (new SimpleDateFormat("yyyy-MM-dd")).format(new Date(millis));
                    values.set(0, date);
                }
                catch (NumberFormatException e) {
                    // Intentionally blank.  A null date attribute will be addressed below.
                }
            }
            return values;
        }
    },
    CREATED_TIMESTAMP(null, false, "metadata", "createdTimestamp"),
    SUBMITTED_TIMESTAMP(null, false, "metadata", "submittedTimestamp"),
    CREATOR("dcterms.creator", false, "metadata", "creator"),
    CONTRIBUTOR("dcterms.contributor", true, "metadata", "contributors"),
    EMAIL(null, false, "metadata", "email"),
    STATUS(null, false, "metadata", "status");
    
    /** The value of the meta tag's name attribute. */
    private final String name;
    
    /** Flag to determine if this can have multiple values. */
    private final boolean multiplesAllowed;
    
    /** The path to the metadata's value in the template JSON structure. */
    private final String[] fieldJSONAttrPath;
    
    /**
     * Constructor.
     * 
     * @param name The name attribute value for the <meta> tag.
     * @param multiplesAllowed Indicates if multiple values are allowed, if {@code true} the value in the JSON
     *                         is expected to be an array of strings.
     * @param fieldJSONAttrPath Variable length array of {@link String} objects defining the path in the JSON
     *                          to the value for the data.
     */
    Metadata(final String name, final boolean multiplesAllowed, final String...fieldJSONAttrPath) {
        this.name = name;
        this.multiplesAllowed = multiplesAllowed;
        this.fieldJSONAttrPath = fieldJSONAttrPath;
    }
    
    /**
     * Get the name for this {@link Metadata}.
     * 
     * @return
     */
    public String getMetadataName() {
        return this.name;
    }
    
    /**
     * Set the value for the {@link Metadata} into the provided {@link JSONObject}.
     * 
     * @param templateJSON The {@link JSONObject} to put the value into.
     * @param value The value to use for the {@link Metadata}.
     */
    public void setValue(final JSONObject templateJSON, final Object value) {
        JSONObject jsonObj = templateJSON;
        int lastIndex = this.fieldJSONAttrPath.length - 1;
        for (int index = 0; index < lastIndex; index++) {
            JSONObject next = jsonObj.optJSONObject(this.fieldJSONAttrPath[index]);
            if (next == null) {
            	next = new JSONObject();
            	try {
					jsonObj.put(this.fieldJSONAttrPath[index], next);
				} 
            	catch (JSONException e) {
					e.printStackTrace();
				}
            }
            jsonObj = next;
        }
        
        try {
            if (multiplesAllowed) {
                JSONArray array = null;
                if (jsonObj.has(this.fieldJSONAttrPath[lastIndex])) {
                    array = jsonObj.getJSONArray(this.fieldJSONAttrPath[lastIndex]);
                }
                else {
                    array = new JSONArray();
                    jsonObj.put(this.fieldJSONAttrPath[lastIndex], array);
                }
                array.put(value);
            }
            else {
                jsonObj.put(this.fieldJSONAttrPath[lastIndex], value);
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Extract the metadata's value from the provided report template JSON structure.
     * 
     * @param templateJSON {@link JSONObject} containing the report template JSON structure from which the
     *                     metadata's value will be extracted.
     * @return A {@link String} containing the value.
     */
    public List<String> extractValues(JSONObject templateJSON) {
        return extractRawValues(templateJSON);
    }
    
    /**
     * Extract the metadata's value from the provided report template JSON structure.
     * 
     * @param templateJSON {@link JSONObject} containing the report template JSON structure from which the
     *                     metadata's value will be extracted.
     * @return A {@link String} containing the value.
     */
    public List<String> extractRawValues(JSONObject templateJSON) {
        JSONObject jsonObj = templateJSON;
        int lastIndex = this.fieldJSONAttrPath.length - 1;
        for (int index = 0; index < lastIndex; index++) {
            jsonObj = jsonObj.optJSONObject(this.fieldJSONAttrPath[index]);
        }
        
        List<String> values = new ArrayList<String>();
        if (jsonObj.has(this.fieldJSONAttrPath[lastIndex])) {
            if (multiplesAllowed) {
                JSONArray valueArr = jsonObj.optJSONArray(this.fieldJSONAttrPath[lastIndex]);
                for (int index = 0; index < valueArr.length(); index++) {
                    String s = valueArr.optString(index);
                    if (s != null && !s.trim().isEmpty()) {
                        values.add(valueArr.optString(index));
                    }
                }
            }
            else {
                values.add(jsonObj.optString(this.fieldJSONAttrPath[lastIndex]));
            }
        }
        else {
            values.add("");
        }
        
        return values;
    }
    
    public static Metadata getByName(final String name) {
    	if (name != null && !name.isEmpty()) {
	    	for (Metadata metadata : Metadata.values()) {
	    		if (name.equals(metadata.name)) {
	    			return metadata;
	    		}
	    	}
    	}
    	return null;
    }
}
