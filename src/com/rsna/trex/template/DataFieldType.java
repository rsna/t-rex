/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.template;

import org.json.JSONException;
import org.json.JSONObject;

import com.rsna.trex.template.field.CheckboxField;
import com.rsna.trex.template.field.DateField;
import com.rsna.trex.template.field.Field;
import com.rsna.trex.template.field.NumberField;
import com.rsna.trex.template.field.RadelementCdeField;
import com.rsna.trex.template.field.RadioField;
import com.rsna.trex.template.field.SectionHeaderField;
import com.rsna.trex.template.field.SelectionListField;
import com.rsna.trex.template.field.TextField;
import com.rsna.trex.template.field.TimeField;

/**
 * Enumeration to define the types of supported fields.
 */
public enum DataFieldType {
    SECTION_HEADER,
    TEXT,
    TEXTAREA,
    NUMBER, 
    SELECTION_LIST, 
    DATE, 
    TIME,
    CHECKBOX,
    RADIO,
    RADELEMENT_CDE;
    
    /**
     * Factory method to build a {@link Field} instance based on the field type specified in the provided
     * {@link JSONObject}.
     * 
     * @param fieldJSON {@link JSONObject} containing field data.
     * @return An instance of {@link Field}.  This will return {@code null} if the type attribute in fieldJSON
     *         is not defined or is not a valid field type or the field type is not yet supported.
     */
    public static Field buildField(JSONObject fieldJSON) {
        if (fieldJSON.has("type")) {
            try {
                switch (DataFieldType.valueOf(fieldJSON.optString("type"))) {
                    case SECTION_HEADER: {
                        return new SectionHeaderField(fieldJSON);
                    }
                    case DATE: {
                        return new DateField(fieldJSON);
                    }
                    case CHECKBOX: {
                        return new CheckboxField(fieldJSON);
                    }
                    case RADIO: {
                    	return new RadioField(fieldJSON);
                    }
                    case NUMBER: {
                        return new NumberField(fieldJSON);
                    }
                    case SELECTION_LIST: {
                        return new SelectionListField(fieldJSON);
                    }
                    case TEXT: {
                        return new TextField(fieldJSON);
                    }
                    case TEXTAREA: {
                        return new TextField(fieldJSON);
                    }
                    case TIME: {
                        return new TimeField(fieldJSON);
                    }
                    case RADELEMENT_CDE: {
                        return new RadelementCdeField(fieldJSON);
                    }
                }
            }
            catch (IllegalArgumentException | NullPointerException | JSONException e) {
                return null;
            }
        }
        return null;
    }
}
