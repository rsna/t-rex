/* 
* Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.servlet;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONObject;


/**
 * {@link BaseServlet} to search for RadLex terms.
 * 
 * @author Michael Crabtree
 */
public class RadElementServlet extends BaseServlet {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static Logger LOG = Logger.getLogger(RadElementServlet.class.getName());

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	final JSONObject json = searchRadelement(request);
		
		returnSuccess(response, json);
    }
    
    private JSONObject searchRadelement(HttpServletRequest request) throws ServletException {
    	try {
    		// start with the defaults that we require for aLL requests
        	final String pageNumber = request.getParameter("page");
        	
        	String urlString;
        	String hosturl = request.getRequestURL().toString();
        	if(hosturl.contains("stage")) {
        		urlString = "https://api3stage.rsna.org/radelement/public/v1/";
        	}else {
        		urlString = "https://api3.rsna.org/radelement/public/v1/";
        	}
    		urlString = urlString + "sets?perPage=10&page=" + pageNumber + "&meta=true&sort=name";
    		
    		// if the user specifically wants to filter or search by a specialty, add it to the query param for the request
    		if (request.getParameterMap().containsKey("specialties")) {
    			final String subspec = request.getParameter("specialties");
    			urlString = urlString + "&specialties=" + subspec;
	        }
    		// if the user specifically adds a search, add it to the query param for the request
    		// adding this here instead of in the base request because we may want to only filter by a specialty and not need this param
    		if (request.getParameterMap().containsKey("search")) {
        		String search = request.getParameter("search");
            	search = search.replaceAll("\\s", "%20");
    			urlString = urlString + "&search=" + search;
	        }
    		LOG.debug(urlString);
    		JSONObject cde = null;
            try {
                final URL url = new URL(urlString);
                final HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                final Scanner scanner = new Scanner(new BufferedInputStream(urlConnection.getInputStream()));
                final StringBuilder sb = new StringBuilder();
                while (scanner.hasNextLine()) {
                    sb.append(scanner.nextLine());
                }
                cde = new JSONObject(sb.toString());
                
            } 
            catch (Exception e ) {
                LOG.error(e);
            }   
            return cde;
        }
        catch (Exception e) {
            throw new ServletException(e);
        }
    }
}
