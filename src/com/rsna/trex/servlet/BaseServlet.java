/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.servlet;

import java.io.IOException;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Base {@link HttpServlet} for all of the application's servlets.
 */
abstract public class BaseServlet extends HttpServlet {
    
    private static final long serialVersionUID = 1L;

    private static final Log LOG = LogFactory.getLog(BaseServlet.class);

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
    
    /**
     * Extract the JSON from the {@link HttpServletRequest}.
     * 
     * @param request
     * @return The request's JSON content or {@code null}.
     * @throws IOException
     */
    protected JSONObject getContentAsJSONObject(HttpServletRequest request) throws IOException {
        JSONObject json = null;
        try {
            json = new JSONObject(getContent(request));
        }
        catch (JSONException e) {
            LOG.error("Exception parsing request JSON!", e);
        }
        return json;
    }
    
    /**
     * Get the request's content.
     * 
     * @param request
     * @return The request's JSON content or {@code null}.
     * @throws IOException
     */
    protected String getContent(final HttpServletRequest request) throws IOException {
    	final StringBuilder sb = new StringBuilder();
    	try (Scanner scanner = new Scanner(request.getInputStream())) {
    		while (scanner.hasNextLine()) {
    			sb.append(scanner.nextLine());
    		}
    	}
        return sb.toString();
    }
    
    /**
     * Return a successful HTML response.
     * 
     * @param response
     * @param html
     * @throws IOException
     */
    protected void returnSuccess(HttpServletResponse response, String html) throws IOException {
        returnSuccess(response, "text/html;charset=utf-8", html);
    }
    
    protected void returnSuccess(final HttpServletResponse response, final String contentType, final String html) throws IOException {
        response.setContentType(contentType);
        response.setStatus(HttpServletResponse.SC_OK);
        if (html != null) {
            response.getWriter().println(html);
        }
    }
    
    /** 
     * Return a successful JSON response.
     * 
     * @param response
     * @param json
     * @throws IOException
     */
    protected void returnSuccess(HttpServletResponse response, JSONObject json) throws IOException {
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);
        if (json != null) {
            response.getWriter().println(json.toString());
        }
    }
    
    /**
     * Return an error response.
     * 
     * @param response
     */
    protected void returnError(HttpServletResponse response) {
        response.setContentType("text/html;charset=utf-8");
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
    }
    
    /**
     * Return an error response.
     * 
     * @param response
     * @throws IOException 
     */
    protected void returnError(final HttpServletResponse response, final Throwable t) throws IOException {
        response.setContentType("text/html;charset=utf-8");
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        response.getWriter().println(t.getMessage());
    }
}
