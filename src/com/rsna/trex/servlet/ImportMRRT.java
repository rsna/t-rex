/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.servlet;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import com.rsna.trex.template.CodedValue;
import com.rsna.trex.template.Metadata;
import com.rsna.trex.template.field.SectionHeaderField;

/**
 * Servlet to transform an MRRT document to the T-Rex internal JSON format used in the worklist and editor.
 */
public class ImportMRRT extends BaseServlet {
	
    private static final long serialVersionUID = 1L;
    
    private static final Log LOG = LogFactory.getLog(ImportMRRT.class);

    /** Enum for the possible selection types. */
    private enum SelectionType { CHECKBOX, RADIO };
    
    /**
     * {@link Document} to store the imported MRRT document so that it can be parsed into the internal JSON
     * format.
     */
    private Document document;
    
    /**
     * {@link JSONObject} representing the internal format of the imported MRRT document.
     */
    private JSONObject json;
    
    /**
     * {@link Map} of the RadLex terms extracted from the imported MRRT document.  The key is the ID of the 
     * field that has been mapped to the RadLex term; value is the {@link CodedValue} information of the 
     * mapped RadLex term.
     */
    private Map<String, CodedValue> codedValues;

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    /**
     * Process the request to convert an MRRT document to the T-Rex internal JSON format.
     * 
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void process(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        LOG.info("Converting MRRT document...");
        final String contents = getContent(request);
        try {
            LOG.debug("MRRT document to convert:\n" + contents);
        	returnSuccess(response, convertToInternalFormat(contents));
        }
        catch (Throwable t) {
            LOG.error("Exception caught converting MRRT document!", t);
        	returnError(response, t);
        }
    }
    
    /**
     * Transform an MRRT document to the T-Rex internal JSON format.
     * 
     * @param mrrt {@link String} representing the MRRT document.
     * @return {@link String} representation of the MRRT document converted to JSON format.
     * @throws JSONException
     */
    private String convertToInternalFormat(final String mrrt) throws JSONException {
    	this.document = Jsoup.parse(mrrt);
    	this.codedValues = new HashMap<>();

        this.json = new JSONObject();
        this.json.put("fields", new JSONObject());
        this.json.put("layout", new JSONArray());
    	
    	// Get the metadata
    	parseMetadata();
    	
    	// Get any coded values
    	parseCodedValues();
    	
    	// Now get the top level sections and start parsing.
    	final Element body = this.document.body();
    	for (final Node sectionNode : body.childNodes()) {
    		parseSection(sectionNode, this.json.getJSONArray("layout"));
    	}
    	
    	final String s = json.toString(4);
    	if (LOG.isDebugEnabled()) {
    	    LOG.debug(s);
    	}
    	return s;
    }
    
    /**
     * Parse the coded values from the imported MRRT document.  This will extract all of the code values from 
     * the 
     */
    private void parseCodedValues() {
        LOG.debug("Parsing Coded Content...");
        
        Document templateAttributes = null;
        Elements elements = this.document.getElementsByTag("script");
        if (elements.size() == 1) { 
            final Element script = elements.get(0);
            if (script.childNodeSize() > 0) {
                String data = script.childNode(0).toString().trim();
                // Found some templates that had this section commented out.  Not sure why so just remove the
                // comment.
                if (data.startsWith("<!--")) {
                    data = data.substring(4);
                }
                if (data.endsWith("-->")) {
                    data = data.substring(0, data.length() - 3);
                }
                                
                templateAttributes = Jsoup.parse(data);
            }
        }
        
        if (templateAttributes == null) {
            return;
        }
        
        elements = templateAttributes.getElementsByTag("term");
        for (int index = 0; index < elements.size(); index++) {
            final Element termElement = elements.get(index);
            final String fieldId = termElement.parent().attr("origtxt");
            
            final String value = ""; // TODO What is this?
            final String codeMeaning;
            final String codeValue;
            final String codeScheme;
            
            // The code value can be in 2 format: one where each value has its own element or there is a 
            // single element and the values are attributes.
            final Elements codeElements = termElement.getElementsByTag("code");
            if (codeElements == null || codeElements.isEmpty()) {
                codeMeaning = termElement.getElementsByTag("code_meaning").get(0).text();
                codeValue = termElement.getElementsByTag("code_value").get(0).text();
                codeScheme = termElement.getElementsByTag("coding_scheme_designator").get(0).text();
            }
            else {
                final Element code = termElement.getElementsByTag("code").get(0);
                codeMeaning = code.attr("meaning");
                codeValue = code.attr("value");
                codeScheme = code.attr("scheme");
            }
            this.codedValues.put(fieldId, new CodedValue(fieldId, value, codeMeaning, codeValue, codeScheme));
        }
    }
    
    /**
     * Parse the MRRT document's metadata.
     * 
     * @param document The MRRT {@link Document}.
     * @param json The {@link JSONObject} to which the metadata will be added.
     */
    private void parseMetadata() {
        LOG.debug("Parsing Metadata...");
        
    	// Set all of the metadata to default values.
    	Metadata.CREATED_TIMESTAMP.setValue(this.json, System.currentTimeMillis());
    	Metadata.CREATOR.setValue(this.json, "");
    	Metadata.DESCRIPTION.setValue(this.json, "");
    	Metadata.EMAIL.setValue(this.json, "");
    	Metadata.LANGUAGE.setValue(this.json, "en");
    	Metadata.LAST_MODIFIED_TIMESTAMP.setValue(this.json, Long.toString(System.currentTimeMillis()));
    	Metadata.LICENSE.setValue(this.json, "http://www.radreport.org/license.pdf");
    	Metadata.PUBLISHER.setValue(this.json, "Radiological Society of North America (RSNA)");
    	Metadata.RIGHTS.setValue(this.json, "May be used freely, subject to license agreement");
    	Metadata.STATUS.setValue(this.json, "DRAFT");
    	Metadata.SUBMITTED_TIMESTAMP.setValue(this.json, "");
    	Metadata.TITLE.setValue(this.json, "");
    	Metadata.TYPE.setValue(this.json, "IMAGE_REPORT_TEMPLATE");
    	
    	// Iterate over the MRRT document's meta tags and extract the values
    	final Elements metaElements = this.document.getElementsByTag("meta");
    	for (int index = 0; index < metaElements.size(); index++) {
    		final Element meta = metaElements.get(index);
    		try {
	    		final Metadata metadata = Metadata.getByName(meta.attr("name"));
	    		if (metadata != null) {
	    		    final String value = meta.attr("content");
	    		    LOG.debug("Setting metadata: " + metadata + "==" + value);
	    			metadata.setValue(this.json, value);
	    		}
    		}
    		catch (IllegalArgumentException e) {
    			// Nothing to do, just in case the MRRT contains other meta elements.
    		}
    	}
    	
    	// Give the Template a new ID
        Metadata.ID.setValue(this.json, UUID.randomUUID().toString());
    }
    
    /**
     * Parse a section from the MRRT document.
     * 
     * @param sectionNode {@link Node} representing a section from the MRRT document.
     * @param layoutArr {@link JSONArray} for storing the template's layout.
     * @throws JSONException
     */
    private void parseSection(final Node sectionNode, final JSONArray layoutArr) throws JSONException {
    	if (!sectionNode.nodeName().equalsIgnoreCase("section")) {
    	    LOG.warn("Unexpected node type: " + sectionNode.toString());
    		return;
    	}
    	
    	LOG.debug("Parsing section: " + sectionNode.toString());
        final String id;
    	if (sectionNode.hasAttr("id")) {
            id = sectionNode.attr("id").replaceAll(" ", "_");
    	}
    	else if (sectionNode.hasAttr("data-section-name")) {
            id = sectionNode.attr("data-section-name").replaceAll(" ", "_");
    	}
    	else {
            id = null;
    	}
    	
    	if (id == null) {
    	    throw new RuntimeException("Section missing identifier: " + sectionNode.toString());
    	}
		
    	final JSONObject sectionJSON = new JSONObject();
        sectionJSON.put("type", SectionHeaderField.TYPE);
		sectionJSON.put("id", id);
		
		// Build the layout data for the section.
		final JSONObject layoutJSON = new JSONObject();
		layoutJSON.put("id", id);
		layoutArr.put(layoutJSON);
		final JSONArray childLayoutArr = new JSONArray();
		layoutJSON.put("children", childLayoutArr);
		
		// get the section's level
		int level = 1;
		if (sectionNode.hasAttr("class")) {
    		try { 
    			level = Integer.parseInt(sectionNode.attr("class").replaceAll("level", ""));
    		}
    		catch (NumberFormatException e) {
    			LOG.error("Exception caught parsing section level from: " + sectionNode, e);
    		}
		}
		else {
		    // check to see if the section's header node has a class attribute
		    for (final Node child : sectionNode.childNodes()) {
		        if (child.nodeName().equals("header")) {
		            if (child.hasAttr("class")) {
		                try { 
		                    level = Integer.parseInt(child.attr("class").replaceAll("level", ""));
		                }
		                catch (NumberFormatException e) {
		                    LOG.error("Exception caught parsing section level from: " + child, e);
		                }
		            }
		            break;
		        }
		    }
		}
		
		sectionJSON.put("level", level);
		
		// Parse the section's text.
		final JSONObject textJSON = buildSectionHeaderText(id, sectionNode);
		sectionJSON.put("text", textJSON);
		
		this.json.getJSONObject("fields").put(id, sectionJSON);
		
		// Parse the <section> element's children
    	for (final Node childNode : sectionNode.childNodes()) {
    		LOG.debug("Child Node==" + childNode.toString());
    	    switch (childNode.nodeName().toLowerCase()) {
	    		case "header": {
	    			// purposely ignoring
	    			break;
	    		}
	    		case "p": {
	    			parseField(childNode, childLayoutArr);
	    			break;
	    		}
	    		case "section": {
	    			parseSection(childNode, childLayoutArr);
	    			break;
	    		}
	    		default: {
	    		    LOG.warn("Unexpected child node type: " + childNode.toString());
	    		}
    		}
    	}
    }
    
    /**
     * Build the "text" JSON for the section header.  This will add the LOINC code values if this was an MRRT
     * created by T-Rex.
     *  
     * @param id
     * @param sectionNode
     * @return
     * @throws JSONException
     */
    private JSONObject buildSectionHeaderText(final String id, final Node sectionNode) throws JSONException {
    	final String codeMeaning;
    	final String codeScheme;
    	final String codeValue;
    	
    	switch (id) {
	    	case "clinicalInformation": {
	    		codeMeaning = "Clinical Information";
	    		codeScheme = "55752-0";
	    		codeValue = "LOINC";
	    		break;
	    	}
	    	case "procedureInformation": {
	    		codeMeaning = "Current Imaging Procedure Description";
	    		codeScheme = "55111-9";
	    		codeValue = "LOINC";
	    		break;
	    	}
	    	case "comparisons": {
	    		codeMeaning = "Radiology Comparison Study";
	    		codeScheme = "18834-2";
	    		codeValue = "LOINC";
	    		break;
	    	}
	    	case "findings": {
	    		codeMeaning = "Procedure Findings";
	    		codeScheme = "59776-5";
	    		codeValue = "LOINC";
	    		break;
	    	}
	    	case "impression": {
	    		codeMeaning = "Impressions";
	    		codeScheme = "19005-8";
	    		codeValue = "LOINC";
	    		break;
	    	}
	    	default: {
	    		codeMeaning = "";
	    		codeScheme = "";
	    		codeValue = "";
	    		break;
	    	}
    	}
    	
		final JSONObject textJSON = new JSONObject();
		textJSON.put("value", sectionNode.attr("data-section-name"));
		textJSON.put("codeMeaning", codeMeaning);
		textJSON.put("codeScheme", codeScheme);
		textJSON.put("codeValue", codeValue);
		
		return textJSON;
    }
    
    /**
     * Parse a field from the MRRT document.
     * 
     * @param fieldNode {@link Node} containing the field data.
     * @param layoutArr {@link JSONArray} for storing the template's layout.
     * @throws JSONException
     */
    private void parseField(final Node fieldNode, final JSONArray layoutArr) throws JSONException {
        LOG.debug("Parsing field node: " + fieldNode.toString());
        Node label = null;
        Node inputNode = null;
        LOG.debug("children==" + fieldNode.childNodeSize());
        for (int index = 0; index < fieldNode.childNodeSize(); index++) {
            final Node child = fieldNode.childNode(index);
            LOG.debug("child==" + child.toString());
            if (child.toString().trim().isEmpty()) {
                continue;
            }
            switch (child.nodeName().toLowerCase()) {
                case "label": {
                    if (inputNode == null) {
                        label = child;
                    }
                    break;
                }
                case "span": {
                    for (Node spanChild : child.childNodes()) {
                        if (!spanChild.toString().trim().isEmpty() && spanChild.nodeName().toLowerCase().equals("input")) {
                            inputNode = spanChild;
                            break;
                        }
                    }
                    break;
                }
                case "input":
                case "select":
                case "textarea": {
                    inputNode = child;
                }
                default: {
                }
            }
        }
        
        if (inputNode == null) {
            throw new RuntimeException("Missing input field: " + fieldNode.toString());
        }
        
        LOG.debug("LABEL==" + label);
        LOG.debug("INPUT==" + inputNode);
        
        final JSONObject fieldJSON = new JSONObject();
        
		// Tool tip is specified in the <p> tag.
		if (fieldNode.hasAttr("title")) {
		    fieldJSON.put("tooltip", fieldNode.attr("title"));
		}
		else {
            fieldJSON.put("tooltip", "");
		}
		
		final String name = inputNode.nodeName().toLowerCase();
		switch (name) {
			case "label": {
				parseLabel(fieldJSON, inputNode);
			}
			case "input": {
				parseCommonAttributes(fieldJSON, inputNode);
				parseInput(fieldJSON, inputNode);
				break;
			}
			case "textarea": {
				parseCommonAttributes(fieldJSON, inputNode);
				parseTextField(fieldJSON, inputNode, true);
				break;
			}
			case "span": {
				// Numbers, checkboxes and radios have an MRRT structure of:
		    	// <span>
		    	//     <input type="number" ... />
		    	// </span>
		    	// So get the input from the span and just parse that.
			    inputNode = inputNode.childNode(0);
				parseCommonAttributes(fieldJSON, inputNode);
		    	parseInput(fieldJSON, inputNode);
				break;
			}
			case "select": {
				parseCommonAttributes(fieldJSON, inputNode);
				parseSelectionListField(fieldJSON, inputNode);
			}
			default: {
			    LOG.warn("Unexpected field input node type: " + inputNode.toString());
			}
		}       
        
        parseLabel(fieldJSON, label);

		final String id = fieldJSON.getString("id");
		
		// add to the layout
		final JSONObject childLayout = new JSONObject();
		childLayout.put("id", id);
		layoutArr.put(childLayout);
		
		this.json.getJSONObject("fields").put(id, fieldJSON);
    }
    
    /**
     * Parse common field attributes.
     * 
     * @param fieldJSON {@link JSONObject} to add the command data extracted from {@code node}.
     * @param node {@link Node} containing the data to be extracted.
     * @throws JSONException
     */
    private void parseCommonAttributes(final JSONObject fieldJSON, final Node node) throws JSONException {
        LOG.debug("Parsing common attributes.");
        
    	fieldJSON.put("id", node.attr("id").replaceAll(" ", "_"));
        LOG.debug("\tID==" + fieldJSON.getString("id"));
    	
    	if (node.hasAttr("value")) {
        	fieldJSON.put("defaultValue", node.attr("value"));
    	}
    	else {
        	fieldJSON.put("defaultValue", ((Element) node).text());
    	}
    	LOG.debug("\tDEFAULT VALUE==" + fieldJSON.getString("defaultValue"));
    	
    	if (node.hasAttr("")) {
    	    fieldJSON.put("dataFieldCompletionAction", node.attr("data-field-completion-action"));
    	}
    	else {
            fieldJSON.put("dataFieldCompletionAction", "NONE");
    	}
        LOG.debug("\tDATA FIELD COMPLETION ACTION==" + fieldJSON.getString("dataFieldCompletionAction"));
    }
    
    /**
     * Parse a label node.
     * 
     * @param fieldJSON The {@link JSONObject} to add the information parsed from the <label> node.
     * @param node The <label> {@link Node}.
     * @throws JSONException
     */
    private void parseLabel(final JSONObject fieldJSON, final Node node) throws JSONException {
    	try {
    		fieldJSON.put("text", parseText(fieldJSON.getString("id"), node));
    	}
    	catch (JSONException e) {
    		throw e;
    	}
    }
    
    /**
     * Parse an <input> node.
     * 
     * @param fieldJSON {@link JSONObject} to add the information parsed from {@code node}.
     * @param inputNode The <input> {@link Node}.
     * @throws JSONException
     */
    private void parseInput(final JSONObject fieldJSON, final Node inputNode) throws JSONException {
        final String type = inputNode.attr("type").toLowerCase();
		switch (type) {
			case "text": {
				parseTextField(fieldJSON, inputNode, false);
				break;
			}
			case "number": {
				parseNumberField(fieldJSON, inputNode);
				break;
			}
            case "date": {
                fieldJSON.put("type", "DATE");
                break;
            }
            case "time": {
                fieldJSON.put("type", "TIME");
                break;
            }
            case "checkbox": {
            	parseSelectionField(SelectionType.CHECKBOX, fieldJSON, inputNode);
            	break;
            }
            case "radio": {
            	parseSelectionField(SelectionType.RADIO, fieldJSON, inputNode);
            	break;
            }
			default: {
			    LOG.warn("Unexpected input type: " + inputNode.toString());
			}
		}
    }
    
    /**
     * Parse a text field.
     * 
     * @param fieldJSON The {@link JSONObject} to add the information parsed from {@code node}.
     * @param node The {@link Node} to parse.
     * @param multipleLines {@code Boolean} to indicate if this text field supports multiple lines.
     * @throws JSONException
     */
    private void parseTextField(final JSONObject fieldJSON, final Node node, final boolean multipleLines) throws JSONException {
        fieldJSON.put("type", "TEXT");
    	fieldJSON.put("multipleLines", multipleLines);
    }
    
    /**
     * Parse a number field.
     * 
     * @param fieldJSON The {@link JSONObject} to add the data parsed from {@code node}.
     * @param node {@link Node} containing the data to be parsed.
     * @throws JSONException
     */
    private void parseNumberField(final JSONObject fieldJSON, final Node node) throws JSONException {
    	// TODO fix min/max
        fieldJSON.put("type", "NUMBER");
        
        if (node.hasAttr("data-field-units")) {
            fieldJSON.put("units", node.attr("data-field-units"));
        }
        else {
            fieldJSON.put("units", "");
        }
        
        if (node.hasAttr("step")) {
            fieldJSON.put("step", node.attr("step"));
        }
        else {
            fieldJSON.put("step", "");
        }
        
        LOG.debug("Parse Number: " + fieldJSON.toString());
    }
    
	/**
	 * Parse the selection field. These are fields that have multiple check box or
	 * radio input elements.
	 * 
	 * @param type
	 *            The {@link SelectionType} to be parsed.
	 * @param fieldJSON
	 *            The {@link JSONObject} for the field data.
	 * @param inputNode
	 *            The {@link Node} of the first input element.
	 * @throws JSONException
	 */
    private void parseSelectionField(final SelectionType type, final JSONObject fieldJSON, final Node inputNode) throws JSONException {
    	fieldJSON.put("type", type.name());
    	fieldJSON.put("id", inputNode.attr("name"));
    	fieldJSON.remove("defaultValue");
    	
    	final JSONArray options = new JSONArray();
        for (Node child : inputNode.parent().childNodes()) {
        	if (child.toString().trim().isEmpty()) {
            	continue;
            }
        	
        	Node input = null;
	    	boolean allowFreeText = false;
        	switch (child.nodeName().toLowerCase()) {
	        	case "input": {
	        		input = child;
	        		break;
	        	}
	        	case "span": {
	                for (Node spanChild : child.childNodes()) {
	                	if (!spanChild.toString().trim().isEmpty() && spanChild.nodeName().toLowerCase().equals("input")) {
		                	if (spanChild.attr("type").equalsIgnoreCase(type.name())) {
		                		input = spanChild;
		                	}
		                	else if (spanChild.attr("type").equalsIgnoreCase("text")) {
		                		allowFreeText = true;
		                	}
	                    }
	                }
	        		break;
	        	}
        	}
        	
        	if (input == null) {
        		continue;
        	}
        	
        	final String id = input.attr("id");
	    	final JSONObject optionJson = parseText(id, input);
	    	optionJson.put("value", input.attr("value"));
	    	optionJson.put("selected", input.hasAttr("checked"));
	    	optionJson.put("allowFreeText", allowFreeText);
	    	options.put(optionJson);
        }
    	fieldJSON.put("options", options);
    	
//    	// All of the nodes should have the same "name" attribute.
//    	final Elements checkboxInputs = document.select("input[name=" + inputNode.attr("name") + "]");
//    	
//    	final JSONArray options = new JSONArray();
//        for (int index = 0; index < checkboxInputs.size(); index++) {
//	    	final Node next = checkboxInputs.get(index);
//        	final String id = next.attr("id");
//	    	final JSONObject optionJson = parseText(id, next);
//	    	optionJson.put("value", next.attr("value"));
//	    	optionJson.put("selected", next.hasAttr("checked"));
//	    	options.put(optionJson);
//	    	
//	    	// Determine if this option allows free text.  If this input has a child "text" input then it allows
//	    	// free text to be entered.
//	    	boolean allowFreeText = false;
//            for (Node child : next.childNodes()) {
//                if (!child.toString().trim().isEmpty() && child.nodeName().toLowerCase().equals("input")) {
//                	allowFreeText = true;
//                    break;
//                }
//            }
//	    	optionJson.put("allowFreeText", allowFreeText);
//        }
//    	fieldJSON.put("options", options);
    }
    
    /**
     * Parse a selection list field.
     * 
     * @param fieldJSON The {@link JSONObject} to add the data parsed from {@code node}.
     * @param node {@link Node} containing the data to be parsed.
     * @throws JSONException
     */
    private void parseSelectionListField(final JSONObject fieldJSON, final Node node) throws JSONException {
    	fieldJSON.put("type", "SELECTION_LIST");
    	
    	if (node.hasAttr("multiple")) {
    	    fieldJSON.put("multipleSelections", node.attr("multiple").equals("multiple"));
    	}
    	else {
            fieldJSON.put("multipleSelections", false);
    	}
    	
    	// Parse the selection list's options.
        final JSONArray options = new JSONArray();
    	for (final Node optionNode : node.childNodes()) {
    	    if (optionNode instanceof TextNode) {
    	        continue;
    	    }
    	    final String id = optionNode.attr("id");
    		options.put(parseText(id, optionNode));
    	}
    	fieldJSON.put("options", options);
    }
    
    private JSONObject parseText(final String id, final Node node) throws JSONException {
        final String value;
        if (node != null && node instanceof Element) {
            value = ((Element) node).text();
        }
        else {
            value = "";
        }
        LOG.debug("Parsing text: '" + value + "'");
        final JSONObject json = new JSONObject();
        json.put("value", value);
        
        // add radlex encoding
        final CodedValue codedValue = this.codedValues.get(id);
        json.put("codeMeaning", (codedValue == null ? "" : codedValue.getCodeMeaning()));
        json.put("codeScheme", (codedValue == null ? "" : codedValue.getCodeScheme()));
        json.put("codeValue", (codedValue == null ? "" : codedValue.getCodeValue()));
        
        return json;
    }
    
    // TEST Coded content
    public static void main(String[] args) throws Exception {
        StringBuilder sb = new StringBuilder();
        try (Scanner scanner = new Scanner(new File("C:/Users/scott/Downloads/0000087.html"))) {
            while (scanner.hasNextLine()) {
                sb.append(scanner.nextLine());
            }
        }
    	final String mrrt = sb.toString();    	
    	System.out.println(mrrt);
    	System.out.println(new ImportMRRT().convertToInternalFormat(mrrt));
    }
}
