/*
 * Copyright (c) 2014, Karos Health Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rsna.trex.servlet;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.rsna.trex.template.CodedValue;

/**
 * {@link BaseServlet} to search for RadLex terms.
 * 
 * @author Scott Jensen
 */
public class RadLexSearchServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    
    private static final Log LOG = LogFactory.getLog(RadLexSearchServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            processRequest(request, response);
        }
        catch (JSONException e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            processRequest(request, response);
        }
        catch (JSONException e) {
            throw new ServletException(e);
        }
    }

    /**
     * Process the RadLex search request.
     * 
     * @param request
     * @param response
     * @throws IOException
     * @throws JSONException
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, JSONException {
        final JSONObject json = new JSONObject();
        final String query = request.getParameter("query");
        LOG.debug("Search for RadLex codes: " + query);
        final JSONArray codes = getCodes(query);
        json.put("codes", codes);
        
        try {
            LOG.debug(json.toString(4));
        }
        catch (JSONException e) {
        }
        
        returnSuccess(response, json);
    }
    
    private JSONArray getCodes(final String searchText) {
        final String queryString = searchText.replaceAll(" ", "%20");
		final String urlString = "https://bioportal.bioontology.org/search/json_search/?q=" + queryString + "&ontologies=RADLEX";
        final JSONArray codedValues = new JSONArray();
        try {
            final URL url = new URL(urlString);
            final HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            final Scanner scanner = new Scanner(new BufferedInputStream(urlConnection.getInputStream()));
            final StringBuilder sb = new StringBuilder();
            while (scanner.hasNextLine()) {
                sb.append(scanner.nextLine());
            }
            final String[] parts = sb.toString().split("~!~");
            for (final String part : parts) {
                LOG.debug(part);
                final String[] codeParts = part.split("\\|");
                final CodedValue codedValue = new CodedValue(null, codeParts[0], codeParts[0], codeParts[0], codeParts[0]);
                final JSONObject code = new JSONObject();
                
                // Value comes back as http://www.owl-ontologies.com/Ontology1415135201.owl#RID123.  Need just
                // the RID123 portion.
                String value = codeParts[4];
                if (value.contains("#")) {
                    value = value.substring(value.lastIndexOf("#") + 1);
                }
                code.put("value", value);
                
                code.put("meaning", codeParts[0]);
                code.put("scheme", codeParts[3]);  // TODO or is this 2?  see form_complete.js line 321
                codedValues.put(code);
            }
        } 
        catch (Exception e ) {
            LOG.error(e);
        }   
        return codedValues;
    }
    
//    public static void main(String[] args) {
//        test();
//    }
}
