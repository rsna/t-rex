<%--
  -- Copyright (c) 2017, Karos Health Incorporated
  -- All rights reserved.
  --
  -- Redistribution and use in source and binary forms, with or without
  -- modification, are permitted provided that the following conditions are met:
  --
  -- 1. Redistributions of source code must retain the above copyright notice,
  -- this list of conditions and the following disclaimer.
  --
  -- 2. Redistributions in binary form must reproduce the above copyright notice,
  -- this list of conditions and the following disclaimer in the documentation
  -- and/or other materials provided with the distribution.
  --
  -- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  -- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  -- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  -- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  -- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  -- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  -- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  -- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  -- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  -- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  -- POSSIBILITY OF SUCH DAMAGE.
  --%>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="com.rsna.trex.language.text" />

		<div id="SELECTION_LIST_FieldModal"
			 class="modal fade field-modal-dialog"
			 style="cursor: default;"  
			 tabindex="-1"
			 role="dialog" 
			 aria-labelledby="fieldModalLabel" 
			 aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">x</span><span class="sr-only"><fmt:message key="close"/></span>
						</button>
						<table>
							<tbody>
								<tr>
									<td>
										<span style="margin-right: 5px;" class="fa fa-list"></span>
									</td>
									<td>
										<span class="modal-title"><fmt:message key="field.selection.list"/></span>  
									</td>
								</tr>
								<tr>
									<td></td>
									<td>
										<span class="modal-subtitle" id="fieldModalSubtitle"><fmt:message key="field.selection.list.hint"/></span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div id="SELECTION_LIST_FieldModalBody" class="modal-body">
						<div>
							<div class="container">
								<div class="row" style="margin-bottom: 5px;">
									<div class="col-md-2"><fmt:message key="text"/>:</div>
									<div class="col-md-10">
										<input id="SELECTION_LIST_FieldInputText" 
											   type="text" 
										       class="modal-field-input">
										<span id="SELECTION_LIST_popover"
										      class="radlex-popover fa fa-link" 
										      style="margin-left: 10px;"
										      onclick="showRadLexPopover('SELECTION_LIST_popover', 'text', 'SELECTION_LIST_FieldInputText')"
										      title="<fmt:message key="link.to.radlex"/>"></span>
									</div>
								</div>
								<div class="row" style="margin-bottom: 5px;">
									<div class="col-md-2"><fmt:message key="tooltip"/>:</div>
									<div class="col-md-10">
										<input id="SELECTION_LIST_FieldTooltipInput" 
										       type="text" 
										       class="modal-field-input">
									</div>
								</div>
								<div class="row" style="margin-bottom: 5px;">
									<div class="col-md-2"><fmt:message key="dialog.selection.list.options"/>:</div>
									<div class="col-md-4">
										<table id="SELECTION_LIST_FieldOptionsList" class="table table-bordered" style="max-width: 645px;">
										</table>
									</div>
								</div>
								<div class="row" style="margin-bottom: 5px;">
									<div class="col-md-2"></div>
									<div class="col-md-10">
										<div>
											<button type="button" class="btn btn-default" onclick="addSelectionListOption('SELECTION_LIST', null, false);"><fmt:message key="dialog.selection.list.add.option"/></button>
										</div>
									</div>
								</div>
								<div class="row" style="margin-bottom: 5px;">
									<div class="col-md-2"><fmt:message key="completion.action"/>:</div>
									<div class="col-md-10">
										<select id="SELECTION_LIST_FieldCompletionAction">
											<option id="SELECTION_LIST_FieldCompletionAction_NONE" value="NONE"><fmt:message key="completion.action.none"/></option>
											<option id="SELECTION_LIST_FieldCompletionAction_ALERT" value="ALERT"><fmt:message key="completion.action.alert"/></option>
											<option id="SELECTION_LIST_FieldCompletionAction_PROHIBIT" value="PROHIBIT"><fmt:message key="completion.action.prohibit"/></option>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-md-2"><fmt:message key="dialog.selection.list.selection.type"/>:</div>
									<div class="col-md-10">
										<input id="SELECTION_LIST_SingleSelection" 
										       type="RADIO" 
										       name="SELECTION_LIST_SelectionType" 
										       class="field-option-input"
										       onclick="$('#SELECTION_LIST_AllowFreeText').removeAttr('disabled'); $('#SELECTION_LIST_AllowFreeText_label').removeClass('field-text-display-diabled')">
										<label class="selection-type-label" for="SELECTION_LIST_SingleSelection"><fmt:message key="dialog.selection.list.single.selection"/></label>
									</div>
								</div>
								<div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-10">
										<input id="SELECTION_LIST_AllowFreeText" type="CHECKBOX" class="field-option-input allow-free-text-checkbox">
										<label class="selection-type-label" for="SELECTION_LIST_AllowFreeText">
											<span id="SELECTION_LIST_AllowFreeText_label"><fmt:message key="dialog.selection.list.allow.free.text"/></span>
										</label>
									</div>
								</div>
								<div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-10">
										<input id="SELECTION_LIST_MultipleSelections" 
										       type="RADIO" 
										       name="SELECTION_LIST_SelectionType" 
										       class="field-option-input"
										       onclick="$('#SELECTION_LIST_AllowFreeText').attr('disabled', 'disabled'); $('#SELECTION_LIST_AllowFreeText_label').addClass('field-text-display-diabled')">
										<label class="selection-type-label" for="SELECTION_LIST_MultipleSelections"><fmt:message key="dialog.selection.list.multiple.selections"/></label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button id="SELECTION_LIST_FieldModelSaveButton"
						        type="button"
							    class="btn btn-primary trexButton"
							    onclick="saveSelectionListField();"
								style="visibility: visible; display: inline;"><fmt:message key="save"/></button>
						<button id="SELECTION_LIST_FieldModelDeleteButton"
						        type="button"
							    class="btn btn-default"
							    onclick="deleteCurrentField(); $('#SELECTION_LIST_FieldModal').modal('hide');"
								style="visibility: visible; display: none;"><fmt:message key="delete"/></button>
						<button id="SELECTION_LIST_FieldModalCloseButton"
						        type="button"
								class="btn btn-default"
								data-dismiss="modal"
								onclick="if (draggedItem) { draggedItem.detach(); }"><fmt:message key="cancel"/></button>
					</div>
				</div>
			</div>
		</div>