<%--
  -- Copyright (c) 2017, Karos Health Incorporated
  -- All rights reserved.
  --
  -- Redistribution and use in source and binary forms, with or without
  -- modification, are permitted provided that the following conditions are met:
  --
  -- 1. Redistributions of source code must retain the above copyright notice,
  -- this list of conditions and the following disclaimer.
  --
  -- 2. Redistributions in binary form must reproduce the above copyright notice,
  -- this list of conditions and the following disclaimer in the documentation
  -- and/or other materials provided with the distribution.
  --
  -- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  -- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  -- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  -- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  -- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  -- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  -- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  -- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  -- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  -- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  -- POSSIBILITY OF SUCH DAMAGE.
  --%>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="com.rsna.trex.language.text" />

        <div id="TIME_FieldModal"
		     class="modal fade field-modal-dialog"
			 style="cursor: default;" 
			 tabindex="-1"
			 role="dialog" 
			 aria-labelledby="fieldModalLabel" 
			 aria-hidden="false">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">x</span><span class="sr-only"><fmt:message key="close"/></span>
						</button>
						<table>
							<tbody>
								<tr>
									<td>
										<span style="margin-right: 5px;" class="fa fa-clock-o"></span>
									</td>
									<td>
										<span class="modal-title"><fmt:message key="field.time"/></span>
										<span class="modal-subtitle" id="fieldModalSubtitle"></span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="modal-body">
						<div>
							<div class="container">
								<div class="row" style="margin-bottom: 5px;">
									<div class="col-md-2"><fmt:message key="text"/>:</div>
									<div class="col-md-10">
										<input id="TIME_FieldInputText" 
										       type="text" 
										       class="modal-field-input">
										<span id="TIME_popover"
										      class="radlex-popover fa fa-link" 
										      style="margin-left: 10px;"
										      onclick="showRadLexPopover('TIME_popover', 'text', 'TIME_FieldInputText')"
										      title="<fmt:message key="link.to.radlex"/>"></span>
									</div>
								</div>
								<div class="row" style="margin-bottom: 5px;">
									<div class="col-md-2"><fmt:message key="default.value"/>:</div>
									<div class="col-md-10">
										<input id="TIME_FieldDefaultValueInput" 
										       type="TIME" 
										       class="modal-field-input">
									</div>
								</div>
								<div class="row" style="margin-bottom: 5px;">
									<div class="col-md-2"><fmt:message key="tooltip"/>:</div>
									<div class="col-md-10">
										<input id="TIME_FieldTooltipInput" 
										       type="text" 
										       class="modal-field-input">
									</div>
								</div>
								<div class="row" style="margin-bottom: 5px;">
									<div class="col-md-2"><fmt:message key="completion.action"/>:</div>
									<div class="col-md-10">
										<select id="TIME_FieldCompletionAction">
											<option id="TIME_FieldCompletionAction_NONE" value="NONE" title="test"><fmt:message key="completion.action.none"/></option>
											<option id="TIME_FieldCompletionAction_ALERT" value="ALERT"><fmt:message key="completion.action.alert"/></option>
											<option id="TIME_FieldCompletionAction_PROHIBIT" value="PROHIBIT"><fmt:message key="completion.action.prohibit"/></option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button id="TIME_FieldModelSaveButton" 
						        type="button"
							    class="btn btn-primary trexButton" 
							    onclick="saveTimeField();"
								style="visibility: visible; display: inline;"><fmt:message key="save"/></button>
						<button id="TIME_FieldModelDeleteButton" 
						        type="button"
							    class="btn btn-default" 
							    onclick="deleteCurrentField(); $('#TIME_FieldModal').modal('hide');"
								style="visibility: visible; display: none;"><fmt:message key="delete"/></button>
						<button id="TIME_FieldModalCloseButton" 
						        type="button"
								class="btn btn-default" 
								data-dismiss="modal"
								onclick="if (draggedItem) { draggedItem.detach(); }"><fmt:message key="cancel"/></button>
					</div>
				</div>
			</div>
		</div>