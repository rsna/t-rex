<%--
  -- Copyright (c) 2017, Karos Health Incorporated
  -- All rights reserved.
  --
  -- Redistribution and use in source and binary forms, with or without
  -- modification, are permitted provided that the following conditions are met:
  --
  -- 1. Redistributions of source code must retain the above copyright notice,
  -- this list of conditions and the following disclaimer.
  --
  -- 2. Redistributions in binary form must reproduce the above copyright notice,
  -- this list of conditions and the following disclaimer in the documentation
  -- and/or other materials provided with the distribution.
  --
  -- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  -- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  -- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  -- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  -- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  -- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  -- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  -- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  -- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  -- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  -- POSSIBILITY OF SUCH DAMAGE.
  --%>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="com.rsna.trex.language.text" />

		<div id="import_template_modal" 
		     class="modal fade field-modal-dialog"
			 style="cursor: default;" 
			 tabindex="-1"
			 role="dialog" 
			 aria-labelledby="fieldModalLabel" 
			 aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">x</span><span class="sr-only"><fmt:message key="close"/></span>
						</button>
						<table>
							<tbody>
								<tr>
									<td>
										<span style="margin-right: 5px;" class="glyphicon glyphicon-import"></span>
									</td>
									<td>
										<span class="modal-title" id="importTemplateModalTitle"><fmt:message key="import.template.title"/></span> 
										<span class="modal-subtitle" id="importTemplateModalSubtitle"></span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div id="import_template_modalBody" class="modal-body">
						<div class="container">
							<input type="file" id="importTemplateFileInput" name="file" />
						</div>
					</div>
					<div class="modal-footer">
						<button id="import_template_modal_import_button" 
						        type="button"
							    class="btn btn-primary trexButton" 
							    onclick="importTemplate();"
								style="visibility: visible; display: inline;"><fmt:message key="import"/></button>
						<button id="import_template_modalCloseButton" 
						        type="button"
							    class="btn btn-default" 
							    data-dismiss="modal"
								onclick="if (draggedItem) { draggedItem.detach(); }"><fmt:message key="close"/></button>
					</div>
				</div>
			</div>
		</div>

		<script>
			function importTemplate() {
			    var files = document.getElementById('importTemplateFileInput').files;
			    if (!files.length) {
			      	bootbox.alert('<fmt:message key="worklist.import.select.file"/>', function() {});
			      	return;
			    } 
			    
			    var reader = new FileReader();
	
			    // If we use onloadend, we need to check the readyState.
			    reader.onloadend = function(evt) {
			    	// Get selected MRRT
			    	var mrrtToImport = evt.target.result;
			    	
			    	// send to server to convert to internal format
					$.post('import', mrrtToImport)
						.done(function(data) {
							var template = jQuery.parseJSON(data);
// 							alert(JSON.stringify(template.layout));
					    	var templates = jQuery.parseJSON(localStorage.templates);

			    			var overwriteFunction = function() {
			    				addTemplateToLocalStorage(templates, template);
				    		};

			    			var importAsNewFunction = function() {
			    				template.metadata.id = 'T' + (new Date()).getTime();
                                addTemplateToLocalStorage(templates, template);
				    		};
				    		
							if (templates[template.metadata.id]) {
				    			bootbox.dialog({
				    				message: '<fmt:message key="template.already.exists.warning"/>',
				    				title: '<fmt:message key="confirmation"/>',
				    				buttons: {
				    					success: {
				    						label: '<fmt:message key="overwrite"/>',
				    						className: 'btn-primary',
				    						callback: overwriteFunction 
				    					},
				    					importasnew: {
				    						label: '<fmt:message key="import.as.new"/>',
				    						className: 'btn-primary',
				    						callback: importAsNewFunction 
				    					},
				    				    main: {
				    				        label: '<fmt:message key="cancel"/>',
					    					className: 'btn-default'
				    				    }
				    				}
				    			});
							}
							else {
								overwriteFunction();
							}
						})
						.error(function(xhr, ajaxOptions, thrownError) {
// 							displayError(xhr.status);
// 							displayError(ajaxOptions);
							displayError('<fmt:message key="worklist.import.failed"/><p><p>' + 
									xhr.responseText.replace(/</g, "&lt;").replace(/>/g, "&gt;"));
						});
		    	};

		    	reader.readAsBinaryString(files[0]);
		  	};
			
			function addTemplateToLocalStorage(templates, template) {
                upgradeTemplate(template);
                templates[template.metadata.id] = template;
                localStorage.templates = JSON.stringify(templates);
                window.open('index.jsp?template=' + template.metadata.id + '&t=' + new Date().getTime(), '_self');
			};
		</script>
