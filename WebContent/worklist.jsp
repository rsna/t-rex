<%--
  -- Copyright (c) 2017, Karos Health Incorporated
  -- All rights reserved.
  --
  -- Redistribution and use in source and binary forms, with or without
  -- modification, are permitted provided that the following conditions are met:
  --
  -- 1. Redistributions of source code must retain the above copyright notice,
  -- this list of conditions and the following disclaimer.
  --
  -- 2. Redistributions in binary form must reproduce the above copyright notice,
  -- this list of conditions and the following disclaimer in the documentation
  -- and/or other materials provided with the distribution.
  --
  -- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  -- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  -- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  -- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  -- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  -- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  -- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  -- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  -- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  -- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  -- POSSIBILITY OF SUCH DAMAGE.
  --%>
<%@page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="com.rsna.trex.language.text" />

<%
	//Get version of application
	java.util.Properties prop = new java.util.Properties();
	prop.load(getServletContext().getResourceAsStream("/META-INF/MANIFEST.MF"));
	String applicationVersion = prop.getProperty("Implementation-Version"); 
	
	final String templateIdParameter = request.getParameter("template");
%>

<!DOCTYPE html>
<html lang="${language}">

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title><fmt:message key="application.name"/></title>

		<link rel='shortcut icon' href='images/favicon.ico?version=<%= applicationVersion %>' type="image/x-icon">
		<link rel='icon' href='images/favicon.ico?version=<%= applicationVersion %>' type="image/x-icon">

		<link rel='stylesheet' href='thirdparty/bootstrap/3.3.4/css/bootstrap.min.css?version=<%= applicationVersion %>' />
		<link rel='stylesheet' href='thirdparty/bootstrap/3.3.4/css/bootstrap-theme.min.css?version=<%= applicationVersion %>' />
		<link rel='stylesheet' href='thirdparty/jquerysortable/jquery-sortable.css?version=<%= applicationVersion %>' />
		<link rel='stylesheet' href='thirdparty/ihe/IHE_Template_Style.css?version=<%= applicationVersion %>' />
		<link rel='stylesheet' href='css/style.css?version=<%= applicationVersion %>' />

	    <script src='thirdparty/jquery/jquery-1.11.2.min.js?version=<%= applicationVersion %>'></script>
  		<script src='thirdparty/jquery/jquery-ui.js?version=<%= applicationVersion %>'></script>
	    <script src='thirdparty/bootstrap/3.3.4/js/bootstrap.min.js?version=<%= applicationVersion %>'></script>

	    <!-- Provides ability for context menus. -->
	    <script src='thirdparty/contextjs/context.js?version=<%= applicationVersion %>'></script>
		<link rel='stylesheet' href='thirdparty/contextjs/context.css?version=<%= applicationVersion %>' />

		<!-- Needed for utilities.jsp -->
		<script src='thirdparty/eligrey/FileSaver.min.js?version=<%= applicationVersion %>'></script>
		<script src='thirdparty/eligrey/Blob.js?version=<%= applicationVersion %>'></script>
		<script src='thirdparty/bootbox/bootbox.min.js?version=<%= applicationVersion %>'></script>

		<jsp:include page='utilities.jsp' />

	    <script type="text/javascript">
	    	var selectedTemplateId = '<%= templateIdParameter == null ? "" : templateIdParameter %>';
	    	var fileReaderSupported = (window.File && window.FileReader && window.FileList && window.Blob);

	    	/**
	    	 * Initialize the list of templates.  This will get the list of templates that have been stored
	    	 * in the browser's local storage and list them on the page.
	    	 */
	    	function initialize() {
	    		context.init({
	    			preventDoubleContext: true,
	    			above: 'auto'
	    		});
	    		setupBodyContextMenu();

	    		updateLocalStorage();
	    		addExistingTemplatesToTable();

				// If the browser does not support FileReader then hide the import and export buttons.
				if (!fileReaderSupported) {
					$('#importButton').hide();
					$('#exportButton').hide();
				}

	    		$(function() {
	    		    $('#templateTable').on('contextmenu', 'tr', function(e) {
	    				selectTemplateRow(this.id);
	    		    });
	    		});
	    	};

			/**
			 * Display a dialog giving the user some information about T-Rex.
			 */
			var setShowWelcomePage = function() {
				localStorage.showWelcomePage = $('#DisplayWelcomeOnStartUp').is(':checked');
			};

			var showHowToVideo = function() {
    			bootbox.dialog({
    				message:
    					'<iframe width="560" height="315" src="https://www.youtube.com/embed/1233DzjO8wk" frameborder="0" allowfullscreen></iframe>',
    				title: '<fmt:message key="overview.video.dialog.title"/>',
    				buttons: {
    					success: {
    						label: '<fmt:message key="close"/>',
    						className: 'btn-primary'
    				    }
    				}
    			});
    			return false;
			};

	    	/**
	    	 * Get the existing templates from local storage and create an entry in the table for each.
	    	 */
	    	function addExistingTemplatesToTable() {
	    		var tableBody = document.getElementById('templateTableBody');
	    		var templates = jQuery.parseJSON(localStorage.templates);

	    		// Create an array of the templates so they can be sorted.
	    		var templatesArr = [];
                for (var id in templates) {
                    templatesArr.push(templates[id]);
                }
	    		templatesArr.sort(function(a, b) {
	    		    var keyA = a.metadata.createdTimestamp;
	    		    var keyB = b.metadata.createdTimestamp;
	    		    if (keyA < keyB) return -1;
	    		    if (keyA > keyB) return 1;
	    		    return 0;
	    		});

	    		for (var index = 0; index < templatesArr.length; index++) {
	    			var template = templatesArr[index];

	    			var row = document.createElement('tr');
	    			row.setAttribute('id', template.metadata.id);
	    			row.appendChild(createTableCell(template, 'title', 'worklistTitleColumn'));
	    			row.appendChild(createTableCell(template, 'creator', 'worklistAuthorColumn'));
	    			row.appendChild(createTableCell(template, 'status', 'worklistStatusColumn'));
	    			row.appendChild(createTableCell(template, 'createdTimestamp', 'worklistCreatedColumn', true));
	    			row.appendChild(createTableCell(template, 'lastModifiedTimestamp', 'worklistLastModifiedColumn', true));
	    			row.appendChild(createTableCell(template, 'description', 'worklistDescriptionColumn'));
	    			tableBody.appendChild(row);

	    			if (template.metadata.id == selectedTemplateId) {
	    				selectTemplateRow(row.id);
	    			}

	    			row.onclick = function() {
	    				selectTemplateRow(this.id);
	    			};

	    			row.ondblclick = function() {
	    				selectTemplateRow(this.id);
	    				editTemplate();
	    			};

	    			setupContextMenu(row.getAttribute('id'));
	    		}
	    	}

	    	function selectTemplateRow(templateId) {
	    		var clazz = 'selectedRow';
	    		var previousSelected = document.getElementById(selectedTemplateId);
	    		if (previousSelected !== undefined && previousSelected !== null) {
	    			removeClass(previousSelected, clazz);
	    		}

	    		selectedTemplateId = templateId;
	    		var row = document.getElementById(templateId);
	    		addClass(row, clazz);
			}

	    	function setupContextMenu(elementId) {
	    		context.attach('#' + elementId, [ {
	    				text: '<fmt:message key="worklist.action.new"/>',
	    				action: function(e) {
	    					newTemplate();
	    				}
	    			},{
	    				divider: true
	    			},{
	    				text: '<fmt:message key="worklist.action.edit"/>',
	    				action: function(e) {
	    					editTemplate();
	    				}
	    			},{
	    				text: '<fmt:message key="worklist.action.preview"/>',
	    				action: function(e) {
	    					previewExistingTemplate();
	    				}
	    			},{
	    				divider: true
	    			},{
	    				text: '<fmt:message key="worklist.action.delete"/>',
	    				action: function(e) {
	    					deleteTemplate();
	    				}
	    			},{
	    				divider: true
	    			},{
	    				text: '<fmt:message key="worklist.action.import"/>',
	    				action: function(e) {
	    					showImportTemplate();
	    				}
	    			},{
	    				text: '<fmt:message key="worklist.action.export"/>',
	    				action: function(e) {
	    					downloadTemplateMRRT();
	    				}
	    			}
	    		]);
				context.settings({compress: true});
	    	}

	    	function setupBodyContextMenu() {
	    		context.attach('body', [ {
	    				text: '<fmt:message key="worklist.action.new"/>',
	    				action: function(e) {
	    					newTemplate();
	    				}
	    			},{
	    				divider: true
	    			},{
	    				text: '<fmt:message key="worklist.action.import"/>',
	    				action: function(e) {
	    					showImportTemplate();
	    				}
	    			}
	    		]);
	    		context.settings({compress: true});
	    	}

	    	/**
	    	 * Create a <td> element containing the template metadata.

	    	 * @param template The JSON object representing the template.
	    	 * @param metadataAttribute The attribute from the Template's metadata table to display in the cell.
	    	 * @param clazz The class for the cell.
	    	 * @param isDate Boolean indicating if this represents a date and time.
	    	 * @return A <td> element object.
	    	 */
	    	function createTableCell(template, metadataAttribute, clazz, isDate) {
    			var cell = document.createElement('td');
    			cell.setAttribute('class', clazz);
    			var value = template.metadata[metadataAttribute];
    			if (value && isDate) {
    				var d = new Date(value);
    				value = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
    			}
    			cell.appendChild(document.createTextNode(value));
    			return cell;
	    	}

    		/**
	    	 * If the local storage contains a template in the old location then it needs to be updated.  This will:
	         * 1) remove the old template from local storage
	         * 2) update the template to new format (if needed)
	         * 3) add the template back into local storage in the proper location
	    	 */
	    	function updateLocalStorage() {
	    		if (localStorage.templateData) {
	    			var templateData = jQuery.parseJSON(localStorage.templateData);
	    			
	    			// Make sure the old version template has all the new version's metadata
	    			var attributes = ['publisher', 'description', 'rights', 'license', 'creator', 
	    			                  'contributors', 'email', 'status'];
	    			for (var i = 0; i < attributes.length; i++) {
	    				var key = attributes[i];
	    				var value = templateData.metadata[key];
	                    if (value === undefined || value === null) {
	                    	templateData.metadata[key] = '';
	                    }
	    			}
	    			
	    			var templates = {};
	    			templates[templateData.metadata.id] = templateData;
	    			localStorage.templates = JSON.stringify(templates);
	    			localStorage.removeItem('templateData');
	    		}
	    		else if (!localStorage.templates) {
	    			localStorage.templates = JSON.stringify({});
	    		}

	    		// Make sure all the templates are at the correct version.
    			var templates = jQuery.parseJSON(localStorage.templates);
	    		for (var id in templates) {
	    			var template = templates[id];
	    			upgradeTemplate(template);
	    			templates[id] = template;
	    		}
    			localStorage.templates = JSON.stringify(templates);
	    	}

	        /**
	         * Start editing a new template.  This will display the template editor with a blank template.
	         */
	        function newTemplate() {
	    		window.open("editor.jsp?language=${language}&t=" + new Date().getTime(), "_self");
	        }

	        /**
	         * Preview a template.  This will display a preview of the currently selected template in a modal dialog.
	         */
	    	function previewExistingTemplate() {
	    		var templateId = getSelectedTemplateId();
                if (templateId) {
                    previewTemplate(templateId);
                }
                else {
                    bootbox.alert('<fmt:message key="worklist.download.select.template"/>', function() {});
                }
	    	}

	    	function downloadTemplateMRRT() {
	    		var templateId = getSelectedTemplateId();
	    		if (templateId) {
	    			exportMRRT(templateId);
	    		}
	    		else {
	    			bootbox.alert('<fmt:message key="worklist.download.select.template"/>', function() {});
	    		}
	    	};

	        /**
	         * Edit a template.  This will display the currently selected template in the editor.
	         */
	    	function editTemplate() {
	    		var id = getSelectedTemplateId();
	    		if (id) {
                    window.open("editor.jsp?language=${language}&id=" + id + "&t=" + new Date().getTime(), "_self");
	    		}
	    		else {
                    bootbox.alert('<fmt:message key="worklist.edit.select.template"/>', function() {});
	    		}
	    	}

	    	function deleteTemplate() {
	    		var id = getSelectedTemplateId();
	    		if (id) {
	    			var callbackFunction = function() {
		    			var templates = jQuery.parseJSON(localStorage.templates);
		    			delete templates[id];
		    			localStorage.templates = JSON.stringify(templates);
		    			var row = document.getElementById(id);
		    			document.getElementById('templateTable').deleteRow(row.rowIndex);
		    			// location.reload();
		    		};

	    			bootbox.dialog({
	    				message : '<fmt:message key="worklist.delete.warning"/>',
	    				title : '<fmt:message key="confirmation"/>',
	    				buttons : {
	    					success : {
	    						label : '<fmt:message key="delete"/>',
	    						className : 'btn-primary',
	    						callback: callbackFunction
	    					},
	    				    main: {
	    				        label: '<fmt:message key="cancel"/>',
		    					className: 'btn-default'
	    				    }
	    				}
	    			});
	    		}
	    		else {
	    			bootbox.alert('<fmt:message key="worklist.delete.select.template"/>', function() {});
	    		}
	    	}

		    function getSelectedTemplateId() {
                if (selectedTemplateId === undefined || selectedTemplateId === null || selectedTemplateId === '') {
                	return null;
                }
		    	return selectedTemplateId;
		    }

		    function showImportTemplate() {
				$('#import_template_modal').modal({ backdrop: 'static', keyboard: false });
		    }

            function languageSelected(language) {
            	var url = 'worklist.jsp?language=' + language + '&t=' + new Date().getTime();
                window.open(url, '_self');
            };
	    </script>
	</head>

	<body onload="initialize()">

        <jsp:include page='navbar.jsp' />

        <div style="top: 78px; left: 10px; right: 10px; position: absolute; overflow: hidden;">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="worklistTitleColumn"><fmt:message key="worklist.column.title"/></th>
                        <th class="worklistAuthorColumn"><fmt:message key="worklist.column.author"/></th>
                        <th class="worklistStatusColumn"><fmt:message key="worklist.column.status"/></th>
                        <th class="worklistCreatedColumn"><fmt:message key="worklist.column.created"/></th>
                        <th class="worklistLastModifiedColumn"><fmt:message key="worklist.column.lastModified"/></th>
                        <th class="worklistDescriptionColumn"><fmt:message key="worklist.column.description"/></th>
                    </tr>
                </thead>
            </table>
        </div>

        <div style="top: 115px; left: 10px; right: 10px; bottom: 60px; position: absolute; overflow: auto;">
            <table id="templateTable" class="table table-bordered">
                <tbody id="templateTableBody">
                </tbody>
            </table>
        </div>

		<div class="footer">
			<div class="button-group footer-button-group">
				<button type="button" class="btn btn-primary trexButton" onclick="newTemplate();" title='<fmt:message key="worklist.action.new.tooltip"/>'><fmt:message key="worklist.action.new"/></button>
				<button type="button" class="btn btn-primary trexButton" onclick="editTemplate();" title='<fmt:message key="worklist.action.edit.tooltip"/>'><fmt:message key="worklist.action.edit"/></button>
				<button type="button" class="btn btn-primary trexButton" onclick="previewExistingTemplate();" title='<fmt:message key="worklist.action.preview.tooltip"/>'><fmt:message key="worklist.action.preview"/></button>
				<button type="button" class="btn btn-primary trexButton" onclick="deleteTemplate();" title='<fmt:message key="worklist.action.delete.tooltip"/>'><fmt:message key="worklist.action.delete"/></button>
				<button id="importButton" type="button" class="btn btn-primary trexButton" onclick="showImportTemplate();" title='<fmt:message key="worklist.action.import.tooltip"/>'><fmt:message key="worklist.action.import"/></button>
				<button id="exportButton" type="button" class="btn btn-primary trexButton" onclick="downloadTemplateMRRT();" title='<fmt:message key="worklist.action.export.tooltip"/>'><fmt:message key="worklist.action.export"/></button>
			</div>
			<div class="footer-version">
				<fmt:message key="version"/> <%= applicationVersion %>
			</div>
		</div>

		<jsp:include page='dialogs/importDialog.jsp' />

	</body>

</html>