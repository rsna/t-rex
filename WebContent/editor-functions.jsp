<%--
  -- Copyright (c) 2017, Karos Health Incorporated
  -- All rights reserved.
  --
  -- Redistribution and use in source and binary forms, with or without
  -- modification, are permitted provided that the following conditions are met:
  --
  -- 1. Redistributions of source code must retain the above copyright notice,
  -- this list of conditions and the following disclaimer.
  --
  -- 2. Redistributions in binary form must reproduce the above copyright notice,
  -- this list of conditions and the following disclaimer in the documentation
  -- and/or other materials provided with the distribution.
  --
  -- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  -- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  -- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  -- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  -- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  -- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  -- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  -- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  -- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  -- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  -- POSSIBILITY OF SUCH DAMAGE.
  --%>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="com.rsna.trex.language.text" />

<script>
	var draggedItem;
	var draggedSuper;
	
	// Keep track of currently displayed popover so that it can be hidden if modal closed.
	var currentPopoverElementId;
	
	/**
	 * Used for setting unique identifiers that are only unique within the scope of the template.  When used should
	 * make sure to perform a post increment.
	 */
	var nextUID = (new Date()).getTime();
	
	/**
	 * JSON to keep track of the template's data.
	 */
	var templateData;
	
	/**
	 * The section (tab) currently being displayed.
	 */
	var currentlyDisplayedSection;
	
	/**
	 * Keep track of the Field that is currently being edited.  This is the JSON.
	 */
	var currentlyEditedField;
	
	/**
	 * Keep track of the ID of the field that has been copied.
	 */
	var copiedFieldId;
	
	/**
	 * Any time the body of the template is modified the ID must be updated.  This only needs to be done once
	 * for all of the changes.  This boolean keeps track to determine if the template's ID has already been
	 * updated.
	 */
	var hasIdAlreadyBeenModified = false;
	
	/**
	 * Definition of the fields that can be added to a template.
	 *
	 * ordinal: used to sort the field types for display order
	 */
    var fieldTypes = {
        'SECTION_HEADER': {
            'ordinal': 0,
            'type': 'SECTION_HEADER',
            'name': '<fmt:message key="field.section.header"/>',
            'hint': '<fmt:message key="field.section.header.hint"/>',
            'icon': 'fa fa-header',
        },
        'TEXT': {
            'ordinal': 1,
            'type': 'TEXT',
            'name': '<fmt:message key="field.text"/>',
            'hint': '<fmt:message key="field.text.hint"/>',
            'icon': 'fa fa-font',
        },
        'NUMBER':   {
            'ordinal': 2,
            'type': 'NUMBER',
            'name': '<fmt:message key="field.number"/>',
            'hint': '<fmt:message key="field.number.hint"/>',
            'icon': 'glyphicon glyphicon-sound-5-1',
        },
        'SELECTION_LIST': {
            'ordinal': 3,
            'type': 'SELECTION_LIST',
            'name': '<fmt:message key="field.selection.list"/>',
            'hint': '<fmt:message key="field.selection.list.hint"/>',
            'icon': 'fa fa-caret-square-o-down',
            'step': '2'
        },
        'CHECKBOX': {
            'ordinal': 4,
            'type': 'CHECKBOX',
            'name': '<fmt:message key="field.checkbox"/>',
            'hint': '<fmt:message key="field.checkbox.hint"/>',
            'icon': 'fa fa-check-square-o',
            'step': '3'
        },
        'RADIO': {
            'ordinal': 5,
            'type': 'RADIO',
            'name': '<fmt:message key="field.radio"/>',
            'hint': '<fmt:message key="field.radio.hint"/>',
            'icon': 'fa fa-dot-circle-o',
            'step': '4'
        },
        'DATE': {
            'ordinal': 6,
            'type': 'DATE',
            'name': '<fmt:message key="field.date"/>',
            'hint': '<fmt:message key="field.date.hint"/>',
            'icon': 'fa fa-calendar'
        },
        'TIME': {
            'ordinal': 7,
            'type': 'TIME',
            'name': '<fmt:message key="field.time"/>',
            'hint': '<fmt:message key="field.time.hint"/>',
            'icon': 'fa fa-clock-o',
        },
        'RADELEMENT_CDE' :{
        	'ordinal': 8,
            'type': 'RADELEMENT_CDE',
            'name': '<fmt:message key="field.radelement_cde.name"/>',
            'hint': '<fmt:message key="field.radelement_cde.hint"/>',
            'icon': 'fa fa-file-text',
        }
    };
	
	/**
	 * This function should be called when page first displayed to display the contents of the template.
	 */
	function displayTemplate(templateId) {
		var templates = jQuery.parseJSON(localStorage.templates);
		if (templateId) {	
			templateData = templates[templateId];
		}
		else {
			templateData = buildBlankTemplate();
			templates[templateData.metadata.id] = templateData;
			localStorage.templates = JSON.stringify(templates);
			
			// No need to update the ID on modifications for new templates.
			hasIdAlreadyBeenModified = true;
		}
		$('#TemplateNameInput').val(templateData.metadata.title);
		$('#TemplateNameInput').select();
		$('#TemplateDescriptionInput').val(templateData.metadata.description);
        $('#TemplateAuthorInput').val(templateData.metadata.creator);
        $('#TemplatePublisherInput').val(templateData.metadata.publisher);
		$('#TemplateAuthorEmailInput').val(templateData.metadata.email);
		$('#TemplateLanguageSelect').val(templateData.metadata.language.toUpperCase());
		var contributors = '';
		var contributorsArray = templateData.metadata.contributors;
		if (contributorsArray !== undefined && contributorsArray !== null) {
			for (var i = 0; i < templateData.metadata.contributors.length; i++) {
				if (i > 0) {
					contributors += '\n';
				}
				contributors += templateData.metadata.contributors[i];
			}
		}
		$('#TemplateContributorsInput').val(contributors);
		
		// Get all of the 1st level sections and add them as tabs.
		var tabs = document.getElementById('templateSectionsTabs');
		var tabPanels = document.getElementById('templateSectionsTabPanels');
		var activeLink;
		for (var i = 0; i < templateData.layout.length; i++) {
			var node = templateData.layout[i];
			if (node === null) {
				continue;
			}
			
			var field = templateData.fields[node.id];
			
			if (field.type != 'SECTION_HEADER') {
				continue;
			}
			
			// Add the tab
			var link = document.createElement('a');
			link.setAttribute('href', '#' + field.id);
			link.setAttribute('role', 'tab');
			link.setAttribute('data-toggle', 'tab');
			link.setAttribute('field', field.id);
			link.appendChild(document.createTextNode(field.text.value));
			if (field.id.toLowerCase() == 'findings') {
				activeLink = link;
			}
			var li = document.createElement('li');
			li.setAttribute('field-id', field.id);
			li.setAttribute('name', 'SectionTabListItem');
			li.appendChild(link);
			tabs.appendChild(li);
			
			// Add the tab panel.
			var panelDiv = document.createElement('div');
			panelDiv.id = field.id;
			panelDiv.className = 'tab-pane fade template-section';
			addSectionTabPanel(panelDiv, field, node);
			tabPanels.appendChild(panelDiv);
		}
		
		$('#templateSectionsTabs a').click(function (e) {
			  e.preventDefault();
			  $(this).tab('show');
			  currentlyDisplayedSection = e.target.getAttribute('field');
			  
			  // When the displayed section changes, make sure to update the predefined fields that are 
			  // available to drag and drop into the section.
			  // displayPredefinedQuestionsForSection(e.target.getAttribute('field'));
		});
		
		if (activeLink) {
			activeLink.click();
		}
	};
	
	/**
	 * 
	 * @param sectionHeaderField The section header field.
	 */
	function addSectionTabPanel(sectionHeaderDiv, sectionHeaderField, node) {
		// Add the tab panel.
		var dropTarget = document.createElement('ol');
		dropTarget.id = sectionHeaderField.id + '_FIELDS';
		// default vertical needed for sortable
		dropTarget.className = 'default vertical template-section-field-list';
		dropTarget.setAttribute('name', 'DropTarget');
	
		sectionHeaderDiv.setAttribute('name', 'section_details');
		sectionHeaderDiv.appendChild(dropTarget);
		
		// Add all of the section's children to the tab panel 
		if (node.children) {
			for (var index = 0; index < node.children.length; index++) {
				var childNode = node.children[index];
				var field = templateData.fields[childNode.id];
				if (field) {
					addFieldToTemplateSectionDisplay(dropTarget, field, childNode);
				}
			}
			
			if (node.children.length <= 1) {
				var instructionsDiv = document.createElement('div');
				instructionsDiv.id = sectionHeaderField.id + '_Instructions';
				instructionsDiv.className = 'template-sections-instructions';
				instructionsDiv.appendChild(document.createTextNode('<fmt:message key="drag.and.drop.instructions"/>'));
				sectionHeaderDiv.appendChild(instructionsDiv);
			}
		}
	};
	
	/**
	 * This will create a new Field based on the field type set in the data transfer object.
	 *  
	 * @param ev
	 */
	function drop(item, _super, isNewField) {
		if (!isNewField) {
			var clonedItem = $('<li/>').css({
				height : 0
			});
			item.before(clonedItem);
			clonedItem.animate({
				'height' : item.height()
			});
	
			item.animate(clonedItem.position(), function() {
				clonedItem.detach();
				_super(item);
			});
		}
		else {		
			// Create a new field based on the dropped field type.
			currentlyEditedField = {};
			currentlyEditedField.text = {};
			currentlyEditedField.text.value = '';
			currentlyEditedField.text.codeMeaning = '';
			currentlyEditedField.text.codeValue = '';
			currentlyEditedField.text.codeScheme = '';
			currentlyEditedField.type = item[0].id;
			if (currentlyEditedField.type.startsWith('RDES')) {
				currentlyEditedField.type = 'RADELEMENT_CDE';
			}
			currentlyEditedField.id = currentlyEditedField.type + '_' + nextUID++;
			
			// Set the field type specific attributes.
			switch (currentlyEditedField.type) {
				case 'TEXT': {
					currentlyEditedField.multipleLines = false;
					currentlyEditedField.defaultValue = '';
					currentlyEditedField.dataFieldCompletionAction = 'NONE';
					break;
				}
				case 'SECTION_HEADER': {
					currentlyEditedField.level = 2;
					break;
				}
				case 'DATE': {
					currentlyEditedField.defaultValue = '';
					currentlyEditedField.dataFieldCompletionAction = 'NONE';
					break;
				}
				case 'TIME': {
					currentlyEditedField.defaultValue = '';
					currentlyEditedField.dataFieldCompletionAction = 'NONE';
					break;
				}
				case 'NUMBER': {
					currentlyEditedField.defaultValue = {};
					currentlyEditedField.defaultValue.value = '';
					currentlyEditedField.defaultValue.codeMeaning = '';
					currentlyEditedField.defaultValue.codeValue = '';
					currentlyEditedField.defaultValue.codeScheme = '';
					currentlyEditedField.minimum = '';
					currentlyEditedField.maximum = '';
					currentlyEditedField.units = '';
					currentlyEditedField.step = '';
					currentlyEditedField.dataFieldCompletionAction = 'NONE';
					break;
				}
				case 'SELECTION_LIST': {
					currentlyEditedField.defaultValue = '';
					currentlyEditedField.multipleSelections = false;
					currentlyEditedField.allowFreeText = false;
					currentlyEditedField.options = [{
						value: '', codeMeaning: '', codeValue: '', codeScheme: ''
					},{
						value: '', codeMeaning: '', codeValue: '', codeScheme: ''
					}];
					currentlyEditedField.dataFieldCompletionAction = 'NONE';
					break;
				}
				case 'RADIO': {
					currentlyEditedField.defaultValue = '';
					currentlyEditedField.options = [{
						value: '', codeMeaning: '', codeValue: '', codeScheme: '', selected: true
					},{
						value: '', codeMeaning: '', codeValue: '', codeScheme: '', selected: false
					}];
					currentlyEditedField.dataFieldCompletionAction = 'NONE';
					break;
				}
				case 'CHECKBOX': {
					currentlyEditedField.defaultValue = '';
					currentlyEditedField.options = [{
						value: '', codeMeaning: '', codeValue: '', codeScheme: '', selected: true
					},{
						value: '', codeMeaning: '', codeValue: '', codeScheme: '', selected: false
					}];
					currentlyEditedField.dataFieldCompletionAction = 'NONE';
					break;
				}
				case 'RADELEMENT_CDE': {
					currentlyEditedField.set = item[0].getAttribute('set');
					currentlyEditedField.defaultValue = '';
					currentlyEditedField.dataFieldCompletionAction = 'NONE';
					break;
				}
			}
			
			draggedItem = item;
			draggedSuper = _super;
			draggedItem[0].style.visibility = 'hidden';
			
			// Make sure the instructions are hidden.
			var instructionDiv = $('#' + currentlyDisplayedSection + '_Instructions');
			if (instructionDiv) {
				instructionDiv.remove();
			}
			
			displayFieldModal(); 
		}
	};
	
	/**
	 * Called to edit an existing field.  This will display a modal dialog to allow users to view/edit the field's
	 * attributes.
	 * 
	 * @param fieldId The fields unique identifier (field.id).
	 */
	function editField(fieldId) {
		currentlyEditedField = templateData.fields[fieldId];
		if (currentlyEditedField) {
			displayFieldModal();
		}
	};
	   
	/**
	 * Display the modal dialog to edit the current field.
	 */
	function displayFieldModal() {
		switch (currentlyEditedField.type) {
			case 'SECTION_HEADER': {
				editSectionHeader();
				break;
			}
			case 'TEXT': {
				editTextField();
				break;
			}
			case 'CHECKBOX': {
				editCheckboxField();
				break;
			}
			case 'RADIO': {
				editRadioField();
				break;
			}
			case 'DATE': {
				editDateField();
				break;
			}
			case 'TIME': {
				editTimeField();
				break;
			}
			case 'NUMBER': {
				editNumberField();
				break;
			}
			case 'SELECTION_LIST': {
				editSelectionListField();
				break;
			}
			case 'RADELEMENT_CDE': {
				editRadelementCdeField();
				break;
			}
		}
	};
	
	/**
	 * Set up the modal dialog for the specified field type so that all of the components common to all of the 
	 * modal dialogs are set up correctly.  For example, the delete button should only be displayed if editing
	 * an existing field.
	 */
	function prepEditModal(fieldType, hasDefaultInput, hasFieldCompletionAction) {
		// cleanup old event handlers
		$('#' + fieldType + '_FieldModal').off();
		
		$('#' + fieldType + '_FieldModal').on('shown.bs.modal', function () {
		    $('#' + fieldType + '_FieldInputText').focus();
		});
		
		$('#' + fieldType + '_FieldModal').on('hidden.bs.modal', function () {
			hideRadLexPopover();
		});
		
		$('#' + fieldType + '_FieldModal').attr('style', 'cursor: default;');
		
		// Add the field's information to the dialog
		var textInput = $('#' + fieldType + '_FieldInputText');
		textInput.val(currentlyEditedField.text.value);
		
		if (hasDefaultInput) {
			var defaultValueInput = $('#' + fieldType + '_FieldDefaultValueInput');
			defaultValueInput.val(currentlyEditedField.defaultValue);
		}
	
		if (hasFieldCompletionAction) {
			var fieldCompletionAction = $('#' + fieldType + '_FieldCompletionAction');
			fieldCompletionAction.val(currentlyEditedField.dataFieldCompletionAction);
		}
		
		// Set the highlighting for the text input field to indicate if linked to RadLex code.
		if (currentlyEditedField.text.codeMeaning.length > 0) {
			textInput.addClass('alert-info');
			textInput.addClass('linked');
			textInput.attr('title', '<fmt:message key="linked.to.radlex"/> ' + currentlyEditedField.text.codeMeaning);
		}
		else {
			textInput.removeClass('alert-info');
			textInput.removeClass('linked');
			textInput.attr('title', '');
		}
		
		// set the tooltip
		var tooltipInput = $('#' + fieldType + '_FieldTooltipInput');
		if (currentlyEditedField.tooltip) {
			tooltipInput.val(currentlyEditedField.tooltip);
		}
		else {
			tooltipInput.val('');
		}
		
		textInput.removeAttr('readonly');
		if (defaultValueInput) {
			defaultValueInput.removeAttr('readonly');
		}
		if (fieldCompletionAction) {
			fieldCompletionAction.removeAttr('disabled');
		}
		
		$('#' + fieldType + '_FieldModelSaveButton').show();
		// If new then hide the delete button
		if (!templateData.fields[currentlyEditedField.id]) {
			$('#' + fieldType + '_FieldModelDeleteButton').hide();
		}
		else {
			$('#' + fieldType + '_FieldModelDeleteButton').show();
		}
		$('#' + fieldType + '_popover').show();
	};
	
	/**
	 * Display the modal dialog to allow users to create/edit a section header field.
	 */
	function editSectionHeader() {
		prepEditModal('SECTION_HEADER', false, false);
		
		// display the modal
		$('#SECTION_HEADER_FieldModal').modal({ backdrop: 'static', keyboard: false });
	};
	
	/**
	 * Save the contents of the section header modal dialog.
	 */
	function saveSectionHeaderField() {
		extractBasicFieldData('SECTION_HEADER', false, false, false);
		updateFieldDisplay();
		$('#SECTION_HEADER_FieldModal').modal('hide');
	};
	
	/**
	 * Display the modal dialog to allow users to create/edit a TEXT field.
	 */
	function editTextField() {
		prepEditModal('TEXT', true, true);
		
		var multipleLinesCheckbox = document.getElementById('TEXT_MultipleLines');
		if (currentlyEditedField.multipleLines) {
			multipleLinesCheckbox.setAttribute('checked', 'checked');
		}
		else {
			multipleLinesCheckbox.removeAttribute('checked');
		}
		
		// display the modal
		$('#TEXT_FieldModal').modal({ backdrop: 'static', keyboard: false });
	};
	
	/**
	 * Save the contents of the Date field type modal dialog.
	 */
	function saveTextField() {
		extractBasicFieldData('TEXT', true, true, false);
		currentlyEditedField.multipleLines = document.getElementById('TEXT_MultipleLines').checked;
		updateFieldDisplay();
		$('#TEXT_FieldModal').modal('hide');
	};
	
	/**
	 * Save the contents of the Checkbox field type modal dialog.
	 */
	function saveCheckboxField() {
		extractBasicFieldData('CHECKBOX', true, false, false);
		
		// Extract the selection list's options.
		currentlyEditedField.options = [];
		var optionInputs = $('[name^=CHECKBOX_OptionInput_]');
		for (var i = 0; i < optionInputs.length; i++) {
			if (optionInputs[i].type == 'hidden') {
				continue;
			}
			var optionJSON = {};
			optionJSON.value = optionInputs[i].value;
			optionJSON.codeMeaning = $('#' + optionInputs[i].id + '_CodeMeaning').val();
			optionJSON.codeValue = $('#' + optionInputs[i].id + '_CodeValue').val();
			optionJSON.codeScheme = $('#' + optionInputs[i].id + '_CodeScheme').val();
			optionJSON.selected = ($('#' + optionInputs[i].id + '_OptionDefault').is(':checked'));
			optionJSON.allowFreeText = ($('#' + optionInputs[i].id + '_AllowFreeText').is(':checked'));
			currentlyEditedField.options.push(optionJSON);
		}
		
		updateFieldDisplay();
		$('#CHECKBOX_FieldModal').modal('hide');
	};
	
	/**
	 * Display the modal dialog to allow users to create/edit a CHECKBOX field.
	 */
	function editCheckboxField() {
		prepEditModal('CHECKBOX', false, true);

		// Add the options.  First make sure to remove the option list items from previous edits.  Then iterate 
		// over options and add them to the list.
		$('#CHECKBOX_FieldOptionsList').empty();
				
		var cols = ['<fmt:message key="editor.option.column.text"/>', 
			        '<fmt:message key="editor.option.column.radlex"/>', 
			        '<fmt:message key="editor.option.column.checked"/>', 
			        '<fmt:message key="editor.option.column.freeText"/>', 
			        '<fmt:message key="editor.option.column.delete"/>'];
		var thead = document.createElement('thead');
		var row = document.createElement('tr');
		thead.appendChild(row);
		for (var index = 0; index < cols.length; index++) {
			var cell = document.createElement('td');
			if (index == 0) {
				cell.setAttribute('class', 'editorOptionTextCell');
			}
			else {
				cell.setAttribute('class', 'editorOptionCell');
			}
			cell.appendChild(document.createTextNode(cols[index]));
			row.appendChild(cell);	
		}
		document.getElementById('CHECKBOX_FieldOptionsList').appendChild(thead);
		
		for (var index = 0; index < currentlyEditedField.options.length; index++) {
			var optionJSON = currentlyEditedField.options[index];
			addSelectionListOption('CHECKBOX', optionJSON, optionJSON.selected, true);
		}
		
		// display the modal
		$('#CHECKBOX_FieldModal').modal({ backdrop: 'static', keyboard: false });
	};
	
	/**
	 * Display the modal dialog to allow users to create/edit a NUMBER field.
	 */
	function editNumberField() {
		prepEditModal('NUMBER', true, true);
		
		// Populate custom NUMBER attributes
		var minimumInput = document.getElementById('NUMBER_MinimumValueInput');
		minimumInput.value = currentlyEditedField.minimum;
		var maximumInput = document.getElementById('NUMBER_MaximumValueInput');
		maximumInput.value = currentlyEditedField.maximum;
		var unitsInput = document.getElementById('NUMBER_UnitsInput');
		unitsInput.value = currentlyEditedField.units;
		var stepInput = document.getElementById('NUMBER_StepInput');
		stepInput.value = currentlyEditedField.step;
	
		// display the modal
		$('#NUMBER_FieldModal').modal({ backdrop: 'static', keyboard: false });
	};
	
	/**
	 * Save the contents of the Date field type modal dialog.
	 */
	function saveNumberField() {
		extractBasicFieldData('NUMBER', true, true, false);
		currentlyEditedField.minimum = document.getElementById('NUMBER_MinimumValueInput').value;
		currentlyEditedField.maximum = document.getElementById('NUMBER_MaximumValueInput').value;
		currentlyEditedField.units = document.getElementById('NUMBER_UnitsInput').value;
		currentlyEditedField.step = document.getElementById('NUMBER_StepInput').value;
		updateFieldDisplay();
		$('#NUMBER_FieldModal').modal('hide');
	};
	
	/**
	 * Display the modal dialog to allow users to create/edit a DATE field.
	 */
	function editDateField() {
		prepEditModal('DATE', true, true);
		
		// display the modal
		$('#DATE_FieldModal').modal({ backdrop: 'static', keyboard: false });
	};
	
	/**
	 * Save the contents of the Date field type modal dialog.
	 */
	function saveDateField() {
		extractBasicFieldData('DATE', true, true, false);
		updateFieldDisplay();
		$('#DATE_FieldModal').modal('hide');
	};
	
	/**
	 * Display the modal dialog to allow users to create/edit a TIME field.
	 */
	function editTimeField() {
		prepEditModal('TIME', true, true);
		
		// display the modal
		$('#TIME_FieldModal').modal({ backdrop: 'static', keyboard: false });
	};
	
	/**
	 * Save the contents of the Time field type modal dialog.
	 */
	function saveTimeField() {
		extractBasicFieldData('TIME', true, true, false);
		updateFieldDisplay();
		$('#TIME_FieldModal').modal('hide');
	};
	
	/**
	 * Display the modal dialog to allow users to create/edit a SELECTION LIST field.
	 */
	function editSelectionListField() {
		prepEditModal('SELECTION_LIST', false, true);

		if (currentlyEditedField.multipleSelections) {
			$('#SELECTION_LIST_MultipleSelections').trigger('click');
			$('#SELECTION_LIST_AllowFreeText').removeAttr('checked');
		}
		else {
			$('#SELECTION_LIST_SingleSelection').trigger('click');
			if (currentlyEditedField.allowFreeText) {
				$('#SELECTION_LIST_AllowFreeText').attr('checked', 'checked');
			}
			else {
				$('#SELECTION_LIST_AllowFreeText').removeAttr('checked');
			}
		}
		
		// Add the options.  First make sure to remove the option list items from previous edits.  Then iterate 
		// over options and add them to the list.
		$('#SELECTION_LIST_FieldOptionsList').empty();
				
		var cols = ['<fmt:message key="editor.option.column.text"/>', 
			        '<fmt:message key="editor.option.column.radlex"/>', 
			        '<fmt:message key="editor.option.column.selected"/>', 
			        '<fmt:message key="editor.option.column.delete"/>'];
		var thead = document.createElement('thead');
		var row = document.createElement('tr');
		thead.appendChild(row);
		for (var index = 0; index < cols.length; index++) {
			var cell = document.createElement('td');
			if (index == 0) {
				cell.setAttribute('class', 'editorOptionTextCell');
			}
			else {
				cell.setAttribute('class', 'editorOptionCell');
			}
			cell.appendChild(document.createTextNode(cols[index]));
			row.appendChild(cell);	
		}
		document.getElementById('SELECTION_LIST_FieldOptionsList').appendChild(thead);
		
		for (var index = 0; index < currentlyEditedField.options.length; index++) {
			var optionJSON = currentlyEditedField.options[index];
			var isDefault = false;
			if (currentlyEditedField.defaultValue == '') {
				isDefault = (index == 0);
			}
			else {
				isDefault = (currentlyEditedField.defaultValue == optionJSON.value);
			}
			addSelectionListOption('SELECTION_LIST', optionJSON, isDefault);
		}
		
		// display the modal
		$('#SELECTION_LIST_FieldModal').modal({ backdrop: 'static', keyboard: false });
	};
	
	/**
	 * Save the contents of the selection list field type modal dialog.
	 */
	function saveSelectionListField() {
		extractBasicFieldData('SELECTION_LIST', true, false, false);
		currentlyEditedField.multipleSelections = document.getElementById('SELECTION_LIST_MultipleSelections').checked;
		if (currentlyEditedField.multipleSelections) {
			currentlyEditedField.allowFreeText = false;
		}
		else {
			currentlyEditedField.allowFreeText = document.getElementById('SELECTION_LIST_AllowFreeText').checked;
		}
		
		// Extract the selection list's options.
		currentlyEditedField.options = [];
		var optionInputs = $('[name^=SELECTION_LIST_OptionInput_]');
		for (var i = 0; i < optionInputs.length; i++) {
			if (optionInputs[i].type == 'hidden') {
				continue;
			}
			var optionJSON = {};
			optionJSON.value = optionInputs[i].value;
			optionJSON.codeMeaning = $('#' + optionInputs[i].id + '_CodeMeaning').val();
			optionJSON.codeValue = $('#' + optionInputs[i].id + '_CodeValue').val();
			optionJSON.codeScheme = $('#' + optionInputs[i].id + '_CodeScheme').val();
			if ($('#' + optionInputs[i].id + '_OptionDefault').is(':checked')) {
				currentlyEditedField.defaultValue = optionJSON.value;
			}
			currentlyEditedField.options.push(optionJSON);
		}
		
		updateFieldDisplay();
		$('#SELECTION_LIST_FieldModal').modal('hide');
	};
	
	function createSetURL(currentlyEditedField){
		const setID = currentlyEditedField.set.id;
		if(window.location.hostname.includes("stage") || window.location.hostname.includes("local")){
			// use stage resoources
			setURL = "https://stage.radelement.org/home/sets/set/" + setID;

		}else{
			// prod
			setURL = "https://radelement.org/home/sets/set/" + setID;
		}
		return setURL
	}
	
	/**
	 * Display the modal dialog to allow users to create/edit a SELECTION LIST field.
	 */
	function editRadelementCdeField() {

		if (typeof currentlyEditedField.set !== "object") {
			currentlyEditedField.set = JSON.parse(currentlyEditedField.set);
    	}
		
		prepEditModal('RADELEMENT_CDE', false, true);
		$('#element_list').remove();
		// display the modal
		$('#RADELEMENT_CDE_FieldModal').modal({ backdrop: 'static', keyboard: false });
		
		const elemArray = currentlyEditedField.set.elements ?? [];
		const setID = currentlyEditedField.set.id;
		const setName = currentlyEditedField.set.name;
		document.getElementById("CDE_header_text").textContent =  setID + ' : ' + setName;
		var setURL = createSetURL(currentlyEditedField);
		
		// check what url to use when generating base href for use in anchor attributes
		if(window.location.hostname.includes("stage") || window.location.hostname.includes("local")){
			// use stage resoources
			document.getElementById("set_details_reference_url").href = setURL;
			
			var elementBaseUrl = 'https://stage.radelement.org/home/elements/element/';
		}else{
			// prod
			// base url for elements, append in foreach
			document.getElementById("set_details_reference_url").href = setURL;
			var elementBaseUrl = 'https://radelement.org/home/elements/element/';
		}

		/*
			add the elements as children of the container div in the modal
			
		*/
		const unorderedList = document.createElement("ul"); // start unordered list, append to modal body div 
		unorderedList.id = "element_list";
		document.getElementById("RADELEMENT_CDE_FieldModalBody").appendChild(unorderedList);
		

		const len = elemArray.length;

		for(var i=0; i< len; i++){
			const listItem = document.createElement("li"); // create list item
			
			//create anchor tag
			const anchorTag = document.createElement("a"); // create anchor
			anchorTag.target = "_blank";
			anchorTag.href = elementBaseUrl + elemArray[i].id;
			
			// set inner value
			anchorTag.innerHTML = elemArray[i].id + " : " + elemArray[i].name;
			
			// append anchor to list item
			listItem.appendChild(anchorTag);
			
			// append list item to list
			unorderedList.appendChild(listItem);
		}

		
	};
	
	/**
	 * Save the contents of the selection list field type modal dialog. 
	 */
	function saveRadelementCdeField() {		
		var setURL = createSetURL(currentlyEditedField);
		const setID = currentlyEditedField.set.id;
		const setName = currentlyEditedField.set.name;

		if(templateData.metadata.cdeSets === undefined) templateData.metadata.cdeSets = []; // just in case
		// set metadata per cde field
		templateData.metadata.cdeSets.push({
			setID: setID,
			setName: setName,
			setURL: setURL
		});

		updateFieldDisplay();
		$('#RADELEMENT_CDE_FieldModal').modal('hide');
	};
	
	/**
	 * Display the modal dialog to allow users to create/edit a RADIO field.
	 */
	function editRadioField() {
		prepEditModal('RADIO', false, true);

		// Add the options.  First make sure to remove the option list items from previous edits.  Then iterate 
		// over options and add them to the list.
		$('#RADIO_FieldOptionsList').empty();
		
		var cols = ['<fmt:message key="editor.option.column.text"/>', 
			        '<fmt:message key="editor.option.column.radlex"/>', 
			        '<fmt:message key="editor.option.column.selected"/>', 
			        '<fmt:message key="editor.option.column.freeText"/>',
			        '<fmt:message key="editor.option.column.delete"/>'];
		var thead = document.createElement('thead');
		var row = document.createElement('tr');
		thead.appendChild(row);
		for (var index = 0; index < cols.length; index++) {
			var cell = document.createElement('td');
			if (index == 0) {
				cell.setAttribute('class', 'editorOptionTextCell');
			}
			else {
				cell.setAttribute('class', 'editorOptionCell');
			}
			cell.appendChild(document.createTextNode(cols[index]));
			row.appendChild(cell);	
		}
		document.getElementById('RADIO_FieldOptionsList').appendChild(thead);
		
		for (var index = 0; index < currentlyEditedField.options.length; index++) {
			var optionJSON = currentlyEditedField.options[index];
			addSelectionListOption('RADIO', optionJSON, optionJSON.selected);
		}
		
		// display the modal
		$('#RADIO_FieldModal').modal({ backdrop: 'static', keyboard: false });
	};
	
	/**
	 * Save the contents of the selection list field type modal dialog.
	 */
	function saveRadioField() {
		extractBasicFieldData('RADIO', true, false, false);
		
		// Extract the selection list's options.
		currentlyEditedField.options = [];
		var optionInputs = $('[name^=RADIO_OptionInput_]');
		for (var i = 0; i < optionInputs.length; i++) {
			if (optionInputs[i].type == 'hidden') {
				continue;
			}
			var optionJSON = {};
			optionJSON.value = optionInputs[i].value;
			optionJSON.codeMeaning = $('#' + optionInputs[i].id + '_CodeMeaning').val();
			optionJSON.codeValue = $('#' + optionInputs[i].id + '_CodeValue').val();
			optionJSON.codeScheme = $('#' + optionInputs[i].id + '_CodeScheme').val();
			optionJSON.selected = ($('#' + optionInputs[i].id + '_OptionDefault').is(':checked'));
			optionJSON.allowFreeText = ($('#' + optionInputs[i].id + '_AllowFreeText').is(':checked'));
			currentlyEditedField.options.push(optionJSON);
		}
		
		updateFieldDisplay();
		$('#RADIO_FieldModal').modal('hide');
	};
	
	/**
	 * Extract basic field data from the specified field type's modal dialog.  This will update the 
	 * currentlyEditedField attribute with the data.
	 * 
	 * @param fieldType The type of field to be extracted, i.e. "TEXT", "NUMBER", ...
	 * @param hasCompletionAction Boolean to indicate if the field has the completion action selection.
	 * @param hasDefaultValue Boolean indicting if the field has a default value to be extracted.
	 * @param isDefaultValueCoded Boolean indicating if the default value is a coded value. 
	 */
	function extractBasicFieldData(fieldType, hasCompletionAction, hasDefaultValue, isDefaultValueCoded) {
		currentlyEditedField.text.value = document.getElementById(fieldType + '_FieldInputText').value;
		if (hasDefaultValue) {
			if (isDefaultValueCoded) {
				currentlyEditedField.defaultValue.value = document.getElementById(fieldType + '_FieldDefaultValueInput').value;
			}
			else {
				currentlyEditedField.defaultValue = document.getElementById(fieldType + '_FieldDefaultValueInput').value;
			}
		}
		if (hasCompletionAction) {
			var actionSelect = document.getElementById(fieldType + '_FieldCompletionAction');
			currentlyEditedField.dataFieldCompletionAction = actionSelect.options[actionSelect.selectedIndex].value;
		}
		currentlyEditedField.tooltip = document.getElementById(fieldType + '_FieldTooltipInput').value;
	};
	
	/**
	 * Add an Option to the Selection List modal dialog.
	 * 
	 * @param fieldType The name of the field type, e.g. SELECTION_LIST, RADIO, CHECKBOX.
	 * @param optionJSON JSON containing the option data.
	 * @param isDefault Boolean to indicate if the option is the default.
	 * @param isCheckbox Boolean to indicate if the default option should be rendered as a radio button (false) 
	 *                   or a checkbox (true).
	 */
	function addSelectionListOption(fieldType, optionJSON, isDefault, isCheckbox) {
		if (optionJSON == null) {
			optionJSON = {};
			optionJSON.value = '';
			optionJSON.codeValue = '';
			optionJSON.codeMeaning = '';
			optionJSON.codeScheme = '';
			optionJSON.allowFreeText = false;
		}
		var id = nextUID++;

		var row = document.createElement('tr');
		row.id = fieldType + '_OptionListItem_' + id;
		row.setAttribute('name', fieldType + '_OptionListItem');
		row.className = 'field-dialog-option-list-item';
		
		var input = document.createElement('input');
		input.id = fieldType + '_OptionInput_' + id;
		input.name = input.id; 
		input.type = 'text';
		input.className = 'field-dialog-option-input';
		if (optionJSON.codeValue.length > 0) {
			input.className = input.className + ' linked alert-info';
			input.setAttribute('title', '<fmt:message key="linked.to.radlex"/> ' + optionJSON.codeMeaning);
		}
		input.value = optionJSON.value;
		var cell = document.createElement('td');
		cell.appendChild(input);
		row.appendChild(cell);
		
		var popover = document.createElement('span');
		popover.id = input.id + '_popover';
		popover.className = 'fa fa-link';
		popover.style.marginLeft = '5px';
		popover.setAttribute('onclick', "showRadLexPopover('" + popover.id + "', 'OPTION', '" + input.id + "')");
		popover.setAttribute('title', '<fmt:message key="link.to.radlex"/>');
		cell = document.createElement('td');
		cell.appendChild(popover);
		row.appendChild(cell);
	
		// Checkbox to set as default option
		var defaultInputType = (!_.isUndefined(isCheckbox) && !_.isNull(isCheckbox) && isCheckbox) ? "checkbox" : "radio";
		var defaultRadioInput = document.createElement('input');
		defaultRadioInput.id = input.id + '_OptionDefault';
		defaultRadioInput.type = defaultInputType;
		defaultRadioInput.name = 'defaultOptionButton';
		defaultRadioInput.setAttribute('title', '<fmt:message key="set.as.default.value"/>');
		if (isDefault) {
			defaultRadioInput.setAttribute('checked', 'checked');
		}
		defaultRadioInput.style.marginLeft = '5px';
		cell = document.createElement('td');
		cell.appendChild(defaultRadioInput);
		row.appendChild(cell);
	
		// Checkbox to set as free text
		if (fieldType == 'RADIO' || fieldType == 'CHECKBOX') {
			var freeTextInput = document.createElement('input');
			freeTextInput.id = input.id + '_AllowFreeText';
			freeTextInput.name = 'AllowFreeText';
			freeTextInput.type = 'radio';
			freeTextInput.setAttribute('title', '<fmt:message key="dialog.selection.list.allow.free.text"/>');
			freeTextInput.style.marginLeft = '5px';
			if (optionJSON.allowFreeText) {
				freeTextInput.setAttribute('checked', 'checked');
			}
			cell = document.createElement('td');
			cell.appendChild(freeTextInput);
			row.appendChild(cell);
		}
		
		// Add an icon to delete the option.
		var deleteButton = document.createElement('span');
		deleteButton.className = 'glyphicon glyphicon-remove';
		deleteButton.setAttribute('name', 'OptionDeleteButton');
		deleteButton.setAttribute('title', '<fmt:message key="delete"/>');
		deleteButton.style.marginLeft = '5px';
		cell = document.createElement('td');
		cell.appendChild(deleteButton);
		row.appendChild(cell);
		
		// Add 3 hidden inputs to hold the linked RadLex code information.
		var hiddenInput = document.createElement('input');
		hiddenInput.type = 'hidden';
		hiddenInput.id = input.id + '_CodeValue';
		hiddenInput.value = optionJSON.codeValue;
		row.appendChild(hiddenInput);
	
		hiddenInput = document.createElement('input');
		hiddenInput.type = 'hidden';
		hiddenInput.id = input.id + '_CodeMeaning';
		hiddenInput.value = optionJSON.codeMeaning;
		row.appendChild(hiddenInput);
	
		hiddenInput = document.createElement('input');
		hiddenInput.type = 'hidden';
		hiddenInput.id = input.id + '_CodeScheme';
		hiddenInput.value = optionJSON.codeScheme;
		row.appendChild(hiddenInput);
	
		deleteButton.onclick = function() {
			$('#' + row.id).remove();
			updateOptionVisibility();
		}
			
		document.getElementById(fieldType + '_FieldOptionsList').appendChild(row);
		updateOptionVisibility();
	};
	
	function addSelectionListOptionORIG(fieldType, optionJSON, isDefault, isCheckbox) {
		if (optionJSON == null) {
			optionJSON = {};
			optionJSON.value = '';
			optionJSON.codeValue = '';
			optionJSON.codeMeaning = '';
			optionJSON.codeScheme = '';
			optionJSON.allowFreeText = false;
		}
		var id = nextUID++;
		
		var input = document.createElement('input');
		input.id = fieldType + '_OptionInput_' + id;
		input.name = input.id; 
		input.type = 'text';
		input.className = 'field-dialog-option-input';
		if (optionJSON.codeValue.length > 0) {
			input.className = input.className + ' linked alert-info';
			input.setAttribute('title', '<fmt:message key="linked.to.radlex"/> ' + optionJSON.codeMeaning);
		}
		input.value = optionJSON.value;
		
		var popover = document.createElement('span');
		popover.id = input.id + '_popover';
		popover.className = 'fa fa-link';
		popover.style.marginLeft = '5px';
		popover.setAttribute('onclick', "showRadLexPopover('" + popover.id + "', 'OPTION', '" + input.id + "')");
		popover.setAttribute('title', '<fmt:message key="link.to.radlex"/>');
	
		// Checkbox to set as default option
		var defaultInputType = (!_.isUndefined(isCheckbox) && !_.isNull(isCheckbox) && isCheckbox) ? "checkbox" : "radio";
		var defaultRadioInput = document.createElement('input');
		defaultRadioInput.id = input.id + '_OptionDefault';
		defaultRadioInput.type = defaultInputType;
		defaultRadioInput.name = 'defaultOptionButton';
		defaultRadioInput.setAttribute('title', '<fmt:message key="set.as.default.value"/>');
		if (isDefault) {
			defaultRadioInput.setAttribute('checked', 'checked');
		}
		defaultRadioInput.style.marginLeft = '5px';
	
		// Checkbox to set as free text
		if (fieldType == 'RADIO' || fieldType == 'CHECKBOX') {
			var freeTextInput = document.createElement('input');
			freeTextInput.id = input.id + '_AllowFreeText';
			freeTextInput.name = 'AllowFreeText';
			freeTextInput.type = 'radio';
			freeTextInput.setAttribute('title', '<fmt:message key="dialog.selection.list.allow.free.text"/>');
			freeTextInput.style.marginLeft = '5px';
			if (optionJSON.allowFreeText) {
				freeTextInput.setAttribute('checked', 'checked');
			}
		}
		
		// Add an icon to delete the option.
		var deleteButton = document.createElement('span');
		deleteButton.className = 'glyphicon glyphicon-remove';
		deleteButton.setAttribute('name', 'OptionDeleteButton');
		deleteButton.setAttribute('title', '<fmt:message key="delete"/>');
		deleteButton.style.marginLeft = '5px';
		
		var listItemDiv = document.createElement('div');
		listItemDiv.appendChild(input);
		listItemDiv.appendChild(popover);
		listItemDiv.appendChild(defaultRadioInput);
		if (fieldType == 'RADIO' || fieldType == 'CHECKBOX') {
			listItemDiv.appendChild(freeTextInput);
		}
		listItemDiv.appendChild(deleteButton);
		
		// Add 3 hidden inputs to hold the linked RadLex code information.
		var hiddenInput = document.createElement('input');
		hiddenInput.type = 'hidden';
		hiddenInput.id = input.id + '_CodeValue';
		hiddenInput.value = optionJSON.codeValue;
		listItemDiv.appendChild(hiddenInput);
	
		hiddenInput = document.createElement('input');
		hiddenInput.type = 'hidden';
		hiddenInput.id = input.id + '_CodeMeaning';
		hiddenInput.value = optionJSON.codeMeaning;
		listItemDiv.appendChild(hiddenInput);
	
		hiddenInput = document.createElement('input');
		hiddenInput.type = 'hidden';
		hiddenInput.id = input.id + '_CodeScheme';
		hiddenInput.value = optionJSON.codeScheme;
		listItemDiv.appendChild(hiddenInput);
		
		var li = document.createElement('li');
		li.id = fieldType + '_OptionListItem_' + id;
		li.setAttribute('name', fieldType + '_OptionListItem');
		li.className = 'field-dialog-option-list-item';
		li.appendChild(listItemDiv);
	
		deleteButton.onclick = function() {
			$('#' + li.id).remove();
			updateOptionVisibility();
		}
			
		document.getElementById(fieldType + '_FieldOptionsList').appendChild(li);
		updateOptionVisibility();
	};
	
	/**
	 * Hide the buttons to delete options if there are 2 or less options.
	 */
	function updateOptionVisibility() {
		var optionDeleteButtons = document.getElementsByName('OptionDeleteButton');
		var visibility = (optionDeleteButtons.length > 2 ? 'visible' : 'hidden');
		var display = (optionDeleteButtons.length > 2 ? 'inline' : 'none');
		for (var i = 0; i < optionDeleteButtons.length; i++) {
			optionDeleteButtons[i].style.visibility = visibility;
			optionDeleteButtons[i].style.display = display;
		}
	};
	
	/**
	 * Update the display using the information in the currentlyEditedField attribute.  If the field already 
	 * exists in the display the existing display will be updated.  If a new field the field will be added to the
	 * display.
	 */
	function updateFieldDisplay() {

		// Update the templateData object with the data.  Replace if already existing.
		
		var updated = templateData.fields[currentlyEditedField.id];
		templateData.fields[currentlyEditedField.id] = currentlyEditedField;
		
		// Build the <li> element to add to the UI.  If updating an existing Field then this will get the existing
		// <li> element and update it.  Otherwise create a new <li> element.
		if (updated) { 
			$('#FieldName_' + currentlyEditedField.id).empty();
			var text = document.createTextNode(currentlyEditedField.text.value);
			document.getElementById('FieldName_' + currentlyEditedField.id).appendChild(text);
		}
		else { 
			var fieldListItem = buildFieldListItem(currentlyEditedField);
			if (draggedItem) {
				draggedItem.before(fieldListItem);
				draggedItem.detach();
			}
			else {
				document.getElementById(currentlyDisplayedSection + '_FIELDS').appendChild(fieldListItem);
			}
			
			// Make sure the instructions are hidden.
			var instructionDiv = $('#' + currentlyDisplayedSection + '_Instructions');
			if (instructionDiv) {
				instructionDiv.remove();
			}
		}
		
		storeTemplateLocally();
	};
	
	/**
	 * Create an <li> element to display the Field.
	 * 
	 * @param element
	 * @param field
	 * @returns 
	 */
	function addFieldToTemplateSectionDisplay(element, field, node) {
		// Create the list item.  Use the field's ID as the list item's ID so that it can be looked up 
		// when editing.
		var listItem = buildFieldListItem(field, node);
		if (element != null) {
			element.appendChild(listItem);
		}
	};
	
	function setupFieldContextMenu(elementId) {
		var field = templateData.fields[elementId];
		var menu = [ {
			text: '<fmt:message key="edit"/>', 
			action: function(e) { editField(elementId); }
		},{
			text: '<fmt:message key="delete"/>', 
			action: function(e) { deleteField(elementId); }
		},{
			divider: true
		},{
			text: '<fmt:message key="copy"/>', 
			action: function(e) { copiedFieldId = elementId; }
		},{
			text: '<fmt:message key="paste"/>', 
			action: function(e) { pasteField(copiedFieldId, elementId, false); }
		} ];
		context.attach('#' + elementId, menu);
		context.settings({compress: true});
	};
	
	function setupEditorContextMenu(elementId) {
		context.attach('#' + elementId, [ {
				text: '<fmt:message key="paste"/>', 
				action: function(e) { pasteField(copiedFieldId, elementId, false); }
			} 
		]);
		context.settings({compress: true});
	};
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// PASTE STUFF
	
	/**
	 * Make a copy of the field that has been copied (field id in copiedFieldId) and paste it into
	 * the template.
	 * @param fieldIdToCopy The element ID of the fields that is to be copied.
	 * @param byFieldId Indicates where the copied field is to be pasted.  If null the field will be pasted
	 *                  at the end of the template.  If isChild is true then the field is being pasted into
	 *                  a section header.  If isChild is false the field will be pasted after the byFieldId
	 *                  element.
	 * @param isChild True indicates that this field is being pasted into a section header.
	 */
	function pasteField(fieldIdToCopy, byFieldId, isChild) {
		if (fieldIdToCopy) {
			var copy = createCopyOfField(fieldIdToCopy);
			var newFieldListItem = buildFieldListItem(copy);
			
			if (byFieldId) {
				if (isChild) {
					$('#' + byFieldId + '_FIELDS').appendChild(newFieldListItem);
				}
				else {
					$('#' + byFieldId).after(newFieldListItem);
				}
			}
			else {
				document.getElementById(currentlyDisplayedSection + '_FIELDS').appendChild(newFieldListItem);
			}
			
			// If this is a section header then a copy needs to be created of all of the headers
			// children and then each of the children added to the layout.
			if (fieldIdToCopy.search('SECTION_HEADER') > -1) {
				// Get the section header's layout and use it to 
				var headerLayout = getHeaderLayout(fieldIdToCopy);
				for (var index = 0; index < headerLayout.children.length; index++) {
					var child = headerLayout.children[index];
					var childId = child.id;
					pasteField(child.id, copy.id, true);
				}				
			}

			clearInstructions();
			store();
		}
		else {
			displayMessage('', '<fmt:message key="no.field.copied"/>');
		}
	};
	
	/**
	 * Get the layout JSON for a section header.  This will recursively search the layout JSON for the section header's
	 * layout.  If not found this will return null.
	 */
	function getHeaderLayout(headerId) {
		for (var i = 0; i < templateData.layout.length; i++) {
			var header = getHeaderLayout2(templateData.layout[i], headerId);
			if (header != null) {
				return header;
			}
		}
		return null;
	};
	
	function getHeaderLayout2(json, headerId) {
		if (json.id == headerId) {
			return json;
		}
		else if (json.children) {
			for (var x = 0; x < json.children.length; x++) {
				var child = json.children[x];
				var header = getHeaderLayout2(child, headerId);
				if (header != null) {
					return header;
				}
			}
		}
		return null;
	};
	
	// END PASTE STUFF
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Create a copy of the field identified by the provided field identifier.  This will create the copy, put the
	 * copy into the template and return it.
	 */
	function createCopyOfField(fieldId) {
		var fieldToCopy = templateData.fields[fieldId];
		var copy = JSON.parse(JSON.stringify(fieldToCopy));
		copy.id = copy.type + '_' + nextUID++;
		templateData.fields[copy.id] = copy;
		return copy;
	};
	
	/**
	 * Remove the div displaying the drag and drop instructions.
	 */
	function clearInstructions() {
		// Make sure the instructions are hidden.
		var instructionDiv = $('#' + currentlyDisplayedSection + '_Instructions');
		if (instructionDiv) {
			instructionDiv.remove();
		}
	};
	
	/**
	 * Delete the currently selected field.
	 */
	function deleteCurrentField() {
		$('#fieldModal').modal('hide');
		deleteField(currentlyEditedField.id);
	};
	
	/**
	 * Delete a field.
	 * 
	 * @param fieldId The ID of the field to be deleted.
	 */
	function deleteField(fieldId) {
		delete templateData.fields[fieldId];
		var fieldListItem = document.getElementById(fieldId);
		fieldListItem.parentNode.removeChild(fieldListItem);
		
		if(fieldId.includes("RADELEMENT_CDE_")){
			const len = templateData.metadata.cdeSets.length;
			for(var i = 0; i < len; i++) {
				if(templateData.metadata.cdeSets[i].setID ===  currentlyEditedField.set.id){

					delete templateData.metadata.cdeSets[i];
				}
			}
			
			if(templateData.metadata.cdeSets[0] === undefined && templateData.metadata.cdeSets.length === 1){
				// weird thing where this wants to keep a first value of 'null' and have length of 1 when its meant to be empty , so reset to empty arr
				templateData.metadata.cdeSets = [];
			}
		}
		
		
		storeTemplateLocally();
	};
	
	/**
	 * Display a preview of the template. 
	 */
	function previewEditedTemplate() {
		store();
		previewTemplate(templateData.metadata.id);
	};
	
	/**
	 * Called when the template has been modifed to write the template data to the browser's local storage.
	 */
	function storeTemplateLocally() {
		setTimeout(function() { store(); }, 250);
	}
	
	/**
	 * Gather all of the template data and update the templateData attribute.  This will also save the template
	 * to the browser's local storage for this website.
	 */
	function store() {
		// Get the locally stored templates
		var templates = jQuery.parseJSON(localStorage.templates);
		
		if (!templates) {
			templates = {};
		}
		
		if (!hasIdAlreadyBeenModified) {
			// Delete the template stored with the old ID
			delete templates[templateData.metadata.id];
			templateData.metadata.id = generateUUID();
			hasIdAlreadyBeenModified = true;
		}

		// Get the metadata information
		templateData.metadata.title = $('#TemplateNameInput').val();
		templateData.metadata.description = $('#TemplateDescriptionInput').val();
		templateData.metadata.creator = $('#TemplateAuthorInput').val();
		templateData.metadata.email = $('#TemplateAuthorEmailInput').val();
		templateData.metadata.publisher = $('#TemplatePublisherInput').val();
		templateData.metadata.language = $('#TemplateLanguageSelect').val().toLowerCase();
		templateData.metadata.lastModifiedTimestamp = (new Date()).getTime();
		
		templateData.metadata.contributors = [];
		var lines = $('#TemplateContributorsInput').val().split('\n');
		for(var i = 0; i < lines.length;i++) {
			templateData.metadata.contributors.push(lines[i]);
		}
		
		var layout = [];
		var panels = document.getElementById('templateSectionsTabPanels');
		for (var i = 0; i < panels.children.length; i++) {
            layout[i] = buildLayoutNode(panels.children[i]);
		}
		templateData.layout = layout;
		
		templates[templateData.metadata.id] = templateData;
		localStorage.templates = JSON.stringify(templates);
	};
	
	/**
	 * Used to build the information that is stored in template.layout attribute.
	 * 
	 * @param element
	 * @returns
	 */
	function buildLayoutNode(element) {
		if (!templateData.fields[element.id]) {
			return null;
		}
		var node = {};
		node.id = element.id;
		for (var i = 0; i < element.children.length; i++) {
			var child = element.children[i];
			if (child.nodeName.toLowerCase() == 'ol') {
				for (var x = 0; x < child.children.length; x++) {
					var childNode = buildLayoutNode(child.children[x]);
					if (childNode) {
						if (!node.children) {
							node.children = [];
						}
						node.children.push(childNode);
					}
				}
			}
		}
		return node;
	};
	
	/**
	 * Create a <li> element to display the field in the template.
	 * 
	 * @param field The field to build a list item for display.
	 * @param node
	 * @return The <li> element.
	 */
	function buildFieldListItem(field, node) {	
		// Create the list item.  Use the field's ID as the list item's ID so that it can be looked up 
		// when editing.
		
		var listItem = document.createElement('li');
		listItem.id = field.id;
		
		// div to display the field data.
		var topDiv = document.createElement('div');
		topDiv.id = field.id + '_DETAILS';
	 	topDiv.className = 'template-section-field-list-item-editable box-shadow-outset';
		listItem.appendChild(topDiv);
		
		var span = document.createElement('span');
		span.className = 'disclose';
		span.appendChild(document.createElement('span'));
		topDiv.appendChild(span);
		
		// add field icon
		var icon = document.createElement('span');
		if(field.type === 'RADELEMENT_CDE') {
			icon.innerHTML = "&nbsp;&nbsp;" + field.set.id + ":" + field.set.name;
			icon.className = fieldTypes[field.type].icon;
		} else {
			icon.className = fieldTypes[field.type].icon;
		}
		
		icon.style.marginRight = '5px';
		topDiv.appendChild(icon);
		
		// Add a delete and edit buttons to allow users to edit and/or delete the Field
		var deleteFieldButton = document.createElement('button');
		deleteFieldButton.type = 'button';
		deleteFieldButton.className = 'glyphicon glyphicon-remove field-button';
		deleteFieldButton.style.visibility = 'hidden';
		deleteFieldButton.onclick = function() {
			deleteField(field.id);
		};
		topDiv.appendChild(deleteFieldButton);
		
		var editFieldButton = document.createElement('button');
		editFieldButton.type = 'button';
		editFieldButton.className = 'glyphicon glyphicon-pencil field-button';
		editFieldButton.style.visibility = 'hidden';
		editFieldButton.onclick = function() {
			editField(field.id);
		};
		topDiv.appendChild(editFieldButton);
	
		// Show the buttons on mouse over and hide on mouse out.
		topDiv.onmouseover = function() {
			deleteFieldButton.style.visibility = 'visible';
			editFieldButton.style.visibility = 'visible';
		};
		topDiv.onmouseout = function() {  
			deleteFieldButton.style.visibility = 'hidden';
			editFieldButton.style.visibility = 'hidden';
		};
		topDiv.ondblclick = function() { 
			editField(field.id);
		};
		
		// Add the field's data to the <div> element.
		var div = document.createElement('span');
		div.id = 'FieldName_' + field.id;
		div.className = 'field-text-display';
		div.appendChild(document.createTextNode(field.text.value));
		topDiv.appendChild(div);
		
		// If this is a section header field then add the <ol> tag to allow field to be added to it.
		if (field.type == 'SECTION_HEADER') {
			var fieldList = document.createElement('ol');
			fieldList.setAttribute('style', 'list-style-type: disc;');
			fieldList.id = field.id + '_FIELDS';
			topDiv.parentNode.appendChild(fieldList);
			
			// If this compound field has any child fields add them now.
			if (node && node.children) {
				for (var index = 0; index < node.children.length; index++) {
					var childNode = node.children[index];
					var childField = templateData.fields[childNode.id];
					addFieldToTemplateSectionDisplay(fieldList, childField, childNode);
				}
			}
			else {
				var child = document.createElement('ol');
				fieldList.appendChild(child);
			}
		}

		setupFieldContextMenu(listItem.id);
		return listItem;
	}
	
	/**
	 * Call the server to generate the MRRT document and then ask the user where the file should be saved
	 */
	function saveTemplate() {
		store();
		exportMRRT(templateData.metadata.id);
	};
	
	/**
	 * Load the template that has the provided ID from the server.  This will perform a synchronous AJAX call to
	 * the server and populate the templateData variable with the results.  If there are any issues getting the
	 * template an error dialog will be displayed.
	 */
	function loadTemplate(id) {
		$.ajax({
			  type: 'POST',
			  url: 'getTemplate?id=' + id,
			  success: function(data) {
			      templateData = data;
			  },
			  fail: function() {
				  displayError('<fmt:message key="unable.to.load.template"/>');
			  },
			  async: false
		});
		return null;
	};
	
	/**
	 * Build a blank template.  The starting point for creating a new template.
	 */
	function buildBlankTemplate() {
		var blankTemplate =  {
			'metadata': {
			    'id': generateUUID(),
			    'reportTemplateCreatorVersion': trexScriptVersion,
				'title': '',
				'description': '',
				'language': 'en',
				'email': '',
				'type': 'IMAGE_REPORT_TEMPLATE',
				'publisher': '',
				'rights': 'May be used freely, subject to license agreement',
				'license': 'http://www.radreport.org/license.pdf',
				'createdTimestamp': (new Date()).getTime(), 
				'lastModifiedTimestamp': '',
				'submittedTimestamp': '',
				'creator': '',
				'status': 'DRAFT',
				'contributors': [],
				'cdeSets': []
			},
			'fields': {
				'clinicalInformation': {
					'id': 'clinicalInformation',
					'text': {
						'value': '<fmt:message key="template.clinical.information"/>',
						'codeMeaning': 'Clinical Information',
						'codeValue': '55752-0',
						'codeScheme': 'LOINC'
					},
					'type': 'SECTION_HEADER',
					'level': 1
				},
				'clinicalInformationText': {
					'id': 'clinicalInformationText',
					'type': 'TEXT',
					'text': {
						'value': '<fmt:message key="template.default.field.text"/>',
						'codeMeaning': '',
						'codeValue': '',
						'codeScheme': ''
					},
					'dataFieldCompletionAction': 'NONE',
					'defaultValue': '',
					'multipleLines': true
				},
				'procedureInformation': {
					'id': 'procedureInformation',
					'text': {
						'value': '<fmt:message key="template.procedure.information"/>',
						'codeMeaning': 'Current Imaging Procedure Description',
						'codeValue': '55111-9',
						'codeScheme': 'LOINC'
					},
					'type': 'SECTION_HEADER',
					'level': 1
				},
				'procedureInformationText': {
					'id': 'procedureInformationText',
					'type': 'TEXT',
					'text': {
						'value': '<fmt:message key="template.default.field.text"/>',
						'codeMeaning': '',
						'codeValue': '',
						'codeScheme': ''
					},
					'dataFieldCompletionAction': 'NONE',
					'defaultValue': '',
					'multipleLines': true
				},
				'comparisons': {
					'id': 'comparisons',
					'text': {
						'value': '<fmt:message key="template.comparison"/>',
						'codeMeaning': 'Radiology Comparison Study',
						'codeValue': '18834-2',
						'codeScheme': 'LOINC'
					},
					'type': 'SECTION_HEADER',
					'level': 1
				},
				'comparisonsText': {
					'id': 'comparisonsText',
					'type': 'TEXT',
					'text': {
						'value': '<fmt:message key="template.default.field.text"/>',
						'codeMeaning': '',
						'codeValue': '',
						'codeScheme': ''
					},
					'dataFieldCompletionAction': 'NONE',
					'defaultValue': '',
					'multipleLines': true
				},
				'findings': {
					'id': 'findings',
					'text': {
						'value': '<fmt:message key="template.findings"/>',
						'codeMeaning': 'Procedure Findings',
						'codeValue': '59776-5',
						'codeScheme': 'LOINC'
					},
					'type': 'SECTION_HEADER',
					'level': 1
				},
				'findingsText': {
					'id': 'findingsText',
					'type': 'TEXT',
					'text': {
						'value': '<fmt:message key="template.default.field.text"/>',
						'codeMeaning': '',
						'codeValue': '',
						'codeScheme': ''
					},
					'dataFieldCompletionAction': 'NONE',
					'defaultValue': '',
					'multipleLines': true
				},
				'impression': {
					'id': 'impression',
					'text': {
						'value': '<fmt:message key="template.impression"/>',
						'codeMeaning': 'Impressions',
						'codeValue': '19005-8',
						'codeScheme': 'LOINC'
					},
					'type': 'SECTION_HEADER',
					'level': 1
				},
				'impressionText': {
					'id': 'impressionText',
					'type': 'TEXT',
					'text': {
						'value': '<fmt:message key="template.default.field.text"/>',
						'codeMeaning': '',
						'codeValue': '',
						'codeScheme': ''
					},
					'dataFieldCompletionAction': 'NONE',
					'defaultValue': '',
					'multipleLines': true
				}
			},
			'layout': [{
				'id': 'procedureInformation',
				'children': [{
					'id': 'procedureInformationText'
				}]
			},{
				'id': 'clinicalInformation',
				'children': [{
					'id': 'clinicalInformationText'
				}]
			},{
				'id': 'comparisons',
				'children': [{
					'id': 'comparisonsText'
				}]
			},{
				'id': 'findings',
				'children': [{
					'id': 'findingsText'
				}]
			},{
				'id': 'impression',
				'children': [{
					'id': 'impressionText'
				}]
			}]
		};
		return blankTemplate;
	};
	
	/////////////////////////////////////////////////
	// FUTURE STUFF BELOW
	/////////////////////////////////////////////////
	
	/**
	 * TODO - for future support of predefined fields.
	 */
	var predefinedFields = {
		'fieldTypes': {
			'1': {
				'id': '1',
				'name': 'Some Predefined Field 1',
				'fields': [{
					'text': 'Field 1',
					'type': 'TEXT'
				}]
			}
		},
		'clinicalInformation': ['1'],
		'procedureInformation': [],
		'comparisons': [],
		'findings': [],
		'impression': []	
	};
	
	/**
	 * Display the predefined questions for the currently displayed section.
	 * 
	 * @param sectionId The ID of the currently displayed section.
	 */
	function displayPredefinedQuestionsForSection(sectionId) {
	//	var predefinedFields = predefinedFields[sectionId];
		
		// TODO
	};
</script>