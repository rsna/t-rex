<%--
  -- Copyright (c) 2017, Karos Health Incorporated
  -- All rights reserved.
  --
  -- Redistribution and use in source and binary forms, with or without
  -- modification, are permitted provided that the following conditions are met:
  --
  -- 1. Redistributions of source code must retain the above copyright notice,
  -- this list of conditions and the following disclaimer.
  --
  -- 2. Redistributions in binary form must reproduce the above copyright notice,
  -- this list of conditions and the following disclaimer in the documentation
  -- and/or other materials provided with the distribution.
  --
  -- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  -- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  -- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  -- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  -- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  -- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  -- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  -- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  -- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  -- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  -- POSSIBILITY OF SUCH DAMAGE.
  --%>
<%@page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="com.rsna.trex.language.text" />

<%
	String[][] languages = {
		{"da_DK", "Danske"},			// Danish
		{"de_DE", "Deutsche"},			// German
		{"et_EE", "Eesti keel"},  		// Estonian
		{"en_US", "English"},
		{"es_ES", "Español"},  			// Spanish - Spain
		{"el_GR", "Ελληνικά"},			// Greek
		{"hu_HU", "Magyar"},  			// Hungarian
		{"nl_NL", "Nederlands"},		// Dutch
		{"pl_PL", "Polskie"},			// Polish
		{"tr_TR", "Türk"}				// Turkish
	};

	//Get version of application
	java.util.Properties prop = new java.util.Properties();
	prop.load(getServletContext().getResourceAsStream("/META-INF/MANIFEST.MF"));
	String applicationVersion = prop.getProperty("Implementation-Version"); 
%>

<nav class="navbar navbar-inverse navbar-static-top navigationBar" style="background: black;">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
                data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
                aria-expanded="false">
                <span class="sr-only">Toggle navigation</span> <span
                    class="icon-bar"></span> <span class="icon-bar"></span> <span
                    class="icon-bar"></span>
            </button>
            <img src="images/t_rex.png" style="display: -webkit-inline-box; vertical-align: top; height: 50px;" />
            <span class="title"><fmt:message key="application.name"/> </span>
            <span class="subtitle"><fmt:message key="application.name.description"/> - <fmt:message key="worklist.subtitle"/></span>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <span id="currentLanguage" style="color: white;"><fmt:message key="navbar.language.dropdown.label"/> </span> <span class="caret"></span>
                    </a>
                    <ul id="languageDropDown" class="dropdown-menu">

<% 
	for (int index = 0; index < languages.length; index++) {
		String code = languages[index][0];
		String name = languages[index][1];
%>

                        <li><a href="#" onclick="languageSelected('<%= code %>');"><%= name %></a></li>

<%
	}
%>

                  </ul>
                </li>
                
            </ul>
        </div>
    </div>
</nav>

<script>
	$('#currentLanguage').ready(function() {
        $(function() {
		    // Set the selected language that is being used to display the application.  The default
		    // language is English.
		    var lang = "English";

<% 
	for (int index = 0; index < languages.length; index++) {
		String code = languages[index][0];
		String name = languages[index][1];
%>

			if ("${language}" === "<%= code %>") { lang = "<%= name %>"; }

<%
	}
%>
		    $('#currentLanguage').append(document.createTextNode(lang));
        });
    });
</script>