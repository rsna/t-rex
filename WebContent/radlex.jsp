<%--
  -- Copyright (c) 2017, Karos Health Incorporated
  -- All rights reserved.
  --
  -- Redistribution and use in source and binary forms, with or without
  -- modification, are permitted provided that the following conditions are met:
  --
  -- 1. Redistributions of source code must retain the above copyright notice,
  -- this list of conditions and the following disclaimer.
  --
  -- 2. Redistributions in binary form must reproduce the above copyright notice,
  -- this list of conditions and the following disclaimer in the documentation
  -- and/or other materials provided with the distribution.
  --
  -- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  -- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  -- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  -- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  -- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  -- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  -- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  -- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  -- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  -- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  -- POSSIBILITY OF SUCH DAMAGE.
  --%>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="com.rsna.trex.language.text" />

<script>
	var codedValues = [];
	
	/**
	 * Display a bootstrap popover to allow users to link a field attribute to a RadLex code.
	 * 
	 * @param popoverElementId The ID of the popover element.
	 * @param fieldAttribute The field's attribute to be linked to a RadLex code, e.g. text or defaultValue.
	 * @param inputId ID of the input element that is displaying the field attribute.
	 */
	function showRadLexPopover(popoverElementId, fieldAttribute, inputId) {

		if (currentPopoverElementId) {
			return;
		}
		
		currentPopoverElementId = popoverElementId;
	
		$('#' + popoverElementId).off();
		
		var currentLinkedCode = getCurrentLinkedCode(fieldAttribute, inputId);
		
		// If the field is already linked to a RadLex code then get the code's value and set as search text.
		// If not use the value currently entered in the input.
		var searchText = '';
		if (currentLinkedCode.meaning.length > 0) {
			searchText = currentLinkedCode.meaning;
		}
		else {
			searchText = $('#' + inputId).val();
		}
		
		$('#' + popoverElementId).popover({
		    html: true,
		    placement: 'bottom',
		    title: '<fmt:message key="link.to.radlex"/>',
		    trigger: 'manual',
		    // need to provide the template to be able to size the popover correctly.
		    template: 
		    	'<div class="popover" style="max-width: 800px; width: auto;">' + 
			        '<div class="arrow"></div>' + 
			        '<div class="popover-inner">' + 
				        '<h3 class="popover-title"></h3>' + 
				        '<div class="popover-content"><p></p></div>' + 
			        '</div>' + 
		        '</div>',
		    content: 
		    	'<table>' + 
		        	'<tbody>' + 
			            '<tr>' + 
				            '<td>' + 
					        	'<fmt:message key="search.text"/>' + 
					        '</td>' + 
				            '<td>' + 
					        	'<input type="text" id="RadLexSearchTextInput" onkeypress="if (event.keyCode === 13) searchRadLexCodes();" value="' + searchText + '" autofocus="autofocus" style="margin-left: 10px; width: 200px;">' + 
					        '</td>' + 
				            '<td>' + 
					            '<button type="button" onclick="searchRadLexCodes();" style="margin-left: 10px;" class="btn btn-primary trexButton">' + 
					            	'<fmt:message key="search"/>' + 
					            '</button>' + 
				            '</td>' + 
			            '</tr>' + 
			            '<tr>' + 
				            '<td style="padding-top: 10px; vertical-align: text-top;">' + 
				            	'<fmt:message key="radlex.codes"/>' + 
				            '</td>' + 
				            '<td colspan="2">' + 
				            	'<select size="10" class="loading" style="margin-top: 10px; margin-left: 10px; width: 100%;" id="RadLexSearchResultsSelect"></select>' + 
				            '</td>' + 
			            '</tr>' + 
		            '</tbody>' + 
		        '</table>' +
	            '<div class="modal-footer">' + 
		            '<button id="LinkRadLexCodeButton" type="button" class="btn btn-primary trexButton" ' + 
		                     'onclick="linkRadLex(&quot;' + fieldAttribute + '&quot;, &quot;' + inputId + '&quot;);">' + 
		            	'<fmt:message key="link"/>' + 
		            '</button>' + 
		            (currentLinkedCode.value.length > 0 ? 
		            	'<button id="UnlinkRadLexCodeButton" type="button" class="btn btn-primary trexButton" ' + 
		            	         'onclick="unlinkRadLex(&quot;' + fieldAttribute + '&quot;, &quot;' + inputId + '&quot;);">' + 
			            	'<fmt:message key="unlink"/>' + 
				        '</button>' : ''
				    ) +
		            '<button id="CancelRadLexCodeButton" type="button" onclick="hideRadLexPopover(&quot;TEXT_FieldInputText&quot;);" class="btn btn-default">' + 
		            	'<fmt:message key="cancel"/>' + 
		            '</button>' + 
	            '</div>'
		});
		
		// When the popover is ready trigger a search if the field is already linked to a radlex code.
		$('#' + popoverElementId).on('shown.bs.popover', function () {
			if (searchText.length > 0) {
				searchRadLexCodes(searchText);
			}
			
		});
		
		$('#' + popoverElementId).popover('show');
	};
	
	function getCurrentLinkedCode(fieldAttribute, inputId) {
		var linkedCode = {};
		if (fieldAttribute == 'OPTION') {
			linkedCode.value = $('#' + inputId + '_CodeValue').val();
			linkedCode.meaning = $('#' + inputId + '_CodeMeaning').val();
			linkedCode.scheme = $('#' + inputId + '_CodeScheme').val();
		}
		else {
			linkedCode.value = currentlyEditedField[fieldAttribute].codeValue;
			linkedCode.meaning = currentlyEditedField[fieldAttribute].codeMeaning;
			linkedCode.scheme = currentlyEditedField[fieldAttribute].codeScheme;
		}
		return linkedCode;
	};
	
	/** 
	 * Hide the popover.
	 * 
	 * @param inputId ID of the input element displaying the field's attribute value.
	 */
	function hideRadLexPopover(inputId) {
		if (currentPopoverElementId) {
			$('#' + currentPopoverElementId).popover('destroy');
			$('#RadLexPopover').remove();
			currentPopoverElementId = null;
		}
		if (inputId) {
			$('#' + inputId).focus();
		}
	};
	
	function searchRadLexCodes(currentLinkedCode) {
		var query = $('#RadLexSearchTextInput').val();
		query = query.replace(" ", "%20");
// 		alert(query);
		var select = document.getElementById('RadLexSearchResultsSelect');
		while (select.hasChildNodes()) {
			select.removeChild(select.firstChild);
		}
		$('#RadLexSearchResultsSelect').addClass('spinner');
		codedValues = [];
		
		$.post('search_radlex?query=' + query)
			.done(function(data) {
				$('#RadLexSearchResultsSelect').removeClass('spinner');
				// populate UI with results.
				for (var i = 0; i < data.codes.length; i++) {
					codedValues = data.codes;
					var code = data.codes[i];
					var option = document.createElement("option");
					option.setAttribute("value", i);
					option.appendChild(document.createTextNode(code.meaning));
					
					// if this code matches currentLinkedCode then mark it as selected
					if (currentLinkedCode !== undefined && currentLinkedCode !== null) {
						if (code.meaning == currentLinkedCode.meaning) {
							option.setAttribute("selected", "selected");
						}
					}
					select.appendChild(option);
				}
			})
			.fail(function() {
				$('#RadLexSearchResultsSelect').removeClass('spinner');
				displayError('ERROR');
			}
		);
	};
	
	/**
	 * Unlink the field attribute from a RadLex code.
	 * 
	 * @param fieldAttribute The field's attribute to be linked to a RadLex code, e.g. text or defaultValue.
	 * @param inputId ID of the input element displaying the field's attribute value.
	 */
	function unlinkRadLex(fieldAttribute, inputId) {
		if (fieldAttribute == 'OPTION') {
			$('#' + inputId + '_CodeMeaning').val('');
			$('#' + inputId + '_CodeValue').val('');
			$('#' + inputId + '_CodeScheme').val('');
		}
		else {
			currentlyEditedField[fieldAttribute].codeValue = '';
			currentlyEditedField[fieldAttribute].codeScheme = '';
			currentlyEditedField[fieldAttribute].codeMeaning = '';
		}
	
		// Remove linked styling from input.
		var input = document.getElementById(inputId);
		input.className = input.className.replace("alert-info", "");
		input.className = input.className.replace("linked", "");
		input.setAttribute("title", "");
		
		hideRadLexPopover(inputId);
	};
	
	/**
	 * Link the field attribute to the RadLex code currently selected in the bootstrap popover.
	 * 
	 * @param fieldAttribute The field's attribute to be linked to a RadLex code, e.g. text or defaultValue.
	 * @param inputId ID of the input element displaying the field's attribute value.
	 */
	function linkRadLex(fieldAttribute, inputId) {
		var code = codedValues[$('#RadLexSearchResultsSelect').val()];
		if (code === undefined || code === null) {
			return;
		}
		
		// If an OPTION field then set the hidden input.  Otherwise update the field JSON.
		if (fieldAttribute == 'OPTION') {
			$('#' + inputId + '_CodeMeaning').val(code.meaning);
			$('#' + inputId + '_CodeValue').val(code.value);
			$('#' + inputId + '_CodeScheme').val(code.scheme);
		}
		else {
			currentlyEditedField[fieldAttribute].codeValue = code.value;
			currentlyEditedField[fieldAttribute].codeScheme = code.scheme;
			currentlyEditedField[fieldAttribute].codeMeaning = code.meaning;
		}
		
		// Add linked styling to input.
		var input = $('#' + inputId);
		input.addClass('alert-info');
		input.addClass('linked');
		input.attr('title', '<fmt:message key="linked.to.radlex"/> ' + code.meaning);
		
		hideRadLexPopover(inputId);
	};
</script>