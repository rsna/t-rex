<%--
  -- Copyright (c) 2017, Karos Health Incorporated
  -- All rights reserved.
  --
  -- Redistribution and use in source and binary forms, with or without
  -- modification, are permitted provided that the following conditions are met:
  --
  -- 1. Redistributions of source code must retain the above copyright notice,
  -- this list of conditions and the following disclaimer.
  --
  -- 2. Redistributions in binary form must reproduce the above copyright notice,
  -- this list of conditions and the following disclaimer in the documentation
  -- and/or other materials provided with the distribution.
  --
  -- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  -- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  -- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  -- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  -- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  -- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  -- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  -- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  -- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  -- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  -- POSSIBILITY OF SUCH DAMAGE.
  --%>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="com.rsna.trex.language.text" />

<%
	//Get version of application
	java.util.Properties prop = new java.util.Properties();
	prop.load(getServletContext().getResourceAsStream("/META-INF/MANIFEST.MF"));
	String applicationVersion = prop.getProperty("Implementation-Version"); 
%>

<!DOCTYPE html>
<html lang="${language}">

    <head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=0.5, minimum-scale=0.5, maximum-scale=0.5" />

	    <title><fmt:message key="application.name"/></title>

		<link rel='shortcut icon' href='images/favicon.ico?version=<%= applicationVersion %>' type="image/x-icon">
		<link rel='icon' href='images/favicon.ico?version=<%= applicationVersion %>' type="image/x-icon">
		
        <script src='thirdparty/underscore/underscore.js?version=<%= applicationVersion %>'></script>

        <link rel='stylesheet' href='thirdparty/jquerysortable/jquery-sortable.css?version=<%= applicationVersion %>' />
        <script src='thirdparty/jquery/jquery-1.11.2.min.js?version=<%= applicationVersion %>'></script>
  		<script src='thirdparty/jquery/jquery-ui.js?version=<%= applicationVersion %>'></script>
  		<script src='thirdparty/jquery/jquery.i18n.properties-min-1.0.9.js?version=<%= applicationVersion %>'></script>
  		<script src='thirdparty/jquerysortable/jquery-sortable.js?version=<%= applicationVersion %>'></script>

        <!-- Bootstrap -->
		<link rel='stylesheet' href='thirdparty/bootstrap/3.3.4/css/bootstrap.min.css?version=<%= applicationVersion %>' />
		<link rel='stylesheet' href='thirdparty/bootstrap/3.3.4/css/bootstrap-theme.min.css?version=<%= applicationVersion %>' />
	    <script src='thirdparty/bootstrap/3.3.4/js/bootstrap.min.js?version=<%= applicationVersion %>'></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />

	    <!-- Provides ability for context menus. -->
		<link rel='stylesheet' href='thirdparty/contextjs/context.css?version=<%= applicationVersion %>' />
	    <script src='thirdparty/contextjs/context.js?version=<%= applicationVersion %>'></script>

		<!-- Needed for utilities.jsp -->
		<script src='thirdparty/eligrey/FileSaver.min.js?version=<%= applicationVersion %>'></script>
		<script src='thirdparty/eligrey/Blob.js?version=<%= applicationVersion %>'></script>
		<script src='thirdparty/bootbox/bootbox.min.js?version=<%= applicationVersion %>'></script>

		<!-- Intro for adding hints and instructions, see https://introjs.com/ -->
		<script src='thirdparty/intro/intro.min.js?version=<%= applicationVersion %>'></script>
		<link rel='stylesheet' href='thirdparty/intro/introjs.min.css?version=<%= applicationVersion %>' />
		<link rel='stylesheet' href='thirdparty/intro/introjs-modern.css?version=<%= applicationVersion %>' />
		
        <!-- T-Rex specific stuff. -->
		<link rel='stylesheet' href='css/style.css?version=<%= applicationVersion %>' />
		<link rel='stylesheet' href='thirdparty/ihe/IHE_Template_Style.css?version=<%= applicationVersion %>' />
		<script src='data.js?version=<%= applicationVersion %>'></script>
		
		<jsp:include page='utilities.jsp' />
		<jsp:include page='editor-functions.jsp' />
    	<jsp:include page='radlex.jsp' />
    	
    	<script type="text/javascript">
            var templateId = getURLParameter('id');
            var radelementPage = 1;
            let radelementSpecialties = [];
      		getRadelementSpecialties();
            function initialize() {				
	    		context.init({
	    			preventDoubleContext: true,
	    			above: 'auto'
	    		});
	    		
				// Create the element to represent the coded questions that can be dragged and dropped onto the
				// template.
				buildDraggableFieldListItems();
				
				var languageSelect = document.getElementById("TemplateLanguageSelect");
				for (var index = 0; index < languages.length; index++) {
					var language = languages[index];
					var option = document.createElement("option");
					option.setAttribute("value", language.locale);
					option.appendChild(document.createTextNode(language.name));
					// sets default language to English
					if (language.locale.toLowerCase() == "en") {
						option.setAttribute("selected", "selected");
					}
					languageSelect.appendChild(option);
				}

				displayTemplate(templateId);
				
				setupEditorContextMenu('templateSectionsTabPanels');
				
				// Create the accordion widget used to display the template metadata and the draggable fields.
				$(function() {
					$("#accordion").accordion();
					
// 					$('#accordion h4').bind('click', function (e) {
// 						setTimeout(function() { 
// 							var intro = introJs();
// 							intro.setOption("nextLabel", "Next");
// 							intro.setOption("prevLabel", "Previous");
// 							intro.setOption("skipLabel", "Skip");
// 							intro.setOption("doneLabel", "Done");
// 							intro.setOption("showStepNumbers", false);
// 							intro.oncomplete(function() {
// 							    //alert("end of introduction");
// 							});
// 							intro.start() 
// 						}, 1000);
// 					});
				});
		
				var isNewField;
				
				// Create the sortable list group for the template.  This allows fields to be dragged and dropped 
				// onto the template.
				$("ol.default").sortable({
					group: 'field_group',
					pullPlaceholder : false,
					tolerance: 10,
					exclude: '.prevent_from_being_sorted',
					
					// animation on drop
					onDrop : function(item, targetContainer, _super) {
					    if (targetContainer.options.drop) {
					    	drop(item, _super, isNewField);
					    }
					    else {
					    	item.detach();
					    }
					},
		
					// set item relative to cursor position
					onDragStart : function($item, container, _super) {
						var offset = $item.offset();
						var pointer = container.rootGroup.pointer;
		
						adjustment = {
							left : pointer.left - offset.left,
							top : pointer.top - offset.top
						}

						// Duplicate items of the no drop area
					    if (!container.options.drop) {
					    	$item.clone().insertAfter($item);
					    	isNewField = true;
					    }
					    else {
					    	isNewField = false;
					    }

					    _super($item, container);
					},

					onDrag : function($item, position) {
						$item.css({
							left: position.left- 20,
							top: position.top - 10
						})
					}
				});

				// This allows the coded questions to be dragged into the template but doesn't allow fields to be
				// dragged to it.
				$("ol.simple_with_no_drop").sortable({
					group: 'field_group',
					drop: false,
					nested: false,
					isValidTarget : function($item, container) {
						return false;
					}
				});
			};

			/**
			 * Build a <li> element for each field type so that can fields can be added to the template using drag and
			 * drop.
			 */
			function buildDraggableFieldListItems() {
				// Iterate over the fields and add them so that they can be dragged into the template sections.
				// Sort them by ordinal so that they are added in the correct order
				var sorted = [];
				for (var type in fieldTypes) {
					sorted[fieldTypes[type].ordinal] = type;
				}

				for (var index = 0; index < sorted.length; index++) {
					
					if(sorted[index] !== 'RADELEMENT_CDE'){
						var fieldType = fieldTypes[sorted[index]];

						var icon = document.createElement('span');
						icon.className = fieldType.icon;
						
						var textSpan = document.createElement('span');
						textSpan.style.marginLeft = '5px';
						textSpan.appendChild(document.createTextNode(fieldType.name));

						var fieldText = document.createElement('div');
						fieldText.className = 'field-text';
						fieldText.appendChild(icon);
						fieldText.appendChild(textSpan);

						var fieldListItem = document.createElement('li');
						fieldListItem.id = fieldType.type;
						fieldListItem.className = 'field-type-list-item box-shadow-outset';
						fieldListItem.setAttribute('title', fieldType.hint);
						fieldListItem.appendChild(fieldText);
						
						if (fieldType.hasOwnProperty('step')) {
							fieldListItem.setAttribute('data-intro', fieldType.hint);
							fieldListItem.setAttribute('data-step', fieldType.step);
						}

						document.getElementById('fieldListGroup').appendChild(fieldListItem);
					}
				}
			};

			/**
			 * Display the template worklist page.
			 */
			function goToWorklist() {
				var id = templateData.metadata.id;
                window.open('index.jsp?language=${language}&template=' + id + '&t=' + new Date().getTime(), '_self');
			};
			
			function languageSelected(language) {
				store();
				var id = templateData.metadata.id;
				window.open('editor.jsp?language=' + language + '&id=' + id + '&t=' + new Date().getTime(), '_self');
			};
			
			async function getRadelementSpecialties() {
				
				await $.get('radelement_specialties')
				.done(function(data) {
					radelementSpecialties = data;
				});
				
				// Generate the subspecialty select drop menu for filter by CDE after we receive this information from the api
				var subspecialtySelect = document.getElementById("SubspecialtySelect");
				const subspecialtyArray = radelementSpecialties.data;
				const subspecListLength = subspecialtyArray.length;
				for (var index = 0; index < subspecListLength; index++) {
						var subspecialty = subspecialtyArray[index];
						var option = document.createElement("option");
						option.setAttribute("value", subspecialty.abbreviation);
						option.appendChild(document.createTextNode(subspecialty.name));
						
						subspecialtySelect.appendChild(option);
				}
			}
			
			async function searchRadelement() {
				$("#cdeList").empty();
				$(".radelementLoading").removeClass("hidden");
				let search = document.getElementById('RadelementSearch').value.trim();
				let radelementData = [];
				var subspecialtySelect = document.getElementById("SubspecialtySelect");
				var subspecialty = subspecialtySelect.value;
				let searchString = 'search_radelement?page=' + radelementPage;
				if(search !== ""){
					searchString += '&search=' + search;
				}
				if(subspecialty !== 'default'){
					searchString += '&specialties=' + subspecialty;
				}
				await $.get(searchString)
				.done(function(data) {
					radelementData = data;
				});
				$(".radelementLoading").addClass("hidden");

				for (let index = 0; index < radelementData.data.length; index++) {
					let set = radelementData.data[index];
					
					generateRadelementSetRow(set);
				};
				
				generateRadelementSearchPagination(radelementData.meta)
			};
			
			function generateRadelementSetRow(set) {
				// id span
				let rdes = document.createElement('span');
				rdes.appendChild(document.createTextNode(set.id));
				
				// set title
				let name = document.createElement('span');
				name.style.marginLeft = '5px';
				name.style.marginRight = '5px';
				name.appendChild(document.createTextNode(set.name));
				
				// link to radelement.org
				let link = document.createElement('a');
				link.href = 'https://radelement.org/home/sets/set/' + set.id;
				link.target = '_blank';
				
				let linkIcon = document.createElement('span');
				linkIcon.className = 'fa fa-external-link'
				link.appendChild(linkIcon);
					
				// append previous generated elements to div
				let fieldText = document.createElement('div');
				fieldText.className = 'field-text';
				fieldText.appendChild(rdes);
				fieldText.appendChild(name);
				fieldText.appendChild(link);
				
				// append div to list of all sets returned
				let fieldListItem = document.createElement('li');
				fieldListItem.id = set.id;				
				fieldListItem.setAttribute('set', JSON.stringify(set));
				fieldListItem.className = 'field-type-list-item box-shadow-outset';
				fieldListItem.appendChild(fieldText);
				
				document.getElementById('cdeList').appendChild(fieldListItem);
			}
			
			function generateRadelementSearchPagination(meta) {
				let pagination = $('<div />').addClass('d-flex').appendTo('#cdeList');
				
				if(radelementPage !== 1) {
					let backButton = $('<input />', {
			              type  : 'button',
			              value : 'prev',
			              id    : 'radelement-back',
			              class : radelementPage === meta.last_page ? 'ml-auto mr-auto' : 'ml-auto',
			              on    : {
			                 click: function() {
			                	 changeRadelementSearchPage('prev');
			                 }
			              }
			          });
					
					pagination.append(backButton);
				}
				
				if(radelementPage !== meta.last_page) {
					let nextButton = $('<input />', {
			              type  : 'button',
			              value : 'next',
			              class : radelementPage === 1 ? 'ml-auto mr-auto' : 'mr-auto',
			              id    : 'radelement-next',
			              on    : {
			                 click: function() {
			                	 changeRadelementSearchPage('next');
			                 }
			              }
			          });
					pagination.append(nextButton);
				}

				pagination.appendTo($('#cdeList'));
				
				let pageDiv = $('<div />', {
						class : 'd-flex',
		          }).appendTo('#cdeList');	
				let pageInfo = $('<div />', {
						class : 'ml-auto mr-auto',
		              html : "Page " + radelementPage + " of " + meta.last_page,
		          });	
				
				pageDiv.append(pageInfo);
			}
			
			function changeRadelementSearchPage(page) {
				if(page === 'next') {
					radelementPage++;
				} else if (page === 'prev') {
					radelementPage--;
				}
				searchRadelement();
			}
		</script>

	</head>

	<body>
        
        <jsp:include page='navbar.jsp' />

		<div class="field-panel">
			<div class="panel-group" id="accordion">
				<div class="field-panel-group">
					<h4><fmt:message key="editor.template.information.panel.title"/></h4>
				</div>
				<div class="panel-body template-information-details">
		       		<div>
		       			<h5><fmt:message key="editor.template.information.panel.name"/>:</h5>
		       		</div>
		       		<div>
		       			<input id="TemplateNameInput" type="text" style="width: 275px;" />
		       		</div>
		       		<div>
		       			<h5><fmt:message key="editor.template.information.panel.description"/>:</h5>
		       		</div>
		       		<div>
		       			<textarea id="TemplateDescriptionInput" rows="3" style="resize: none; width: 275px;"></textarea>
		       		</div>
		       		<div>
		       			<h5><fmt:message key="editor.template.information.panel.author"/>:</h5>
		       		</div>
		       		<div>
		       			<input id="TemplateAuthorInput" type="text" style="width: 275px;" />
		       			<!--
						<select id="TemplateModalitySelect"
								ng-model="TemplateModalitySelect"
			                    ng-options="modality.name as modality.name for modality in data.modalities"></select>
			            -->
		       		</div>
		       		<div>
		       			<h5><fmt:message key="editor.template.information.panel.author.email"/>:</h5>
		       		</div>
		       		<div>
		       			<input id="TemplateAuthorEmailInput" type="text" style="width: 275px;" />
		       		</div>
		       		<div>
		       			<h5><fmt:message key="editor.template.information.panel.language"/>:</h5>
		       		</div>
		       		<div>
						<select id="TemplateLanguageSelect"></select>
		       		</div>
                    <div>
                        <h5><fmt:message key="editor.template.information.panel.contributors"/>:</h5>
                    </div>
                    <div>
                        <textarea id="TemplateContributorsInput" rows="3" style="resize: none; width: 275px;"></textarea>
                    </div>
                    <div>
                        <h5><fmt:message key="editor.template.information.panel.publisher"/>:</h5>
                    </div>
                    <div>
                        <input id="TemplatePublisherInput" type="text" style="width: 275px;" />
                    </div>
		     	</div>
				<div class="field-panel-group" data-intro="T-Rex v<%= applicationVersion %> has some new coded questions." data-step="1">
					<h4><fmt:message key="editor.coded.questions.panel.title"/></h4>
				</div>
		     	<div class="panel-body" style="padding: 0px;">
					<ol id="fieldListGroup" class="list-group simple_with_no_drop vertical" style="margin-top: 5px;"></ol>
		     	</div>
				<div class="field-panel-group">
					<h4>CDE Search</h4>
				</div>
				<div class="panel-body" style="padding: 0px;">
					
					<div style="margin-top: 10px; margin-left: 10px;">
						<h5>Search:</h5>
		       			<input id="RadelementSearch" name="radelementSearch" class="mr-auto" type="text" style="width: 275px;" />
		       			<h5>Filter by Subspecialty:</h5>
		       			<select style="width: 275px;" id="SubspecialtySelect" name="subspecialtySelect">
			       			<option selected="selected" value="default">
			       			---Select a subspecialty---
			       			</option>
		       			</select>
		       			<button type="button" onclick="searchRadelement();" style="margin-top: 10px; margin-right: 10px;" class="btn btn-primary trexButton"> 
					    	<fmt:message key="search"/> 
					    </button>
		       		</div>
		       		<div class="radelementLoading spinner hidden"></div>
		       		<div id="radelementSearchResults">
		       			<ol id="cdeList" class="list-group simple_with_no_drop vertical" style="margin-top: 5px;"></ol>
		       		</div>
		     	</div>
			</div>
		</div>

		<form id="templateForm">

			<!-- All of the 1st level sections will be added as tabs, see displayTemplates function. -->
			<div class="template-sections-tabs">
				<ul id="templateSectionsTabs" class="nav nav-tabs" role="tablist">
				</ul>
			</div>

			<!-- Tab panes, see displayTemplates function. -->
			<div id="templateSectionsTabPanels" class="tab-content template-sections-tab-panels">
			</div>

			<div class="footer">
				<div class="button-group footer-button-group">
					<button type="button" class="btn btn-primary trexButton" onclick="store(); goToWorklist();" title='<fmt:message key="editor.action.worklist.tooltip"/>'>
                        <fmt:message key="editor.action.worklist"/>
                    </button>
					<button type="button" class="btn btn-primary trexButton" onclick="previewEditedTemplate();" title='<fmt:message key="editor.action.preview.tooltip"/>'>
                        <fmt:message key="editor.action.preview"/>
                    </button>
					<button type="button" class="btn btn-primary trexButton" onclick="store(); saveTemplate();" title='<fmt:message key="editor.action.export.tooltip"/>'>
                        <fmt:message key="editor.action.export"/>
                    </button>
				</div>
				<div class="footer-version">
					<fmt:message key="version"/> <%= applicationVersion %>
				</div>
			</div>
			
		</form>
		
		<!-- TODO Look into building the modal dialogs dynamically to reduce size and improve maintainability. -->
		<jsp:include page='dialogs/sectionHeaderFieldDialog.jsp' />
        <jsp:include page='dialogs/textFieldDialog.jsp' />
        <jsp:include page='dialogs/numberFieldDialog.jsp' />
        <jsp:include page='dialogs/dateFieldDialog.jsp' />
        <jsp:include page='dialogs/timeFieldDialog.jsp' />
        <jsp:include page='dialogs/selectionListFieldDialog.jsp' />
        <jsp:include page='dialogs/checkboxFieldDialog.jsp' />
        <jsp:include page='dialogs/radioFieldDialog.jsp' />
        <jsp:include page='dialogs/radelementCdeDialog.jsp' />

		<script type="text/javascript">
			initialize();
		</script>
		
    </body>

</html>