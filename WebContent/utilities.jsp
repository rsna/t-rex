<%--
  -- Copyright (c) 2017, Karos Health Incorporated
  -- All rights reserved.
  --
  -- Redistribution and use in source and binary forms, with or without
  -- modification, are permitted provided that the following conditions are met:
  --
  -- 1. Redistributions of source code must retain the above copyright notice,
  -- this list of conditions and the following disclaimer.
  --
  -- 2. Redistributions in binary form must reproduce the above copyright notice,
  -- this list of conditions and the following disclaimer in the documentation
  -- and/or other materials provided with the distribution.
  --
  -- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  -- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  -- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  -- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  -- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  -- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  -- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  -- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  -- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  -- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  -- POSSIBILITY OF SUCH DAMAGE.
  --%>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="com.rsna.trex.language.text" />

<%
/**
 * Utility javascript functions.  Users must include the following JS libs:
 * - FileSaver.min.js
 * - Blob.js
 * - bootbox.min.js
 *
 * Written as JSP to allow for internationalization.
 */

	//Get version of application
	java.util.Properties prop = new java.util.Properties();
	prop.load(getServletContext().getResourceAsStream("/META-INF/MANIFEST.MF"));
	String applicationVersion = prop.getProperty("Implementation-Version"); 
%>

<script>

    var trexScriptVersion = '<%= applicationVersion %>.1';

	function generateUUID() {
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		    var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
		    return v.toString(16);
		});
	};
	
    /**
     * Make sure the template structure is correct for this version of the application.
     */
    function upgradeTemplate(template) {
    	// make sure the template is the correct version
    	template.metadata.reportTemplateCreatorVersion = '<%= applicationVersion %>';
    	
    	// Make sure the LOINC codes are set.
    	template.fields.clinicalInformation.text.codeMeaning = 'Clinical Information';
    	template.fields.clinicalInformation.text.codeValue = '55752-0';
    	template.fields.clinicalInformation.text.codeScheme = 'LOINC';
    	template.fields.procedureInformation.text.codeMeaning = 'Current Imaging Procedure Description';
    	template.fields.procedureInformation.text.codeValue = '55111-9';
    	template.fields.procedureInformation.text.codeScheme = 'LOINC';
    	template.fields.comparisons.text.codeMeaning = 'Radiology Comparison Study';
    	template.fields.comparisons.text.codeValue = '18834-2';
    	template.fields.comparisons.text.codeScheme = 'LOINC';
    	template.fields.findings.text.codeMeaning = 'Procedure Findings';
    	template.fields.findings.text.codeValue = '59776-5';
    	template.fields.findings.text.codeScheme = 'LOINC';
    	template.fields.impression.text.codeMeaning = 'Impressions';
    	template.fields.impression.text.codeValue = '19005-8';
    	template.fields.impression.text.codeScheme = 'LOINC';
    };
    
	function getURLParameter(name) {
		return decodeURIComponent((new RegExp('[?|&]' + name + '='
				+ '([^&;]+?)(&|#|;|$)').exec(location.search) || [ , "" ])[1]
				.replace(/\+/g, '%20'))
				|| null
	};
	
	/**
	 * Get a preview of the MRRT document of one of the templates stored in the browser's local storage.   
	 * This will call the server to create the MRRT document and display the results in a modal dialog.
	 *
	 * @param templateId The unique identifier of the template to be displayed.
	 */
	function previewTemplate(templateId) {
		var templates = jQuery.parseJSON(localStorage.templates);
		var templateData = templates[templateId];
		var json = JSON.stringify(templateData);

		$.post('preview', json)
			.done(function(data) {
				bootbox.dialog({
					message : "<div style='overflow: scroll; position: relative; max-height: " + (screen.height/2) + "px;'>" + data + "</div>",
					title : '<fmt:message key="preview"/>',
					className: 'largeWidth',
					buttons : {
						success : {
							label : '<fmt:message key="ok"/>',
							className : 'btn-primary'
						}
					}
				}).find('div.modal-dialog').addClass('largeWidth');;
			})
			.fail(function() {
				displayError('<fmt:message key="preview.generation.error"/>');
			});
	};
	
	/**
	 * Call the server to generate and download the MRRT document.
	 */
	function exportMRRT(templateId) {
		var templates = jQuery.parseJSON(localStorage.templates);
		var templateData = templates[templateId];
		
		var name = templateData.metadata.title.trim();
		if (name.length == 0) {
			displayError(
				'<fmt:message key="template.name.missing"/>', 
				function() { 
					// Make sure the template name input is visible and put the focus into it.
					$('#accordion').accordion('option', 'active', 0);
					$('#TemplateNameInput').focus(); 
				}
			);
			return;
		}
		
		var json = JSON.stringify(templateData);
		$.post('download', json)
			.done(function(data) {
				var blob = new Blob([data], {type: 'text/html;charset=utf-8'});
				saveAs(blob, name + '.html');
		        bootbox.dialog({
		            message: '<fmt:message key="download.complete.message"/>',
		            title: '<fmt:message key="thank.you"/>',
		            buttons: {
		            	link: {
                            label: 'radreport.org',
                            className: 'btn-primary',
                            callback: function() { 
                            	let url = '';
                            	if(window.location.href.includes('stage')) {
                					url = 'http://stage.radreport.org/submit';
                				} else if (window.location.href.includes('dev') || window.location.href.includes('localhost')) {
                					url = 'http://dev.radreport.org/submit';
                				} else {
                					url = 'https://radreport.org/submit';
                				}
                            	var win = window.open(url, '_blank');
                            	win.focus();
                            }
		            	},
		                success: {
		                    label: '<fmt:message key="ok"/>',
		                    className: 'btn-primary'
		                }
		            }
		        });
			})
			.fail(function() {
				displayError('<fmt:message key="download.template.error.message"/>');
			});
	};
	
	function addClass(element, classToAdd) {
	    var currentClassValue = element.className;
	      
	    if (currentClassValue.indexOf(classToAdd) == -1) {
	        if ((currentClassValue == null) || (currentClassValue === "")) {
	            element.className = classToAdd;
	        } else {
	            element.className += " " + classToAdd;
	        }
	    }
	}
	 
	function removeClass(element, classToRemove) {
	    var currentClassValue = element.className;
	 
	    if (currentClassValue == classToRemove) {
	        element.className = "";
	        return;
	    }
	 
	    var classValues = currentClassValue.split(" ");
	    var filteredList = [];
	 
	    for (var i = 0 ; i < classValues.length; i++) {
	        if (classToRemove != classValues[i]) {
	            filteredList.push(classValues[i]);
	        }
	    }
	 
	    element.className = filteredList.join(" ");
	}
	
	/**
	 * Display a message is a modal dialog.
	 * 
	 * @param title The title to use for the modal dialog.
	 * @param message The message to display.
	 */
	function displayMessage(title, message) {
		bootbox.dialog({
			message : message,
			title : title,
			buttons : {
				success : {
					label : '<fmt:message key="ok"/>',
					className : 'btn-primary'
				}
			}
		});
	};
	
	/**
	 * Display an error message is a modal dialog.
	 * 
	 * @param message The message to display.
	 * @param callbackFunction Optional function that should be called when the modal dialog is closed.
	 */
	function displayError(message, callbackFunction) {
		if (!callbackFunction) {
			callbackFunction = function() {};
		}
		
		bootbox.dialog({
			message : message,
			title : '<fmt:message key="error"/>',
			buttons : {
				success : {
					label : '<fmt:message key="ok"/>',
					className : 'btn-primary',
					callback: callbackFunction 
				}
			}
		});
	};
	
	function elementFromPoint(x, y) {
		var check=false;
		var isRelative=true;
		
		if(!document.elementFromPoint) return null;

		if (!check) {
			var sl;
		    if((sl = $(document).scrollTop()) >0) {
		    	isRelative = (document.elementFromPoint(0, sl + $(window).height() -1) == null);
		    }
		    else if((sl = $(document).scrollLeft()) >0) {
		       	isRelative = (document.elementFromPoint(sl + $(window).width() -1, 0) == null);
		    }
		    check = (sl>0);
		}

		if (!isRelative) {
		    x += $(document).scrollLeft();
		    y += $(document).scrollTop();
		}

		return document.elementFromPoint(x,y);
	};
</script>