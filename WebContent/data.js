var languages = [ {
	"name" : "Abkhazian",
	"locale" : "AB"
}, {
	"name" : "Afar",
	"locale" : "AA"
}, {
	"name" : "Afrikaans",
	"locale" : "AF"
}, {
	"name" : "Albanian",
	"locale" : "SQ"
}, {
	"name" : "Amharic",
	"locale" : "AM"
}, {
	"name" : "Arabic",
	"locale" : "AR"
}, {
	"name" : "Armenian",
	"locale" : "HY"
}, {
	"name" : "Assamese",
	"locale" : "AS"
}, {
	"name" : "Aymara",
	"locale" : "AY"
}, {
	"name" : "Azerbaijani",
	"locale" : "AZ"
}, {
	"name" : "Bashkir",
	"locale" : "BA"
}, {
	"name" : "Basque",
	"locale" : "EU"
}, {
	"name" : "Bengali, Bangla",
	"locale" : "BN"
}, {
	"name" : "Bhutani",
	"locale" : "DZ"
}, {
	"name" : "Bihari",
	"locale" : "BH"
}, {
	"name" : "Bislama",
	"locale" : "BI"
}, {
	"name" : "Breton",
	"locale" : "BR"
}, {
	"name" : "Bulgarian",
	"locale" : "BG"
}, {
	"name" : "Burmese",
	"locale" : "MY"
}, {
	"name" : "Byelorussian",
	"locale" : "BE"
}, {
	"name" : "Cambodian",
	"locale" : "KM"
}, {
	"name" : "Catalan",
	"locale" : "CA"
}, {
	"name" : "Chinese",
	"locale" : "ZH"
}, {
	"name" : "Corsican",
	"locale" : "CO"
}, {
	"name" : "Croatian",
	"locale" : "HR"
}, {
	"name" : "Czech",
	"locale" : "CS"
}, {
	"name" : "Danish",
	"locale" : "DA"
}, {
	"name" : "Dutch",
	"locale" : "NL"
}, {
	"name" : "English",
	"locale" : "EN"
}, {
	"name" : "Esperanto",
	"locale" : "EO"
}, {
	"name" : "Estonian",
	"locale" : "ET"
}, {
	"name" : "Faeroese",
	"locale" : "FO"
}, {
	"name" : "Fiji",
	"locale" : "FJ"
}, {
	"name" : "Finnish",
	"locale" : "FI"
}, {
	"name" : "French",
	"locale" : "FR"
}, {
	"name" : "Frisian",
	"locale" : "FY"
}, {
	"name" : "Gaelic (Scots Gaelic)",
	"locale" : "GD"
}, {
	"name" : "Galician",
	"locale" : "GL"
}, {
	"name" : "Georgian",
	"locale" : "KA"
}, {
	"name" : "German",
	"locale" : "DE"
}, {
	"name" : "Greek",
	"locale" : "EL"
}, {
	"name" : "Greenlandic",
	"locale" : "KL"
}, {
	"name" : "Guarani",
	"locale" : "GN"
}, {
	"name" : "Gujarati",
	"locale" : "GU"
}, {
	"name" : "Hausa",
	"locale" : "HA"
}, {
	"name" : "Hebrew",
	"locale" : "IW"
}, {
	"name" : "Hindi",
	"locale" : "HI"
}, {
	"name" : "Hungarian",
	"locale" : "HU"
}, {
	"name" : "Icelandic",
	"locale" : "IS"
}, {
	"name" : "Indonesian",
	"locale" : "IN"
}, {
	"name" : "Interlingua",
	"locale" : "IA"
}, {
	"name" : "Interlingue",
	"locale" : "IE"
}, {
	"name" : "Inupiak",
	"locale" : "IK"
}, {
	"name" : "Irish",
	"locale" : "GA"
}, {
	"name" : "Italian",
	"locale" : "IT"
}, {
	"name" : "Japanese",
	"locale" : "JA"
}, {
	"name" : "Javanese",
	"locale" : "JW"
}, {
	"name" : "Kannada",
	"locale" : "KN"
}, {
	"name" : "Kashmiri",
	"locale" : "KS"
}, {
	"name" : "Kazakh",
	"locale" : "KK"
}, {
	"name" : "Kinyarwanda",
	"locale" : "RW"
}, {
	"name" : "Kirghiz",
	"locale" : "KY"
}, {
	"name" : "Kirundi",
	"locale" : "RN"
}, {
	"name" : "Korean",
	"locale" : "KO"
}, {
	"name" : "Kurdish",
	"locale" : "KU"
}, {
	"name" : "Laothian",
	"locale" : "LO"
}, {
	"name" : "Latin",
	"locale" : "LA"
}, {
	"name" : "Latvian, Lettish",
	"locale" : "LV"
}, {
	"name" : "Lingala",
	"locale" : "LN"
}, {
	"name" : "Lithuanian",
	"locale" : "LT"
}, {
	"name" : "Macedonian",
	"locale" : "MK"
}, {
	"name" : "Malagasy",
	"locale" : "MG"
}, {
	"name" : "Malay",
	"locale" : "MS"
}, {
	"name" : "Malayalam",
	"locale" : "ML"
}, {
	"name" : "Maltese",
	"locale" : "MT"
}, {
	"name" : "Maori",
	"locale" : "MI"
}, {
	"name" : "Marathi",
	"locale" : "MR"
}, {
	"name" : "Moldavian",
	"locale" : "MO"
}, {
	"name" : "Mongolian",
	"locale" : "MN"
}, {
	"name" : "Nauru",
	"locale" : "NA"
}, {
	"name" : "Nepali",
	"locale" : "NE"
}, {
	"name" : "Norwegian",
	"locale" : "NO"
}, {
	"name" : "Occitan",
	"locale" : "OC"
}, {
	"name" : "Oriya",
	"locale" : "OR"
}, {
	"name" : "Oromo, Afan",
	"locale" : "OM"
}, {
	"name" : "Pashto, Pushto",
	"locale" : "PS"
}, {
	"name" : "Persian",
	"locale" : "FA"
}, {
	"name" : "Polish",
	"locale" : "PL"
}, {
	"name" : "Portuguese",
	"locale" : "PT"
}, {
	"name" : "Punjabi",
	"locale" : "PA"
}, {
	"name" : "Quechua",
	"locale" : "QU"
}, {
	"name" : "Rhaeto-Romance",
	"locale" : "RM"
}, {
	"name" : "Romanian",
	"locale" : "RO"
}, {
	"name" : "Russian",
	"locale" : "RU"
}, {
	"name" : "Samoan",
	"locale" : "SM"
}, {
	"name" : "Sangro",
	"locale" : "SG"
}, {
	"name" : "Sanskrit",
	"locale" : "SA"
}, {
	"name" : "Serbian",
	"locale" : "SR"
}, {
	"name" : "Serbo-Croatian",
	"locale" : "SH"
}, {
	"name" : "Sesotho",
	"locale" : "ST"
}, {
	"name" : "Setswana",
	"locale" : "TN"
}, {
	"name" : "Shona",
	"locale" : "SN"
}, {
	"name" : "Sindhi",
	"locale" : "SD"
}, {
	"name" : "Singhalese",
	"locale" : "SI"
}, {
	"name" : "Siswati",
	"locale" : "SS"
}, {
	"name" : "Slovak",
	"locale" : "SK"
}, {
	"name" : "Slovenian",
	"locale" : "SL"
}, {
	"name" : "Somali",
	"locale" : "SO"
}, {
	"name" : "Spanish",
	"locale" : "ES"
}, {
	"name" : "Sudanese",
	"locale" : "SU"
}, {
	"name" : "Swahili",
	"locale" : "SW"
}, {
	"name" : "Swedish",
	"locale" : "SV"
}, {
	"name" : "Tagalog",
	"locale" : "TL"
}, {
	"name" : "Tajik",
	"locale" : "TG"
}, {
	"name" : "Tamil",
	"locale" : "TA"
}, {
	"name" : "Tatar",
	"locale" : "TT"
}, {
	"name" : "Tegulu",
	"locale" : "TE"
}, {
	"name" : "Thai",
	"locale" : "TH"
}, {
	"name" : "Tibetan",
	"locale" : "BO"
}, {
	"name" : "Tigrinya",
	"locale" : "TI"
}, {
	"name" : "Tonga",
	"locale" : "TO"
}, {
	"name" : "Tsonga",
	"locale" : "TS"
}, {
	"name" : "Turkish",
	"locale" : "TR"
}, {
	"name" : "Turkmen",
	"locale" : "TK"
}, {
	"name" : "Twi",
	"locale" : "TW"
}, {
	"name" : "Ukrainian",
	"locale" : "UK"
}, {
	"name" : "Urdu",
	"locale" : "UR"
}, {
	"name" : "Uzbek",
	"locale" : "UZ"
}, {
	"name" : "Vietnamese",
	"locale" : "VI"
}, {
	"name" : "Volapuk",
	"locale" : "VO"
}, {
	"name" : "Welsh",
	"locale" : "CY"
}, {
	"name" : "Wolof",
	"locale" : "WO"
}, {
	"name" : "Xhosa",
	"locale" : "XH"
}, {
	"name" : "Yiddish",
	"locale" : "JI"
}, {
	"name" : "Yoruba",
	"locale" : "YO"
}, {
	"name" : "Zulu",
	"locale" : "ZU"
} ];